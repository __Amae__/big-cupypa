//
//  Warp.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __WARP_H__
#define __WARP_H__

#include "GameObject.h"

/**
 * @brief	ワープ
 */
class Warp : public GameObject
{
private:
    
	// ステート
	typedef void (Warp::*State)(float);
    
public:
    
	/**
	 * @brief	ワープの生成
	 * @return	ワープ
	 */
	CREATE_FUNC(Warp);
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	Warp();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~Warp();
    
public:
    
	/**
	 * @brief	ワープの初期化
	 * @retval	true 初期化に成功
	 * @retval	false 初期化に失敗
	 */
	bool init();
    
	/**
	 * @brief	ワープの更新
	 * @param	dt 経過時間
	 */
	void update(float dt);
    
	/**
	 * @brief	ステージ構築完了イベント
	 * @param	stage ステージ
	 */
	void onConstructedStage(Stage* stage);
    
	/**
	 * @brief	衝突イベント
	 * @param	target 衝突相手
	 */
	void onCollisionDetected(GameObject* target);
    
private:
    
	/**
	 * @brief	通常更新
	 * @param	dt 経過時間
	 */
	void normalUpdate(float dt);
    
	/**
	 * @brief	ディレイ中の更新
	 * @param	dt 経過時間
	 */
	void delayUpdate(float dt);
    
	/**
	 * @brief	キャラクターの移動
	 * @param	character キャラクター
	 */
	void teleport(GameObject* character);
    
	/**
	 * @brief	回転するアクションの生成
	 * @return	生成されたアクション
	 */
	cocos2d::ActionInterval* createRotateAction();
    
	/**
	 * @brief	スケールアクションの生成
	 * @return	生成されたアクション
	 */
	cocos2d::ActionInterval* createScaleAction();
    
private:
    
	// ステート
	State state;
    
	// ワープディレイ
	float warpDelay;
    
	// ワープ
	Warp* target;
};

#endif //__WARP_H__
