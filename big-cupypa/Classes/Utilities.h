//
//  Utilities.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include "cocos2d.h"

namespace Utils
{
    /**
     * @brief   タイルマップ座標への変換
     * @param   point 座標の格納先
     * @param   x X軸のインデックス
     * @param   y Y軸のインデックス
     */
    void convertoTiledMapPosition(cocos2d::Point* point, unsigned int indexX, unsigned int indexY);
    
    /**
     * @brief   座標変換
     * @param   point 座標
     */
    void convertPosition(cocos2d::Point* point);
    
    /**
     * @brief   時間を秒に変換
     * @param   time 時間(f)
     */
    int convertTimeToSecond(int time);
    
    /**
     * @brief   時間を分に変換
     * @param   time 時間(f)
     */
    int convertTimeToMinute(int time);
}

#endif //__UTILITIES_H__
