﻿//
//  ActionSpecial2.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "ActionSpecial2.h"

USING_NS_CC;

/**
 * @brief	アクションの生成
 * @param	target	アクションを実行する対象の座標
 * @return	生成されたアクション
 */
ActionInterval* ActionSpecial2::create(const Point& target)
{
	float duration1 = 0.8f;
	float duration2 = 1.2f;

	// ジャンプするアクションを生成する
	ActionInterval* pJumpAction1 = JumpBy::create(duration1, Point(0.0f,0.0f), 64, 1);
	ActionInterval* pJumpAction2 = JumpBy::create(duration2, Point(0.0f,0.0f), 96, 1);

	// 移動するアクションを生成する
	ActionInterval* pMoveAction1 = MoveTo::create(duration1, Point(target.x-64.0f, target.y));
	ActionInterval* pMoveAction2 = MoveTo::create(duration1, Point(target.x+64.0f, target.y));
	ActionInterval* pMoveAction3 = MoveTo::create(duration2, Point(target.x, target.y));

	// 回転するアクションを生成する
	ActionInterval* pRotaAction1 = RotateBy::create(duration1, -720.0f);
	ActionInterval* pRotaAction2 = RotateBy::create(duration1, 720.0f);
	ActionInterval* pRotaAction3 = RotateBy::create(duration2, 0, 1440.0f);

	/// アクションを組み合わせる
	Spawn* pSpecialActionMaterial1 = Spawn::create(pJumpAction1, pMoveAction1, pRotaAction1, NULL);
	Spawn* pSpecialActionMaterial2 = Spawn::create(pJumpAction1, pMoveAction2, pRotaAction2, NULL);
	Spawn* pSpecialActionMaterial3 = Spawn::create(pJumpAction2, pMoveAction3, pRotaAction3, NULL);

	/// スペシャルアクションを生成する
	return Sequence::create(pSpecialActionMaterial1, pSpecialActionMaterial2, pSpecialActionMaterial3, NULL);
}
