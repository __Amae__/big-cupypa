//
//  Character.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/07.
//
//

#include "Character.h"
#include "ActionFactory.h"
#include "GameParameter.h"
#include "Stage.h"
#include "StaminaManager.h"
#include "GameTimer.h"

USING_NS_CC;

namespace
{
	// 移動速度
	const float MOVE_SPEED = 30.0f;
    
	// ダッシュの倍率
	const float DASH_RATIO = 5.0f;
    
	// ジャンプ力
	const float JUMP_POWER = 2.3f;
}

// スタミナの最大値
const int Character::MAX_STAMINA = 100;

// キャラクターのスケール
const float Character::CUPY_SCALE = 0.8f;

/**
 * @brief	デフォルトコンストラクタ
 */
Character::Character()
: state(NULL)
, action(NULL)
, viewDir(-1.0f)
, moveSpeed(MOVE_SPEED)
, beginJump(false)
, isJumping(false)
, jumpPower(0.0f)
, isDashing(false)
, stamina(MAX_STAMINA)
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_CHARACTER);
    
	// 移動フラグを設定
	setMovable(true);
}

/**
 * @brief	デストラクタ
 */
Character::~Character()
{
}

/**
 * @brief	キャラクターの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool Character::init()
{
	// 基底クラスを初期化
	if (!GameObject::init())
	{
		return false;
	}
    
	// スタミナをマネージャに登録
	StaminaManager::getInstance()->setStamina(&stamina);
    
	// スプライトのスケールを設定する
	getSprite()->setScale(CUPY_SCALE);
    
	Size collisionSize;
	collisionSize.width = 20.0f * CUPY_SCALE;
	collisionSize.height = 51.0f * CUPY_SCALE;
	setCollisionSize(collisionSize);
    
	// 移動
	requestWalk();
    
    //requestDash();
    
	return true;
}

/**
 * @brief	キャラクターの更新
 * @param	dt 経過時間
 */
void Character::update(float dt)
{
	// ステートを実行
	(this->*state)();
    
	// 重力計算
	calcGravity();
    
	// 次の座標を計算
	calcNextPosition();
    
	// スタミナ関係
	if (!isDashing && stamina < MAX_STAMINA)
	{
		// スタミナを変更
		++stamina;
        
		// スタミナ変更を通知
		StaminaManager::getInstance()->onStaminaChanged();
	}
}

/**
 * @brief	方向の設定
 * @param	dir 方向
 */
void Character::setDirection(float dir)
{
	// 視点方向を設定
	viewDir = dir;
    
	// クランプ
	viewDir = clampf(viewDir, -1.0f, 1.0f);
    
	// 回転を設定
	float rot = viewDir > 0.0f ? 180.0f : 0.0f;
	getSprite()->setRotationY(rot);
}

/**
 * @brief	待機要求
 */
void Character::requestWait()
{
	// ステートを設定
	state = &Character::wait;
    
	// アクションを変更
	changeAction(ACTIONTYPE_WAIT);
}

/**
 * @brief	徒歩要求
 */
void Character::requestWalk()
{
	if (isJumping) return;
    
	// ダッシュフラグを設定
	isDashing = false;
    
	// ステートを設定
	state = &Character::move;
    
	// アクションを変更
	changeAction(ACTIONTYPE_WALK);
}

/**
 * @brief	ダッシュ要求
 */
void Character::requestDash()
{
	if (isJumping) return;
    
	// ダッシュフラグを設定
	isDashing = true;
    
	// ステートを設定
	state = &Character::move;
    
	// アクションを変更
	changeAction(ACTIONTYPE_DASH);
}

/**
 * @brief	ジャンプ要求
 */
void Character::requestJump()
{
	if (beginJump || isJumping)
	{
		return;
	}
    
	// ダッシュ状態を解除
	isDashing = false;
    
	// ステートを設定
	state = &Character::jump;
    
	// アクションを変更
	changeAction(ACTIONTYPE_JUMP);
    
	// ジャンプ開始フラグを設定
	beginJump = true;
    
	// ジャンプフラグを設定
	isJumping = true;
    
	// ジャンプ力を設定
	jumpPower = JUMP_POWER;
    
	if (getPositionY() > (GameParameter::BASE_SCREEN_HEIGHT - GameParameter::BLOCK_HEIGHT))
	{
		jumpPower *= 1.4f;
	}
}

/**
 * @brief	反転要求
 */
void Character::requestTurn()
{
	float angle = viewDir >= 0.0f ? 0.0f : -180.0f;
    
	RotateTo* action = RotateTo::create(0.5f, 0.0f, angle);
	getSprite()->runAction(action);
    
	// 視点方向を反転
	viewDir = -viewDir;
}

/**
 * @brief	衝突イベント
 * @param	target 衝突相手
 */
void Character::onCollisionDetected(GameObject* target)
{
	// 底辺との衝突処理
	collideBottom(target);
    
	// 横との衝突判定
	collideSide(target);
}

/**
 * @brief	スプライトの生成
 * @return	スプライト
 */
Sprite* Character::createSprite()
{
	return Sprite::create();
}

/**
 * @brief	待機
 */
void Character::wait()
{
    setVelocityX(0.0f);
}

/**
 * @brief	移動
 */
void Character::move()
{
	// 速度
	float vel = viewDir * MOVE_SPEED * GameTimer::getElapsedTime();
    
	if (isDashing)
	{
		// スタミナ計算
		if (0 < stamina)
		{
			//--stamina;
            
			// マネージャにスタミナ変更通知
			StaminaManager::getInstance()->onStaminaChanged();
            
			// 速度を設定
			vel *= DASH_RATIO;
		}
		else
		{
			requestWalk();
            //requestWait();
		}
	}
    
	// X軸の速度を設定
	setVelocityX(vel);
}

/**
 * @brief	ジャンプ
 */
void Character::jump()
{
	if (beginJump) beginJump = false;
    
	// X軸の速度を設定
	setVelocityX(viewDir * MOVE_SPEED * GameTimer::getElapsedTime());
    
	// Y軸の速度を設定
	setVelocityY(jumpPower);
}

/**
 * @brief	下境界線衝突時の処理
 */
void Character::onCollidedBottomBorder()
{
	if (isJumping && !beginJump)
	{
		// ジャンプをキャンセル
		cancelJump();
        
		// 移動
		requestWalk();
	}
}

/**
 * @brief	左境界線衝突時の処理
 */
void Character::onCollidedLeftBorder()
{
	Point next = getNextPosition();
    
	Point p = next;
	p.x = GameParameter::BASE_SCREEN_WIDTH - GameParameter::HALF_BLOCK_WIDTH;
	p.y += GameParameter::HALF_BLOCK_HEIGHT;
    
	// 衝突結果
	bool hit = false;
    
	if (stage->collisionAt(p, NULL))
	{
		Point tmp = p;
		tmp.y += GameParameter::BLOCK_HEIGHT;
        
		if (stage->collisionAt(tmp, NULL))
		{
			hit = true;
		}
		else
		{
			tmp.x = next.x;
            
			if (stage->collisionAt(tmp, NULL))
			{
				hit = true;
			}
		}
        
		if (hit)
		{
			requestTurn();
		}
		else
		{
			requestJump();
		}
	}
}

/**
 * @brief	右境界線衝突時の処理
 */
void Character::onCollidedRightBorder()
{
	Point next = getNextPosition();
    
	Point p = next;
	p.x = GameParameter::HALF_BLOCK_WIDTH;
	p.y += GameParameter::HALF_BLOCK_HEIGHT;
    
	// 衝突結果
	bool hit = false;
    
	if (stage->collisionAt(p, NULL))
	{
		Point tmp = p;
		tmp.y += GameParameter::BLOCK_HEIGHT;
        
		if (stage->collisionAt(tmp, NULL))
		{
			hit = true;
		}
		else
		{
			tmp.x = next.x;
            
			if (stage->collisionAt(tmp, NULL))
			{
				hit = true;
			}
		}
        
		if (hit)
		{
			requestTurn();
		}
		else
		{
			requestJump();
		}
	}
}

/**
 * @brief	上から下へ移動した場合の処理
 */
void Character::onChangedTopToBottom()
{
}

/**
 * @brief	アクションの変更
 * @param	type アクションタイプ
 */
void Character::changeAction(ActionType type)
{
	// スプライトを取得
	Sprite* sprite = getSprite();
    
	if (action)
	{
		// アクションを停止
		sprite->stopAction(action);
	}
    
	// アクションを生成
	action = ActionFactory::createAction(type);
    
	// アクションを実行
	sprite->runAction(action);
}

/**
 * @brief	ジャンプ状態の解除
 */
void Character::cancelJump()
{
	if (!isJumping)
	{
		return;
	}
    
	// ジャンプ開始フラグを設定
	beginJump = false;
    
	// ジャンプフラグを設定
	isJumping = false;
    
	// ジャンプ力を設定
	jumpPower = 0.0f;
    
	// Y軸の速度を設定
	setVelocityY(0.0f);
}

/**
 * @brief	横との衝突処理
 * @param	target 衝突相手
 */
void Character::collideSide(GameObject* target)
{
	if (intersectSide(viewDir, target))
	{
		// 次の座標
		float nextX = target->getPositionX();
		float offset = (target->getCollisionSize().width * 0.5f +
                        getCollisionSize().width * 0.5f) * -viewDir;
		nextX += offset;
        
		// 次のX座標を設定
		setNextPositionX(nextX);
        
		// ターンフラグ
		bool turnFlag = true;
        
		// ターゲットの上に乗れる場合
		if (target->isMountable())
		{
			// ターゲットの上と衝突判定
			Point p = target->getPosition();
			p.y += GameParameter::BLOCK_HEIGHT;
            
			// 画面外判定
			if (GameParameter::BASE_SCREEN_HEIGHT < p.y)
			{
				p.y = GameParameter::BLOCK_HEIGHT + GameParameter::HALF_BLOCK_HEIGHT;
			}
            
			GameObjectArray objects;
			unsigned int numObjects = stage->collisionAt(p, &objects);
            
			if (0 == numObjects || !objects[0]->isMountable())
			{
				// キャラクターの上と衝突判定
				Point point = getPosition();
                
				Point topPoint = point;
				topPoint.y += GameParameter::BLOCK_HEIGHT;
                
				// 画面外判定
				if (GameParameter::BASE_SCREEN_HEIGHT < topPoint.y)
				{
					topPoint.y = GameParameter::BLOCK_HEIGHT + GameParameter::HALF_BLOCK_HEIGHT;
				}
                
				if (!stage->collisionAt(topPoint, NULL))
				{
					Point bottomPoint = point;
					bottomPoint.y -= getCollisionSize().height * 0.6f;
                    
					if (GameParameter::BLOCK_HEIGHT > bottomPoint.y)
					{
						bottomPoint.y = GameParameter::BASE_SCREEN_HEIGHT - GameParameter::HALF_BLOCK_HEIGHT;
					}
                    
					if (stage->collisionAt(bottomPoint, NULL)
                        /*|| GameParameter::BLOCK_HEIGHT > bottomPoint.y*/)
					{
						// ジャンプ
						requestJump();
						turnFlag = false;
					}
				}
			}
		}
        
		if (turnFlag && !isJumping)
		{
			// 判定要求
			requestTurn();
		}
	}
}

/**
 * @brief	底辺との衝突処理
 * @param	target 衝突相手
 */
void Character::collideBottom(GameObject* target)
{
	if (intersectBottom(target))
	{
		float nextY = target->getPositionY();
		nextY += target->getCollisionSize().height * 0.5f;
		nextY += getCollisionSize().height * 0.5f;
        
		// 次のY座標を設定
		setNextPositionY(nextY);
        
		// 重力をリセット
		resetGravity();
        
		// ジャンプ中の場合
		if (isJumping)
		{
			// ジャンプ状態を解除
			if (!beginJump)
			{
				cancelJump();
                
				// 移動
				requestWalk();
			}
		}
	}
}

/**
 * @brief	横方向と線分判定
 * @param	dir 左右
 * @param	target ターゲット
 * @retval	true 交差している
 * @retval	false 交差していない
 */
bool Character::intersectSide(float dir, GameObject* target)
{
	// 線分開始点
	Point start = getPosition();
    
	// 線分終了点
	Point end = getNextPosition();
    
	// 視点方向を基準に計算
	end.x += getCollisionSize().width * 0.5f * viewDir;
    
	// Y軸を統一
	//end.y = start.y;
	start.y = end.y;
    
	// 線分
	Segment segment;
	segment.set(start, end);
    
	// 頂点インデックス
	unsigned int p1, p2;
    
	// 右向きの場合
	if (0.0f < dir)
	{
		// 左側
		p1 = 0;
		p2 = 2;
	}
	// 左向きの場合
	else if (0.0f > dir)
	{
		// 右側
		p1 = 1;
		p2 = 3;
	}
    
	// 線分による交差判
	return target->checkCollision(segment, p1, p2);
}

/**
 * @brief	下方向と線分判定
 * @param	target [in] ターゲット
 * @retval	true 交差している
 * @retval	false 交差していない
 */
bool Character::intersectBottom(GameObject* target)
{
	// 線分開始点
	Point start = getPosition();
    
	// 線分終了点
	Point end = getNextPosition();
    
	// 底辺を基準に計算
	end.y -= getCollisionSize().height * 0.5f;
    
	// X軸を統一
	end.x = start.x;
    
	// 線分
	Segment segment;
	segment.set(start, end);
    
	// 線分による交差判定
	return target->checkCollision(segment, 0, 1);
}
