//
//  BugBlock.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __BUGBLOCK_H__
#define __BUGBLOCK_H__

#include "GameObject.h"

/**
 * @brief	バグブロック
 */
class BugBlock : public GameObject
{
    
public:
    
	/**
	 * @brief	バグブロックの生成
	 * @return	バグブロック
	 */
	CREATE_FUNC(BugBlock);
    
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	BugBlock();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~BugBlock();
};

#endif //__BUGBLOCK_H__
