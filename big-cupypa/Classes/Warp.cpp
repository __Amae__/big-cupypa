//
//  Warp.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "Warp.h"
#include "GameParameter.h"
#include "Stage.h"

USING_NS_CC;

namespace
{
	// ワープディレイ
	const float WARP_DELAY = 5.0f;
    
	// 回転アクションのアングル
	const float ROTATE_ANGLE = -3.0f;
    
	// スケールアクションの持続時間
	const float SCALE_DURATION = 2.0f;
    
	// スケールアクションの最小スケール
	const float SCALE_MINSCALE = 0.85f;
    
	// スケールアクションの最大スケール
	const float SCALE_MAXSCALE = 1.15f;
}

/**
 * @brief	デフォルトコンストラクタ
 */
Warp::Warp()
: state(NULL)
, warpDelay(0.0f)
, target(NULL)
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_WARP);
}

/**
 * @brief	デストラクタ
 */
Warp::~Warp()
{
}

/**
 * @brief	ワープの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool Warp::init()
{
	// 基底クラスを初期化
	if (!GameObject::init())
	{
		return false;
	}
    
	// 衝突サイズ
	Size collisionSize;
	collisionSize.width = 2.0f;
	collisionSize.height = 2.0f;
    
	// 衝突サイズを設定
	setCollisionSize(collisionSize);
    
	// 乗れるフラグを設定
	setMountable(false);
    
	// ステートを設定
	state = &Warp::normalUpdate;
    
	// アクションを実行
	getSprite()->runAction(RepeatForever::create(createRotateAction()));
	getSprite()->runAction(RepeatForever::create(createScaleAction()));
    
	return true;
}

/**
 * @brief	ワープの更新
 * @param	dt 経過時間
 */
void Warp::update(float dt)
{
	// ステートを実行
	(this->*state)(dt);
}

/**
 * @brief	ステージ構築完了イベント
 * @param	stage ステージ
 */
void Warp::onConstructedStage(Stage* stage)
{
	GameObject::onConstructedStage(stage);
    
	// 移動先のワープを取得
	GameObjectArray tmp;
	if (stage->getGameObjects(&tmp, OBJECTTYPE_WARP))
	{
		for (unsigned int i = 0; i < tmp.size(); ++i)
		{
			if (this != tmp[i])
			{
				target = static_cast<Warp*>(tmp[i]);
				break;
			}
		}
	}
}

/**
 * @brief	衝突イベント
 * @param	target 衝突相手
 */
void Warp::onCollisionDetected(GameObject* target)
{
	// キャラクター以外の場合
	if (OBJECTTYPE_CHARACTER != target->getObjectType())
	{
		return;
	}
    
	// 移動
	this->target->teleport(target);
}

/**
 * @brief	通常更新
 * @param	dt 経過時間
 */
void Warp::normalUpdate(float dt)
{
}

/**
 * @brief	ディレイ中の更新
 * @param	dt 経過時間
 */
void Warp::delayUpdate(float dt)
{
	// 時間経過
	warpDelay -= dt;
    
	// ディレイが終了した場合
	if (0.0f >= warpDelay)
	{
		// 衝突判定を設定
		setCollidable(true);
        
		// ステートを設定
		state = &Warp::normalUpdate;
        
		// ワープディレイをリセット
		warpDelay = 0.0f;
        
		Sprite* sprite = getSprite();
		Sprite* targetSprite = target->getSprite();
        
		sprite->stopAllActions();
		sprite->runAction(RepeatForever::create(createRotateAction()));
		sprite->runAction(RepeatForever::create(createScaleAction()));
		targetSprite->stopAllActions();
		targetSprite->runAction(RepeatForever::create(createRotateAction()));
		targetSprite->runAction(RepeatForever::create(createScaleAction()));
	}
}

/**
 * @brief	キャラクターの移動
 * @param	character キャラクター
 */
void Warp::teleport(GameObject* character)
{
	if (0.0f < warpDelay)
	{
		return;
	}
    
	// 座標
	Point point = getPosition();
    
	// キャラクターの座標を設定
	character->setNextPosition(point);
    
	// ワープディレイを設定
	warpDelay = WARP_DELAY;
    
	// 衝突判定を設定
	setCollidable(false);
    
	// ステートを設定
	state = &Warp::delayUpdate;
    
	Sprite* sprite = getSprite();
	Sprite* targetSprite = target->getSprite();
    
	ScaleTo* scale1 = ScaleTo::create(0.5f, 0.5f);
	ScaleTo* scale2 = ScaleTo::create(0.5f, 0.5f);
    
	sprite->stopAllActions();
	sprite->runAction(scale1);
	targetSprite->stopAllActions();
	targetSprite->runAction(scale2);
}

/**
 * @brief	回転するアクションの生成
 * @return	生成されたアクション
 */
ActionInterval* Warp::createRotateAction()
{
	RotateBy* rotateAction = RotateBy::create(GameParameter::ANIMATION_INTERVAL, ROTATE_ANGLE);
	return rotateAction;
}

/**
 * @brief	スケールアクションの生成
 * @return	生成されたアクション
 */
ActionInterval* Warp::createScaleAction()
{
	ScaleTo* minscaleAction = ScaleTo::create(SCALE_DURATION, SCALE_MINSCALE);
	ScaleTo* maxscaleAction = ScaleTo::create(SCALE_DURATION, SCALE_MAXSCALE);
	Sequence* action = Sequence::create(minscaleAction, maxscaleAction, NULL);
	return action;
}
