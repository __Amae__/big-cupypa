#include "SceneFactory.h"
#include "LogoScene.h"
#include "TitleScene.h"
#include "StageSelectScene.h"
#include "PlayScene.h"
#include "ResultScene.h"
#include "ConfigScene.h"

USING_NS_CC;

/**
 * @brief コンストラクタ
 * @note  インスタンス禁止
 */
SceneFactory::SceneFactory()
{
}

/**
 * @brief	シーンの生成
 * @param	シーンID
 * @return	シーン
 */
Scene* SceneFactory::createScene(SceneId sceneId)
{
    Scene* scene = nullptr;
    
	switch (sceneId)
	{
        case LOGO_SCENE:
            // ロゴシーン
            scene = LogoScene::createScene();
            break;
            
        case TITLE_SCENE:
            // タイトルシーン
            scene = TitleScene::createScene();
            break;
            
        case STAGESELECT_SCENE:
            // ステージセレクトシーン
            scene = StageSelectScene::createScene();
            break;
            
        case PLAY_SCENE:
            // プレイシーン
            scene = PlayScene::createScene();
            break;
            
        case RESULT_SCENE:
            // リザルトシーン
            scene = ResultScene::createScene();
            break;
            
        case CONFIG_SCENE:
            // 設定シーン
            scene = ConfigScene::createScene();
            break;
        
        default:
            assert(false);
            break;
	}
    
	return scene;
}