#include "PlayScene.h"
#include "SceneFactory.h"
#include "GameParameter.h"
#include "GameManager.h"
#include "Stage.h"
#include "GameTimer.h"
#include "StaminaManager.h"
#include "StopWatch.h"

#include "Button.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

namespace
{
	/**
	 * @brief ノード
	 */
	enum NodeDepth
	{
		NODEDEPTH_BACKGROUND,       // 背景
        NODEDEPTH_STAGE,            // ステージ
        NODEDEPTH_EFFECT,           // エフェクト
        NODEDEPTH_MENUBACKGROUND,	// メニュー背景
        NODEDEPTH_MAGICLINE,        // マジックライン
        NODEDEPTH_STAMINA,          // スタミナ
        NODEDEPTH_CLOSEMENU,        // クローズメニュー
	};
    
    /**
     * @brief ノードタグ
     */
    enum NodeTag
    {
        // クローズメニュー
        NODETAG_CLOSEMENU,
        // スタミナ
        NODETAG_STAMINA,
        // エフェクト
        NODETAG_EFFECT,
        // マジックライン
        NODETAG_MAGICLINE,
        // ステージ
        NODETAG_STAGE,
        // 背景
        NODETAG_BACKGROUND,
        // メニュー背景
        NODETAG_MENUBACKGROUND,
    };
    
	// チームロゴのアクションの持続時間
	const float TEAMLOGO_ACTION_DURATION = 3.0f;
    
	// チームロゴの延滞時間
	const float TEAMLOGO_DELAY_DURATION = 1.0f;
    
	// 次のシーンID
	const SceneId NEXT_SCENE = RESULT_SCENE;
    
    // タイトルロゴのファイル名
	const char* BACKGROUND_FILENAME = "Universe.png";
    
    // BGMのファイル名
    const char* PLAY_BGM = "Ancient_Ocean_FreeVer.mp3";
    
	// 透過時間
	const float TRANSITION_DURATION = 1.0f;
}

/**
 * @brief   デフォルトコンストラクタ
 */
PlayScene::PlayScene()
: mStage(nullptr)
, mDashButton(nullptr)
, mWalkButton(nullptr)
, mWaitButton(nullptr)
, mStopWatch(nullptr)
, mTimeLabel(nullptr)
, mIsChange(false)
{
    // 音楽データを読み込む
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic(PLAY_BGM);
}

/**
 * @brief   デストラクタ
 */
PlayScene::~PlayScene()
{
}

/**
 * @brief   解放処理
 */
void PlayScene::finalize()
{
    // BGM停止
    SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    // 解放
    SimpleAudioEngine::getInstance()->end();
}

/**
 * @brief シーンの交換
 */
void PlayScene::replaceScene()
{
    // 解放処理
    this->finalize();
    
    // すべてリスナーの削除
    this->getEventDispatcher()->removeAllEventListeners();
    
	// 次のシーンの生成
	Scene* nextScene = SceneFactory::createScene(NEXT_SCENE);
    
	//  
	nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
    
	// シーンの交換
	Director::getInstance()->replaceScene(nextScene);
}

/**
 * @brief	初期化処理
 * @retval	true  成功
 * @retval	false 失敗
 */
bool PlayScene::init()
{
    // 親レイヤーの初期化
	if(!Layer::init())
	{
		return false;
	}
    
    // リスナーの生成
	auto listener = EventListenerTouchOneByOne::create();
    
    // イベントの設定
    listener->onTouchBegan = CC_CALLBACK_2(PlayScene::onTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(PlayScene::onTouchEnded, this);
    
    // リスナーの優先度の設定
    this->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
    
    // 背景スプライトを生成
    Sprite* background = Sprite::create(BACKGROUND_FILENAME);
    
    // スプライト座標を設定
    background->setPosition(GameParameter::SCREEN_CENTER);
    background->setRotation(-90);
    this->addChild(background);
    
    // ステージを生成
    mStage = Stage::create(1);
    this->addChild(mStage, NODEDEPTH_STAGE, NODETAG_STAGE);
    
    // ダッシュボタンを生成
    Rect r(64.0f, 0.0f, 32.0f, 32.0f);
    mDashButton = Button::create("Icons.png", &r);
    mDashButton->setPosition(16, 16);
    mDashButton->setColor(Color3B::WHITE);
    this->addChild(mDashButton, NODETAG_CLOSEMENU, NODEDEPTH_MENUBACKGROUND);
    
    // 歩くボタンを生成
    mWalkButton = Button::create("Icons.png", &r);
    mWalkButton->setPosition(64, 16);
    mWalkButton->setColor(Color3B::GREEN);
    this->addChild(mWalkButton, NODETAG_CLOSEMENU, NODEDEPTH_MENUBACKGROUND);
    
    // 待機ボタンを生成
    mWaitButton = Button::create("Icons.png", &r);
    mWaitButton->setPosition(120, 16);
    mWaitButton->setColor(Color3B::RED);
    this->addChild(mWaitButton, NODETAG_CLOSEMENU, NODEDEPTH_MENUBACKGROUND);
    
    // ストップウォッチを生成
    mStopWatch = StopWatch::create();
    GameManager::getInstance()->setStopWatch(mStopWatch);
    this->addChild(mStopWatch);
    
    // 更新メソッドをスケジュールする
    this->schedule(schedule_selector(PlayScene::update));
    
    // ストップウォッチのカウントをスタートする
    mStopWatch->countStart(true);
    
    // タイムラベル
//    mTimeLabel = Label::createWithSystemFont("TIME : ", "arial", 35);
//    mTimeLabel->setPosition(Point(350.0f, 200.0f));
//    this->addChild(mTimeLabel);
    
    mIsChange = false;
    
    // タイマーを初期化する
    GameTimer::init();
    
    // BGMを再生する
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(PLAY_BGM, true);
    
    return true;
}

/**
 * @brief	シーンの生成
 * @return	シーン
 */
Scene* PlayScene::createScene()
{
    // シーンの生成
    Scene *scene = Scene::create();
    
    // レイヤーの生成
    PlayScene *layer = PlayScene::create();
    
    // シーンの子にレイヤーを追加
    scene->addChild(layer);
    
    // シーンを返す
    return scene;
}

/**
 * @brief   更新処理
 */
void PlayScene::update(float delta)
{
    // タイマーを更新
    GameTimer::update();
    
    // ステージを更新
    mStage->update(GameTimer::getElapsedTime());
    
    // ストップウォッチを更新
    mStopWatch->update();
    
//    // タイムラベル
//    std::string str = this->createTimeString(stopWatch->getTime());
//    mTimeLabel->setString(str);
    
    // ステージクリアしている場合、シーン遷移させる
    if(!mIsChange && mStage->getStageClearFlag())
    {
        mIsChange = true;
        this->replaceScene();
    }
}

/**
 * @brief タッチされたときの処理
 */
bool PlayScene::onTouchBegan(Touch* touch, Event* event)
{
    if(mDashButton->isTouch(touch->getLocation()))
    {
        log("DashMenu >> requestDash");
        GameManager::getInstance()->getCharacter()->requestDash();
    }
    if(mWalkButton->isTouch(touch->getLocation()))
    {
        log("DashMenu >> requestWalk");
        GameManager::getInstance()->getCharacter()->requestWalk();
    }
    if(mWaitButton->isTouch(touch->getLocation()))
    {
        log("DashMenu >> requestWait");
        GameManager::getInstance()->getCharacter()->requestWait();
    }

    
    return true;
}

/**
 * @brief タッチ処理が終わったときの処理
 */
void PlayScene::onTouchEnded(Touch *touch, Event *event)
{
    log("Touch >> Released");
    //GameManager::getInstance()->getCharacter()->requestWait();
}
/**
 * @brief	時間の文字列を生成する
 * @param	time	時間(f)
 * @return	生成された文字列
 */
std::string PlayScene::createTimeString(int time)
{
    char p[100] = {};
    int second = this->convertTimeToSecond(time);
    int minute = this->convertTimeToMinute(time);
    
    // 99分を超えた場合
    if(minute > 99)
    {
        std::string unknown = "- - : - -";
        return unknown;
    }
    sprintf(p, "%02d:%02d", minute, second);
    std::string str = p;
    return str;
}

/**
 * @brief	時間を秒に変換
 * @param	time	時間(f)
 */
int PlayScene::convertTimeToSecond(int time)
{
    return time / 60 % 60;
}

/**
 * @brief	時間を分に変換
 * @param	time	時間(f)
 */
int PlayScene::convertTimeToMinute(int time)
{
    return time / 60 / 60;
}