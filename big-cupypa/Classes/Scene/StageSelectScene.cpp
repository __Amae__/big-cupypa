#include "StageSelectScene.h"
#include "SceneFactory.h"
#include "GameParameter.h"

USING_NS_CC;

namespace
{
	/**
	 * @brief ノード
	 */
	enum NodeDepth
	{
        NODEDEPTH_BACKGROUND,	// 背景のノード
	};
    
    /**
     * @brief	ステージ名
     */
    const char* STAGE_NAME[] = {
		"MONO",
		"FACTORY",
		"CITY",
		"UNIVERSE",
		"COLDSLEEP",
		"CLOUD",
		"SILKTOUCH",
		"HELL",
		"STRING",
		"SPARK",
		"HOGE",
		"HOEE"
    };

    
    // 次のシーンID
	const SceneId NEXT_SCENE = PLAY_SCENE;
    
    // タイトルロゴのファイル名
	const char* BACKGROUND_FILENAME = "DarkWooden.png";
    
	// 背景の切り替わる時間
	const float BACKGROUND_DURATION = 30.0f;
    
	// 背景のフェード持続時間
	const float BACKGROUND_FADEDURATION = 0.0f;
    
	// トランジションの持続時間
	const float TRANSITION_DURATION = 1.0f;
}

/**
 * @brief シーンの交換
 */
void StageSelectScene::replaceScene()
{
    // すべてリスナーの削除
    this->getEventDispatcher()->removeAllEventListeners();
    
	// 次のシーンの生成
	Scene* nextScene = SceneFactory::createScene(NEXT_SCENE);
    
	//
	nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
    
	// シーンの交換
	Director::getInstance()->replaceScene(nextScene);
}

/**
 * @brief	初期化処理
 * @return	true  成功
 * @return	false 失敗
 */
bool StageSelectScene::init()
{
    // 親レイヤーの初期化
	if(!Layer::init())
	{
		return false;
	}
    
    // リスナーの生成
	auto listener = EventListenerTouchOneByOne::create();
    
    // イベントの設定
    listener->onTouchBegan = CC_CALLBACK_2(StageSelectScene::onTouchBegan, this);
    
    // リスナーの優先度の設定
    this->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
    
    Sprite* backGround = Sprite::create(BACKGROUND_FILENAME);
    
    // 座標を設定する
	backGround->setPosition(GameParameter::SCREEN_CENTER);
    
    this->addChild(backGround);
    
    mStartButton = Button::create("START.png");
    mStartButton->setPosition(Point(120, 40));
    this->addChild(mStartButton);
    
    Rect configRect = Rect(0.0f, 0.0f, 32.0f, 32.0f);
    mConfigButton = Button::create("Icons.png", &configRect);
	mConfigButton->setPosition(480.0f - (32.0f * 3), 16.0f);
    this->addChild(mConfigButton);
    
    for(int i = 0; i < 12; i++)
    {
        // 画像の名前
        std::stringstream filename;
        filename << "Stage" << (i + 1) << "Image.png";
        
        mStageButton[i] = Button::create(filename.str().c_str());
        mStageButton[i]->setScale(0.62f, 0.62f);
        mStageButton[i]->setPosition(Point(280 + (i % 3 * 80), 290 - ( i / 3 * 70)));
        
        this->addChild(mStageButton[i]);
    }
    
    // 画像の名前
    std::stringstream filename;
    filename << "Stage" << (0 + 1) << "Image.png";
    mSelectStage = Sprite::create();
    mSelectStage->setTexture(filename.str().c_str());
    mSelectStage->setPosition(Point(122.0f, 195.0f));
    mSelectStage->setScale(1.5f, 1.5f);
    this->addChild(mSelectStage);
    
    // ステージ名ラベル
    mStageName = Label::create(STAGE_NAME[0], "arial", 30);
    mStageName->setPosition(Point(122.0f, 290.0f));
    this->addChild(mStageName);
    
    return true;
}

/**
 * @brief	シーンの生成
 * @return	シーン
 */
Scene* StageSelectScene::createScene()
{
    // シーンの生成
    Scene *scene = Scene::create();
    
    // レイヤーの生成
    StageSelectScene *layer = StageSelectScene::create();
    
    // シーンの子にレイヤーを追加
    scene->addChild(layer);
    
    // シーンを返す
    return scene;

}

/**
 * @brief タッチされたときの処理
 */
bool StageSelectScene::onTouchBegan(Touch* touch, Event* event)
{
    for(int i = 0; i < 12; i++)
    {
        // ステージボタンが押されたときの処理
        if(mStageButton[i]->isTouch(touch->getLocation()))
        {
            // 画像の名前
            std::stringstream filename;
            filename << "Stage" << (i + 1) << "Image.png";
            
            // 画像の変更
            mSelectStage->setTexture(filename.str().c_str());
            
            // ステージ名の変更
            mStageName->setString(STAGE_NAME[i]);
            
            break;
        }
    }
    
    if(mStartButton->isTouch(touch->getLocation()))
    {
        // シーンの交換
        this->replaceScene();
    }
    
    if(mConfigButton->isTouch(touch->getLocation()))
    {
        // すべてリスナーの削除
        this->getEventDispatcher()->removeAllEventListeners();
        
        // 次のシーンの生成
        Scene* nextScene = SceneFactory::createScene(CONFIG_SCENE);
        
        //
        nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
        
        // シーンの交換
        Director::getInstance()->replaceScene(nextScene);
    }
    return true;
}
