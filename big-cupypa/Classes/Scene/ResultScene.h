#ifndef __CUPYPA_RESULT_SCENE_H__
#define __CUPYPA_RESULT_SCENE_H__

#include "cocos2d.h"

/**
 * @brief リザルトシーン
 */
class ResultScene : public cocos2d::Layer
{
private:
    
    //
    cocos2d::Label* mNextButton;
    
    // ステージ
    int mStage;
    
    //　タイム
    int mTime;
    
    // ベストタイム
    int mBestTime;
    
    // テンプタイム
    int mTempTime;
    
    /**
	 * @brief シーンの交換
	 */
	void replaceScene();
    
    /**
	 * @brief	シーン遷移メニューのコールバック
	 * @param	pSender	シーン遷移メニュー
	 */
	void callBackReplaceMenu(cocos2d::Ref* pSender);
    
	/**
	 * @brief	時間を秒に変換
	 * @param	time	時間(f)
	 */
	int convertTimeToSecond(int time);
    
	/**
	 * @brief	時間を分に変換
	 * @param	time	時間(f)
	 */
	int convertTimeToMinute(int time);
    
	/**
	 * @brief	スケールアクションの生成
	 * @return 	生成されたアクション
	 */
	cocos2d::ActionInterval* createScaleAction();
    
	/**
	 * @brief	時間の文字列を生成する
	 * @param	time	時間(f)
	 * @return	生成された文字列
	 */
	std::string createTimeString(int time);
    
	/**
	 * @brief	背景の初期化
	 */
	void initBackground();
    
	/**
	 * @brief	ベストタイムの初期化
	 */
	void initBestTime();
    
	/**
	 * @brief	ベストタイムラベルの初期化
	 */
	void initBestTimeLabel();
    
	/**
	 * @brief	ラベルの初期化
	 */
	void initLabel();
    
	/**
	 * @brief	ニューレコードの初期化
	 */
	void initNewRecord();
    
	/**
	 * @brief	シーン遷移をするメニューの初期化
	 */
	void initReplaceMenu();
    
	/**
	 * @brief	リザルトラベルの初期化
	 */
	void initResultLabel();
    
	/**
	 * @brief	テンプタイムの初期化
	 */
	void initTempTime();
    
	/**
	 * @brief	タイムの初期化
	 */
	void initTime();
    
	/**
	 * @brief	タイムラベルの初期化
	 */
	void initTimeLabel();
    
	/**
	 * @brief	次のステージへ進む
	 */
	void nextStage();
    
	/**
	 * @brief	更新
	 */
	void update(float delta);
    
	/**
	 * @brief	タイム初期化の更新
	 */
	void updateInitTime();
    
	/**
	 * @brief	ベストタイム初期化の更新
	 */
	void updateInitBestTime();
    
	/**
	 * @brief	ベストタイム更新の更新
	 */
	void updateUpdateBestTime();
    
	/**
	 * @brief	エンドの更新
	 */
	void updateEnd();
    
public:
    
	/**
	 * @brief	シーンの生成
	 * @return	シーンのアドレス
	 */
	CREATE_FUNC(ResultScene);
    
	/**
	 * @brief	初期化処理
	 * @retval	true  成功
	 * @retval	false 失敗
	 */
	bool init();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene();
    
	/**
	 * @brief タッチされたときの処理
	 */
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    
    
};

#endif // __CUPYPA_RESULT_SCENE_H__
