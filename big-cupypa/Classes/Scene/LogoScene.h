#ifndef __CUPYPA_LOGO_SCENE_H__
#define __CUPYPA_LOGO_SCENE_H__

#include "cocos2d.h"

/**
 * @brief ロゴシーン
 */
class LogoScene : public cocos2d::Layer
{
private:
    
	/**
	 * @brief チームロゴの初期化
	 */
	void initTeamLogo();
    
	/**
	 * @brief シーンの交換
	 */
	void replaceScene();
    
public:
    
	/**
	 * @brief	シーンの生成
	 * @return	シーンのアドレス
	 */
	CREATE_FUNC(LogoScene);
    
	/**
	 * @brief	初期化処理
	 * @retval	true  成功
	 * @retval	false 失敗
	 */
	bool init();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene();
    
	/**
	 * @brief タッチされたときの処理
	 */
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
};

#endif // __CUPYPA_LOGO_SCENE_H__
