#include "LogoScene.h"
#include "SceneFactory.h"

USING_NS_CC;

namespace
{
	/**
	 * @brief ノード
	 */
	enum NodeDepth
	{
		NODEDEPTH_TEAMLOGO,	// チームロゴのノード
	};
    
	// チームロゴのファイル名
	const char* TEAMLOGO_FILENAME = "TeamLogo.png";
    
	// チームロゴの表示位置
	const Point TEAMLOGO_POSITION = Point(240.0f,160.0f);
    
	// チームロゴのアクションの持続時間
	const float TEAMLOGO_ACTION_DURATION = 3.0f;
    
	// チームロゴの延滞時間
	const float TEAMLOGO_DELAY_DURATION = 1.0f;
    
	// 次のシーンID
	const SceneId NEXT_SCENE = TITLE_SCENE;
    
	// 透過時間
	const float TRANSITION_DURATION = 1.0f;
}

/**
 * @brief チームロゴの初期化
 */
void LogoScene::initTeamLogo()
{
	// スプライトの生成
	Sprite* teamLogo = Sprite::create(TEAMLOGO_FILENAME);
    
	// 表示座標の設定
	teamLogo->setPosition(TEAMLOGO_POSITION);
    
	//
	teamLogo->setOpacity(0);
    
	// アクションの生成
	ActionInterval* action1 = FadeIn::create(TEAMLOGO_ACTION_DURATION);
	DelayTime* action2 = DelayTime::create(TEAMLOGO_DELAY_DURATION);
	ActionInterval* action3 = action1->reverse();
	CallFunc* actionDone = CallFunc::create([this]() { this->replaceScene(); } );
    
	// アクションの実行
	teamLogo->runAction(Sequence::create(action1, action2, action3, actionDone, NULL));
    
	// スプライトをレイヤーの子に追加
	this->addChild(teamLogo, NODEDEPTH_TEAMLOGO);
}

/**
 * @brief シーンの交換
 */
void LogoScene::replaceScene()
{
    // すべてリスナーの削除
    this->getEventDispatcher()->removeAllEventListeners();
    
	// 次のシーンの生成
	Scene* nextScene = SceneFactory::createScene(NEXT_SCENE);
    
	//
	nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
    
	// シーンの交換
	Director::getInstance()->replaceScene(nextScene);
}

/**
 * @brief  初期化処理
 * @retval true  成功
 * @retval false 失敗
 */
bool LogoScene::init()
{
	// 親レイヤーの初期化
	if(!Layer::init())
	{
		return false;
	}
    
    // リスナーの生成
	auto listener = EventListenerTouchOneByOne::create();
    
    // イベントの設定
    listener->onTouchBegan = CC_CALLBACK_2(LogoScene::onTouchBegan, this);
    
    // リスナーの優先度の設定
    this->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
    
	// チームロゴの初期化
	this->initTeamLogo();
    
	return true;
}

/**
 * @brief	シーンの生成
 * @return	シーン
 */
Scene* LogoScene::createScene()
{
    // シーンの生成
    Scene *scene = Scene::create();
    
    // レイヤーの生成
    LogoScene *layer = LogoScene::create();
    
    // シーンの子にレイヤーを追加
    scene->addChild(layer);
    
    // シーンを返す
    return scene;
}

/**
 * @brief タッチされたときの処理
 */
bool LogoScene::onTouchBegan(Touch* touch, Event* event)
{
	// シーンの交換
	this->replaceScene();
    
	return true;
}