#ifndef __CUPYPA_STAGE_SELECT_SCENE_H__
#define __CUPYPA_STAGE_SELECT_SCENE_H__

#include "cocos2d.h"
#include "Button.h"

/**
 * @brief ステージセレクトシーン
 */
class StageSelectScene : public cocos2d::Layer
{
private:
    
    /**
	 * @brief シーンの交換
	 */
	void replaceScene();
    
    Button* mStartButton;
    
    Button* mConfigButton;
    
    Button*  mStageButton[12];
    
    cocos2d::Sprite* mSelectStage;
    
    cocos2d::Label* mStageName;
    
public:
    
	/**
	 * @brief	シーンの生成
	 * @return	シーンのアドレス
	 */
	CREATE_FUNC(StageSelectScene);
    
	/**
	 * @brief	初期化処理
	 * @retval	true  成功
	 * @retval	false 失敗
	 */
	bool init();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene();
    
	/**
	 * @brief タッチされたときの処理
	 */
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    
};

#endif // __CUPYPA_STAGE_SELECT_SCENE_H__
