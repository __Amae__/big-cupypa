#ifndef __CUPYPA_SCENE_FACTORY_H__
#define __CUPYPA_SCENE_FACTORY_H__

#include "cocos2d.h"
#include "SceneId.h"

/**
 * @brief シーンファクトリー
 */
class SceneFactory
{
private:
    
	/**
	 * @brief コンストラクタ
	 * @note  インスタンス禁止
	 */
	SceneFactory();
    
public:
	
    /**
	 * @brief	シーンの生成
	 * @param	シーンID
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene(SceneId sceneId);
};

#endif // __CUPYPA_SCENE_FACTORY_H__
