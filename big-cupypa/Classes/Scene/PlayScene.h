#ifndef __CUPYPA_PLAY_SCENE_H__
#define __CUPYPA_PLAY_SCENE_H__

#include "cocos2d.h"

// ステージ
class Stage;

//
class Button;

class StopWatch;

/**
 * @brief プレイシーン
 */
class PlayScene : public cocos2d::Layer
{
private:
    
    /**
	 * @brief シーンの交換
	 */
	void replaceScene();
    
    /**
     * @brief   コンストラクタ
     */
    PlayScene();
    
    /**
     * @brief   解放処理
     */
    void finalize();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    ~PlayScene();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーンのアドレス
	 */
	CREATE_FUNC(PlayScene);
    
	/**
	 * @brief	初期化処理
	 * @retval	true  成功
	 * @retval	false 失敗
	 */
	bool init();
    
    /**
     * @brief   更新処理
     */
    void update(float delta);
    
	/**
	 * @brief	シーンの生成
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene();
    
	/**
	 * @brief タッチされたときの処理
	 */
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    
    /**
     * @brief タッチ処理が終わったときの処理
     */
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    /**
	 * @brief	時間の文字列を生成する
	 * @param	time	時間(f)
	 * @return	生成された文字列
	 */
	std::string createTimeString(int time);
    
    /**
	 * @brief	時間を秒に変換
	 * @param	time	時間(f)
	 */
	int convertTimeToSecond(int time);
    
	/**
	 * @brief	時間を分に変換
	 * @param	time	時間(f)
	 */
	int convertTimeToMinute(int time);
    
private:
    // ステージ
    Stage* mStage;
    
    // ダッシュボタン
    Button* mDashButton;
    
    // 歩くボタン
    Button* mWalkButton;
    
    // 待機ボタン
    Button* mWaitButton;
    
    // ストップウォッチ
    StopWatch* mStopWatch;
    
    // タイムラベル
    cocos2d::Label* mTimeLabel;
    
    // ステージ切り換え
    bool mIsChange;
};

#endif // __CUPYPA_PLAY_SCENE_H__
