#include "ConfigScene.h"
#include "SceneFactory.h"
#include "GameParameter.h"

USING_NS_CC;

namespace {
    
    /**
     * @brief   ノード
     */
    enum NodeDepth
    {
        NODEDEPTH_BACKGROUND,   // 背景のノード
    };
    
    // 次のシーンID
    const SceneId NEXT_SCENE = STAGESELECT_SCENE;
    
    // 背景の切り替わる時間
    const float BACKGROUND_DURATION = 30.0f;
    
    // 背景のフェード持続時間
    const float BACKGROUND_FADEDURATION = 0.0f;
    
    // トランジションの持続時間
    const float TRANSITION_DURATION = 1.0f;
}

/**
 * @brief   シーンの交換
 */
void ConfigScene::replaceScene()
{
    // すべてのリスナーの削除
    this->getEventDispatcher()->removeAllEventListeners();
    
    // 次のシーンの生成
    Scene* nextScene = SceneFactory::createScene(NEXT_SCENE);
    
    //
    nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
    
    // シーンの交換
    Director::getInstance()->replaceScene(nextScene);
}

/**
 * @brief   初期化処理
 * @return  true 成功
 * @return  false 失敗
 */
bool ConfigScene::init()
{
    // 親レイヤーの初期化
    if(!Layer::init())
    {
        return false;
    }
    
    // リスナーの生成
    auto listener = EventListenerTouchOneByOne::create();
    
    // イベントの設定
    listener->onTouchBegan = CC_CALLBACK_2(ConfigScene::onTouchBegan, this);
    
    // リスナーの優先度の設定
    this->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
    
    // ラベルの初期化
	this->initLabel();
    
	// BGMスライダーの初期化
	//this->initBGMSlider();
    
	// SEスライダーの初期化
	//this->initSESlider();
    
	// スムージングスイッチの初期化
	//this->initSmoothingSwitch();
    
	// メニューの初期化
	this->initBackMenu();
    
    return true;
}

/**
 * @brief   シーンの生成
 * @return  シーン
 */
Scene* ConfigScene::createScene()
{
    // シーンの生成
    Scene *scene = Scene::create();
    
    // レイヤーの生成
    ConfigScene *layer = ConfigScene::create();
    
    // シーンの子にレイヤーを追加
    scene->addChild(layer);
    
    // シーンを返す
    return scene;
}

/**
 * @brief   タッチされたときの処理
 */
bool ConfigScene::onTouchBegan(Touch* touch, Event* event)
{
    if(mBackButton->isTouch(touch->getLocation()))
    {
        // シーンの交換
        this->replaceScene();
    }
    return true;
}

/**
 * @brief ラベルの初期化
 */
void ConfigScene::initLabel()
{
    // シーン名のラベル
    Label* sceneName = Label::create("CONFIG", "arial", 40);
    sceneName->setPosition(Point(240.0f, 270.0f));
    this->addChild(sceneName);
}

/**
 * @brief BGMスライダーの初期化
 */
void ConfigScene::initBGMSlider()
{
    
}

/**
 * @brief SEスライダーの初期化
 */
void ConfigScene::initSESlider()
{
    
}

/**
 * @brief スムージングスイッチの初期化
 */
void ConfigScene::initSmoothingSwitch()
{
    
}

/**
 * @brief メニューの初期化
 */
void ConfigScene::initBackMenu()
{
    Rect backRect = Rect(128.0f, 0.0f, 32.0f, 32.0f);
    mBackButton = Button::create("Icons.png", &backRect);
    mBackButton->setPosition(Point(400.0f, 32.0f));
    this->addChild(mBackButton);
}

