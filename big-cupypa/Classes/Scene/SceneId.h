#ifndef __CUPYPA_SCENE_ID_H__
#define __CUPYPA_SCENE_ID_H__

/**
 *  シーンのID
 */
enum SceneId
{
    EMPTY_SCENE = -1,
    LOGO_SCENE,
    TITLE_SCENE,
    STAGESELECT_SCENE,
    PLAY_SCENE,
    RESULT_SCENE,
    CONFIG_SCENE,
};


#endif // __CUPYPA_SCENE_ID_H__
