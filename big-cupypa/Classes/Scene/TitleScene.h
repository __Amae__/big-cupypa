#ifndef __CUPYPA_TITLE_SCENE_H__
#define __CUPYPA_TITLE_SCENE_H__

#include "cocos2d.h"

/**
 * @brief タイトルシーン
 */
class TitleScene : public cocos2d::Layer
{
private:
    
    /**
     * @brief   コンストラクタ
     */
    TitleScene();
    
    /**
     * @brief   解放処理
     */
    void finalize();
    
	/**
	 * @brief シーンの交換
	 */
	void replaceScene();
    
    /**
	 * @brief	タイトルラベルの初期化
	 */
	void initTitleLabel();
    
	/**
	 * @brief	タイトルロゴの初期化
	 */
	void initTitleLogo();
    
    /**
	 * @brief	ブロック群の初期化
	 */
	void initBlocks();
    
    /**
	 * @brief	ブロック群の更新
	 */
	void updateBlocks();
    
    /**
	 * @brief	背景の初期化
	 */
	void initBackground();
    
    /**
     * @brief   BGMの初期化
     */
    void initBGM();
    
    /**
	 * @brief	背景アクションを生成
	 * @return	背景アクション
	 */
	cocos2d::ActionInterval* createBackgroundAction();
    
    /**
	 * @brief	背景を切り替える
	 */
	void replaceBackground();
    
    /**
	 * @brief	揺れるアクションの生成
	 * @return	生成されたアクション
	 */
	cocos2d::ActionInterval* createShakeAction();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    ~TitleScene();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーンのアドレス
	 */
	CREATE_FUNC(TitleScene);
    
	/**
	 * @brief	初期化処理
	 * @retval	true  成功
	 * @retval	false 失敗
	 */
	bool init();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene();
    
	/**
	 * @brief タッチされたときの処理
	 */
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
};


#endif // __CUPYPA_TITLE_SCENE_H__