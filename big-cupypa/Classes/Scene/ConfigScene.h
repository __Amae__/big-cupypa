#ifndef __CUPYPA_CONFIG_SCENE_H__
#define __CUPYPA_CONFIG_SCENE_H__

#include "cocos2d.h"
#include "Button.h"

/**
 * @brief コンフィグシーン
 */
class ConfigScene : public cocos2d::Layer
{
private:
    
    /**
	 * @brief シーンの交換
	 */
	void replaceScene();
    
    /**
     * @brief ラベルの初期化
     */
    void initLabel();
    
    /**
     * @brief BGMスライダーの初期化
     */
    void initBGMSlider();
    
    /**
     * @brief SEスライダーの初期化
     */
    void initSESlider();
    
    /**
     * @brief スムージングスイッチの初期化
     */
    void initSmoothingSwitch();
    
    /**
     * @brief メニューの初期化
     */
    void initBackMenu();
    
    Button* mBackButton;
    
public:
    
	/**
	 * @brief	シーンの生成
	 * @return	シーンのアドレス
	 */
	CREATE_FUNC(ConfigScene);
    
	/**
	 * @brief	初期化処理
	 * @retval	true  成功
	 * @retval	false 失敗
	 */
	bool init();
    
	/**
	 * @brief	シーンの生成
	 * @return	シーン
	 */
	static cocos2d::Scene* createScene();
    
	/**
	 * @brief タッチされたときの処理
	 */
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
};

#endif // __CUPYPA_CONFIG_SCENE_H__