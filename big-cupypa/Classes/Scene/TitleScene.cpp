#include "TitleScene.h"
#include "SceneFactory.h"
#include "GameManager.h"
#include "GameParameter.h"

#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

namespace
{
	/**
	 * @brief ノード
	 */
	enum NodeDepth
	{
        NODEDEPTH_BACKGROUND,	// 背景のノード
		NODEDEPTH_CUPY,         // カピーさんのノード
		NODEDEPTH_BLOCKS,		// ブロック群のノード
		NODEDEPTH_TITLELABEL,	// タイトルラベルのノード
		NODEDEPTH_TITLELOGO,	// タイトルロゴのノード
		NODEDEPTH_CLOSEMENU,	// 終了メニューのノード
	};
    
    /**
	 * @brief	ノードタグ
	 */
	enum NodeTag
	{
		NODETAG_BACKGROUND,	// 背景のタグ
		NODETAG_BLOCKS,		// ブロック群のタグ
	};
    
    // 次のシーンID
    //const SceneId NEXT_SCENE = PLAY_SCENE;
    const SceneId NEXT_SCENE = STAGESELECT_SCENE;
    // タイトルロゴのファイル名
	const char* TITLELOGO_FILENAME = "TitleLogo.png";
    
	// 背景の切り替わる時間
	const float BACKGROUND_DURATION = 30.0f;
    
	// 背景のフェード持続時間
	const float BACKGROUND_FADEDURATION = 0.0f;
    
	// ブロック群のファイル名
	const char* BLOCKS_FILENAME = "TitleBlocks.png";
    
	// ブロック群の座標
	const Point BLOCKS_POSITION = GameParameter::SCREEN_CENTER;
    
	// ブロック群のスライドの速さ
	const float BLOCKS_SPEED = 3.1f;
    
	// 点滅の持続時間
	const float BLINK_DURATION = 0.1f;
    
	// 点滅の回数
	const int BLINK_NUM = 2;
    
	// 点滅のクールダウン
	const float BLINK_COOLDOWN = 5.0f;
    
	// カピーさんの座標
	const Point CUPY_POSITION = Point(112.0f, 81.0f);
    
	// タイトル画面で行うアクション数
	const int CUPY_NUMACTIONS = 3;
    
	// 揺れの持続時間
	const float SHAKE_DURATION = 6.0f;
    
	// 揺れ幅
	const float SHAKE_WIDTH = 12.0f;
    
	// 揺れの緩急
	const float SHAKE_HIGHANDLOWSPEED = 1.75f;
    
	// タイトルラベルの座標
	const Point TITLELABEL_POSITION = Point(240.0f, 110.0f);
    
	// タイトルラベルの文字列
	const char* TITLELABEL_STRING = "TOUCH THE SCREEN";
    
    // BGMファイル名
    const char* TITLE_BGM = "crazy_cat.mp3";
    
	// タイトルラベルのサイズ
	const int TITLELABEL_SIZE = 22;
    
	// タイトルラベルのフェードの持続時間
	const float TITLELABEL_FADEDURATION = 1.0f;
    
	// タイトルロゴの座標
	const Point TITLELOGO_POSITION = Point(240.0f, 200.0f);
    
	// タイトルロゴのスケール
	const float TITLELOGO_SCALE = 1.4f;
    
	// トランジションの持続時間
	const float TRANSITION_DURATION = 1.0f;
}

/**
 * @brief デフォルトコンストラクタ
 */
TitleScene::TitleScene()
{
}

/**
 * @brief デストラクタ
 */
TitleScene::~TitleScene()
{
}

/**
 * @brief   解放処理
 */
void TitleScene::finalize()
{
    // BGM停止
    SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    // 解放
    SimpleAudioEngine::getInstance()->end();
}

/**
 * @brief シーンの交換
 */
void TitleScene::replaceScene()
{
    // 後始末処理
	this->finalize();
    
    // すべてリスナーの削除
    this->getEventDispatcher()->removeAllEventListeners();
    
	// 次のシーンの生成
	Scene* nextScene = SceneFactory::createScene(NEXT_SCENE);
    
	// シーン遷移のアクションを設定
	nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
    
	// シーンの交換
	Director::getInstance()->replaceScene(nextScene);
}

/**
 * @brief  初期化処理
 * @return true  成功
 * @return false 失敗
 */
bool TitleScene::init()
{
	// 親レイヤーの初期化
	if(!Layer::init())
	{
		return false;
	}
    
    // リスナーの生成
	auto listener = EventListenerTouchOneByOne::create();
    
    // イベントの設定
    listener->onTouchBegan = CC_CALLBACK_2(TitleScene::onTouchBegan, this);
    
    // リスナーの優先度の設定
    this->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
    
    // カピーさんの初期化
	//this->initCupy();
    
	// 背景の初期化
	//this->initBackground();
    
	// BGMの初期化
	//this->initBGM();
    
	// ブロック群の初期化
	this->initBlocks();
    
	// タイトルラベルの初期化
	this->initTitleLabel();
    
	// タイトルロゴの初期化
	this->initTitleLogo();
    
	// 終了メニューの初期化
	//this->initCloseMenu();
    
	return true;
}

/**
 * @brief	シーンの生成
 * @return	シーン
 */
Scene* TitleScene::createScene()
{
    // シーンの生成
    Scene *scene = Scene::create();
    
    // レイヤーの生成
    TitleScene *layer = TitleScene::create();
    
    // シーンの子にレイヤーを追加
    scene->addChild(layer);
    
    // シーンを返す
    return scene;
}

/**
 * @brief タッチされたときの処理
 */
bool TitleScene::onTouchBegan(Touch* touch, Event* event)
{
	// シーンの交換
	this->replaceScene();
    
	return true;
}

/**
 * @brief	タイトルラベルの初期化
 */
void TitleScene::initTitleLabel()
{
    // タイトルラベルを生成する
	Label* titleLabel = GameManager::createCyberLabel(TITLELABEL_STRING, TITLELABEL_SIZE);
    
	// タイトルラベルの座標を設定する
	titleLabel->setPosition(TITLELABEL_POSITION);
    
	// タイトルラベルのアクションを生成して実行する
	FadeIn* action1 = FadeIn::create(TITLELABEL_FADEDURATION);
	FadeOut* action2 = FadeOut::create(TITLELABEL_FADEDURATION);
	Sequence* action = Sequence::create(action1, action2, NULL);
	titleLabel->runAction(RepeatForever::create(action));
    
	// タイトルラベルをシーンに追加する
	this->addChild(titleLabel, NODEDEPTH_TITLELABEL);
}

/**
 * @brief	タイトルロゴの初期化
 */
void TitleScene::initTitleLogo()
{
    // タイトルロゴのスプライトを生成する
	Sprite* titleLogo = Sprite::create(TITLELOGO_FILENAME);
    
	// タイトルロゴの座標を設定する
	titleLogo->setPosition(TITLELOGO_POSITION);
    
	// タイトルロゴのスケールを設定する
	titleLogo->setScale(TITLELOGO_SCALE);
    
	// アクションを実行する
	titleLogo->runAction(this->createShakeAction());
    
	// シーンに追加する
	this->addChild(titleLogo, NODEDEPTH_TITLELOGO);
}

/**
 * @brief	ブロック群の初期化
 */
void TitleScene::initBlocks()
{
    // ブロック群を生成する
	Sprite* blocks = Sprite::create(BLOCKS_FILENAME, Rect(0, 0, 480, 320));
    
	// テクスチャパラメータを設定する
    Texture2D::TexParams tp = { GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
	blocks->getTexture()->setTexParameters(&tp);
    
	// 座標を設定する
	blocks->setPosition(GameParameter::SCREEN_CENTER);
    
	// シーンに追加する
	this->addChild(blocks, NODEDEPTH_BLOCKS, NODETAG_BLOCKS);
    
	// 更新をスケジュールする
	//this->schedule(schedule_selector(TitleScene::updateBlocks), GameDefs::ANIMATION_INTERVAL);
}

/**
 * @brief	ブロック群の更新
 */
void TitleScene::updateBlocks()
{
    // テクスチャのレクトを更新する
	Sprite* blocks = static_cast<Sprite*>(this->getChildByTag(NODETAG_BLOCKS));
	Rect rect = blocks->getTextureRect();
	rect.origin.x += BLOCKS_SPEED;
	if(rect.origin.x >= 512.0f)
    {
        rect.origin.x -= 512.0f;
    }
    blocks->setTextureRect(rect);
}

/**
 * @brief	背景の初期化
 */
void TitleScene::initBackground()
{
    // 背景を生成
	this->replaceBackground();
}

/**
 * @brief   BGMの初期化
 */
void TitleScene::initBGM()
{
    // BGMの読み込み
    SimpleAudioEngine::getInstance()->preloadBackgroundMusic(TITLE_BGM);
    // BGMの再生開始
    SimpleAudioEngine::getInstance()->playBackgroundMusic(TITLE_BGM, true);

}

/**
 * @brief	背景アクションを生成
 * @return	背景アクション
 */
ActionInterval* TitleScene::createBackgroundAction()
{
    FadeIn* fadeIn = FadeIn::create(BACKGROUND_FADEDURATION);
	DelayTime* delay = DelayTime::create(BACKGROUND_DURATION);
	FadeOut* fadeOut = FadeOut::create(BACKGROUND_FADEDURATION);
	CallFunc* actionDone = CallFunc::create([this]() { this->replaceBackground(); } );
	Sequence* action = Sequence::create(fadeIn, delay, fadeOut, actionDone, NULL);
	return action;

}

/**
 * @brief	背景を切り替える
 */
void TitleScene::replaceBackground()
{
}

cocos2d::ActionInterval* TitleScene::createShakeAction()
{
    
	EaseInOut* rotateLeft = EaseInOut::create(RotateTo::create(SHAKE_DURATION, SHAKE_WIDTH * 0.5f), SHAKE_HIGHANDLOWSPEED);
	EaseInOut* rotateRight = EaseInOut::create(RotateTo::create(SHAKE_DURATION, -SHAKE_WIDTH * 0.5f), SHAKE_HIGHANDLOWSPEED);
	Sequence* action = Sequence::create(rotateLeft, rotateRight, NULL);
    
	return RepeatForever::create(action);
   
}