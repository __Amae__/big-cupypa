#include "ResultScene.h"
#include "SceneFactory.h"
#include "GameParameter.h"
#include "GameManager.h"

USING_NS_CC;

namespace {
    
    // ノード深度
	enum NodeDepth
	{
		NODEDEPTH_BG,				// 背景
		NODEDEPTH_RESULTLABEL,		// リザルトラベル
		NODEDEPTH_TIMELABEL,		// タイムラベル
		NODEDEPTH_TIME,				// タイム
		NODEDEPTH_BESTTIMELABEL,	// ベストタイムラベル
		NODEDEPTH_BESTTIME,			// ベストタイム
		NODEDEPTH_REPLACEMENU,		// シーン遷移
		NODEDEPTH_NEWRECORD,		// ニューレコード
		NODEDEPTH_CLOSEMENU,		// クローズメニュー
	};
    
	// ノードタグ
	enum NodeTag
	{
		NODETAG_BG,				// 背景
		NODETAG_RESULTLABEL,	// リザルトラベル
		NODETAG_TIMELABEL,		// タイムラベル
		NODETAG_TIME,			// タイム
		NODETAG_BESTTIMELABEL,	// ベストタイムラベル
		NODETAG_BESTTIME,		// ベストタイム
		NODETAG_REPLACEMENU,	// シーン遷移
		NODETAG_NEWRECORD,		// ニューレコード
		NODETAG_CLOSEMENU,		// クローズメニュー
	};
    
    // ステージ
	enum Stage
	{
		STAGE_INITTIME,
		STAGE_UPDATEBESTTIME,
		STAGE_END,
	};

    // ベストタイムの座標
	const Point BESTTIME_POSITION = Point(360.0f, 120.0f);
    
	// ベストタイムラベルの座標
	const Point BESTTIMELABEL_POSITION = Point(120.0f, 120.0f);
    
	// リザルトラベルの座標
	const Point RESULTLABEL_POSITION = Point(160.0f, 240.0f);
    
	// シーン遷移をするメニューの座標
	const Point REPLACEMENU_POSITION = Point(360.0f, 60.0f);
    
	// タイムの座標
	const Point TIME_POSITION = Point(360.0f, 200.0f);
    
	// タイムラベルの座標
	const Point TIMELABEL_POSITION = Point(120.0f, 200.0f);

    // 次のシーンID
    const SceneId NEXT_SCENE = PLAY_SCENE;
    
    // タイトルロゴのファイル名
    const char* BACKGROUND_FILENAME = "DarkWooden.png";
    
    // 背景の切り替わる時間
    const float BACKGROUND_DURATION = 30.0f;
    
    // 背景のフェード持続時間
    const float BACKGROUND_FADEDURATION = 0.0f;
    
    // トランジションの持続時間
    const float TRANSITION_DURATION = 1.0f;
    
    // 時間に加算する速さ
	const int TIME_ADDSPEED = 360;
    
	// 時間の限界
	const int TIME_LIMITE = 359940;
}

/**
 * @brief   シーンの交換
 */
void ResultScene::replaceScene()
{
    // すべてのリスナーの削除
    this->getEventDispatcher()->removeAllEventListeners();
    
    // 次のシーンの生成
    Scene* nextScene = SceneFactory::createScene(NEXT_SCENE);
    
    //
    nextScene = TransitionZoomFlipAngular::create(TRANSITION_DURATION, nextScene);
    
    // シーンの交換
    Director::getInstance()->replaceScene(nextScene);
}

/**
 * @brief	シーン遷移メニューのコールバック
 * @param	pSender	シーン遷移メニュー
 */
void ResultScene::callBackReplaceMenu(Ref* pSender)
{
    this->replaceScene();
}

/**
 * @brief	時間を秒に変換
 * @param	time	時間(f)
 */
int ResultScene::convertTimeToSecond(int time)
{
    return time / 60 % 60;
}

/**
 * @brief	時間を分に変換
 * @param	time	時間(f)
 */
int ResultScene::convertTimeToMinute(int time)
{
    return time / 60 / 60;
}

/**
 * @brief	スケールアクションの生成
 * @return 	生成されたアクション
 */
ActionInterval* ResultScene::createScaleAction()
{
    ScaleTo* action1 = ScaleTo::create(0.3f, 1.2f);
	ScaleTo* action2 = ScaleTo::create(0.3f, 1.0f);
	DelayTime* action3 = DelayTime::create(0.2f);
	Sequence* action = Sequence::create(action1, action2, action3, NULL);
	return RepeatForever::create(action);
}

/**
 * @brief	時間の文字列を生成する
 * @param	time	時間(f)
 * @return	生成された文字列
 */
std::string ResultScene::createTimeString(int time)
{
    char p[100] = {};
    int second = this->convertTimeToSecond(time);
    int minute = this->convertTimeToMinute(time);
    
    // 99分を超えた場合
    if(minute > 99)
    {
        std::string unknown = "- - : - -";
        return unknown;
    }
    sprintf(p, "%02d:%02d", minute, second);
    std::string str = p;
    return str;
}

/**
 * @brief	背景の初期化
 */
void ResultScene::initBackground()
{
    // 背景の生成
    Sprite* background = Sprite::create(BACKGROUND_FILENAME);
    background->setPosition(GameParameter::SCREEN_CENTER);
    this->addChild(background);
}

/**
 * @brief	ベストタイムの初期化
 */
void ResultScene::initBestTime()
{
    std::string str = this->createTimeString(mBestTime);
    Label* label = Label::createWithSystemFont(str.c_str(), "arial", 40);
    label->setPosition(BESTTIME_POSITION);
    this->addChild(label, NODEDEPTH_BESTTIME, NODETAG_BESTTIME);
}

/**
 * @brief	ベストタイムラベルの初期化
 */
void ResultScene::initBestTimeLabel()
{
    // ベストクリアタイムラベル
    Label* bestClearTime = Label::create("BEST TIME", "arial", 35);
    bestClearTime->setPosition(BESTTIMELABEL_POSITION);
    this->addChild(bestClearTime);
}

/**
 * @brief	ラベルの初期化
 */
void ResultScene::initLabel()
{
    // リザルトラベルの初期化
    this->initResultLabel();
    
    // タイムラベルの初期化
    this->initTimeLabel();
    
    // ベストタイムラベルの初期化
    this->initBestTimeLabel();
    
    // タイムの初期化
    this->initTime();
    
    // ベストタイムの初期化
    this->initBestTime();
}

/**
 * @brief	ニューレコードの初期化
 */
void ResultScene::initNewRecord()
{
   	Label* label = Label::create("NEW RECORD", "arial", 32);
	Sprite* nr = Sprite::createWithTexture(label->getTexture());
	nr->setPosition(Point(416.0f, 256.0f));
	nr->setScale(0.0f);
	nr->setColor(Color3B(0,255,255));
	this->addChild(nr, NODEDEPTH_NEWRECORD, NODETAG_NEWRECORD);
    
	RotateBy* action1 = RotateBy::create(0.8f, 385.0f);
	ScaleTo* action2 = ScaleTo::create(0.8f, 0.7f);
	nr->runAction(Spawn::create(action1, action2, NULL));
}

/**
 * @brief	シーン遷移をするメニューの初期化
 */
void ResultScene::initReplaceMenu()
{
    
}

/**
 * @brief	リザルトラベルの初期化
 */
void ResultScene::initResultLabel()
{
    Label* label = Label::create("RESULT", "arial", 35);
    label->setPosition(RESULTLABEL_POSITION);
    this->addChild(label);
}

/**
 * @brief	テンプタイムの初期化
 */
void ResultScene::initTempTime()
{
    mTempTime = 0;
}

/**
 * @brief	タイムの初期化
 */
void ResultScene::initTime()
{
    std::string str = this->createTimeString(mTime);
    Label* label = Label::create(str.c_str(), "arial", 40);
    label->setPosition(TIME_POSITION);
    this->addChild(label, NODEDEPTH_TIME, NODETAG_TIME);
}

/**
 * @brief	タイムラベルの初期化
 */
void ResultScene::initTimeLabel()
{
    Label* label = Label::create("YOUR TIME", "arial", 35);
    label->setPosition(TIMELABEL_POSITION);
    this->addChild(label);
}

/**
 * @brief	次のステージへ進む
 */
void ResultScene::nextStage()
{
    mStage++;
}

/**
 * @brief	更新
 */
void ResultScene::update(float delta)
{
    switch(mStage)
    {
        case STAGE_INITTIME:
            this->updateInitTime();
            break;
        case STAGE_UPDATEBESTTIME:
            //this->updateUpdateBestTime();
            break;
        case STAGE_END:
            this->updateEnd();
            break;
    }
}

/**
 * @brief	タイム初期化の更新
 */
void ResultScene::updateInitTime()
{
	// タイムを表示するラベルを取得
	Label* label = static_cast<Label*>(this->getChildByTag(NODETAG_TIME));
    
	// 時間を加算していく
	mTempTime += TIME_ADDSPEED;
    
	// 加算しすぎた時は引き戻す
	mTempTime = MAX(0, MIN(mTime, mTempTime));
    
	// ラベルに文字列をセットする
	std::string str = this->createTimeString(mTempTime);
	label->setString(str.c_str());
    
	// 終わったら次のステージへと遷移
	if(mTempTime == mTime)
	{
		this->initTempTime();
		this->nextStage();
	}
}

/**
 * @brief	ベストタイム初期化の更新
 */
void ResultScene::updateInitBestTime()
{
	// タイムを表示するラベルを取得
	Label* label = static_cast<Label*>(this->getChildByTag(NODETAG_BESTTIME));
    
	// 時間を加算していく
	mTempTime += TIME_ADDSPEED;
    
	// 加算しすぎた時は引き戻す
	mTempTime = MAX(0, MIN(mBestTime, mTempTime));
    
	// ラベルに文字列をセットする
	std::string str = this->createTimeString(mTempTime);
	label->setString(str.c_str());
    
	// 終わったら次のステージへと遷移
	if(mTempTime == mBestTime)
	{
		this->initTempTime();
		this->nextStage();
	}
}

/**
 * @brief	ベストタイム更新の更新
 */
void ResultScene::updateUpdateBestTime()
{
	Label* label = static_cast<Label*>(this->getChildByTag(NODETAG_TIME));
	Label* blabel = static_cast<Label*>(this->getChildByTag(NODETAG_BESTTIME));
	std::string key = GameManager::getStringAndNumCombine("stage", "besttime", GameManager::getInstance()->getChosenStageId());
    
	// タイム更新できた時
	if(mTime <= mBestTime)
	{
		GameManager::setIntKey(key.c_str(), mTime);
		std::string str = this->createTimeString(mTime);
		blabel->setString(str.c_str());
        
		// タイムのアクションを実行する
		label->runAction(this->createScaleAction());
		blabel->runAction(this->createScaleAction());
        
		// ニューレコードの初期化
		this->initNewRecord();
	}
	// できなかった時
	else
	{
        
	}
    
	// シーン遷移をするメニューの初期化
	//this->initReplaceMenu();
    
	// 次のステージへ遷移
	this->nextStage();
}

/**
 * @brief	エンドの更新
 */
void ResultScene::updateEnd()
{
}

/**
 * @brief   初期化処理
 * @return  true 成功
 * @return  false 失敗
 */
bool ResultScene::init()
{
    // 親レイヤーの初期化
    if(!Layer::init())
    {
        return false;
    }
    
//    // 変数の初期化
//    mStage = STAGE_INITTIME;
//    mTime = 0;
//    mBestTime = 0;
//    mTempTime = 0;
//
//    // タイムの取得
//    mTime = MAX(0, MIN(TIME_LIMITE, GameManager::getInstance()->getTime()));
//    //mTime = MAX(0, MIN(TIME_LIMITE, 200));
//    
//    // ベストタイムの取得
//    std::string key = GameManager::getStringAndNumCombine("stage", "besttime", GameManager::getInstance()->getChosenStageId());
//    mBestTime = MAX(0, MIN(TIME_LIMITE, GameManager::getIntKey(key.c_str(), 99999999)));
//
//    // 背景の初期化
//    this->initBackground();
//    
//    // テンプタイムの初期化
//    this->initTempTime();
//    
//    // ラベル達の初期化
//    this->initLabel();
    
    this->initResultLabel();
    
    // リスナーの生成
    auto listener = EventListenerTouchOneByOne::create();
    
    // イベントの設定
    listener->onTouchBegan = CC_CALLBACK_2(ResultScene::onTouchBegan, this);
    
    // リスナーの優先度の設定
    this->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 100);
    
//    // シーン切り替えボタン
//    mNextButton = Label::create("NEXT", "arial", 40);
//    mNextButton->setPosition(Point(370, 50));
//    this->addChild(mNextButton);
    
    // アクションの生成
//	ActionInterval* action1 = FadeIn::create(3.0f);
//	DelayTime* action2 = DelayTime::create(1.0f);
//	ActionInterval* action3 = action1->reverse();
//	CallFunc* actionDone = CallFunc::create([this]() { this->replaceScene(); } );
//    
//	// アクションの実行
//	this->runAction(Sequence::create(action1, action2, action3, actionDone, NULL));
    
    // スケジュールに更新メソッドを指定
    //this->schedule(schedule_selector(ResultScene::update));
    
    return true;
}

/**
 * @brief   シーンの生成
 * @return  シーン
 */
Scene* ResultScene::createScene()
{
    // シーンの生成
    Scene *scene = Scene::create();
    
    // レイヤーの生成
    ResultScene *layer = ResultScene::create();
    
    // シーンの子にレイヤーを追加
    scene->addChild(layer);
    
    // シーンを返す
    return scene;
}

/**
 * @brief   タッチされたときの処理
 */
bool ResultScene::onTouchBegan(Touch* touch, Event* event)
{
//    Rect rect = mNextButton->getBoundingBox();
//    if(rect.containsPoint(touch->getLocation()))
//    {
//        // シーンの交換
//        this->replaceScene();
//    }
    
    this->replaceScene();
    
    return true;
}