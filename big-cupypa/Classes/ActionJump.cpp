﻿//
//  ActionJump.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//
#include "ActionJump.h"
#include "cocos2d.h"

USING_NS_CC;

namespace
{
	/// パラパラアニメーションの更新ディレイ
	const float ANIMATION_DELAYPERUNIT = 0.075f;
}

/**
 * @brief	ジャンプアクションの生成
 * @return	ジャンプアクション
 */
ActionInterval* ActionJump::create()
{
	// アニメーションを生成する
	Animation* pAnimation = Animation::create();

	// ファイルの名前から、アニメーションを作成する
	for( int i=1; i<=1; i++ )
	{
		char filename[100] = {0};
		sprintf(filename, "CupyJump.png");
		pAnimation->addSpriteFrameWithFile(filename);
	}

	// アニメーションの更新速度を設定する
	pAnimation->setDelayPerUnit(ANIMATION_DELAYPERUNIT);

	// アニメーションをループさせる
	pAnimation->setLoops(0);

	// 作成したアニメーションを、アクションとして保存する
	ActionInterval* pAction = Animate::create(pAnimation);

	return pAction;
}
