#ifndef __CUPYPA_GAMEMANAGER_H__
#define __CUPYPA_GAMEMANAGER_H__

#include "cocos2d.h"
#include "Character.h"
#include "SceneId.h"
#include "StopWatch.h"

/**
 * @brief	ゲームマネージャークラス
 */
class GameManager
{
private:
    
	// ステージセレクトシーンで選択されたステージのID
	int m_chosenStageId;
    
	// 時間
	int m_time;
    
    // ステージクリアフラグ
    bool m_isStageClear;
    
	// キャラクターのポインタ
	Character* m_pCharacter;
    
	// ストップウォッチのポインタ
	StopWatch* m_pStopWatch;
    
public:
    
	/**
	 * @brief	インスタンスの取得
	 * @return	GameManagerのインスタンスのアドレス
	 */
	static GameManager* getInstance();
    
	/**
	 * @brief	サイバーっぽいラベルを生成する
	 * @param	label	ラベルの文章
	 * @param	size	ラベルのサイズ
	 * @return	生成されたラベルのアドレス
	 */
	static cocos2d::Label* createCyberLabel(const char* label, int size);
    
	/**
	 * @brief	色付のスプライトを生成する
	 * @param	filename	ファイル名
	 * @param	color	色
	 * @return	生成された色付スプライト
	 */
	static cocos2d::Sprite* createColorSprite(const char* filename, const cocos2d::Color3B& color);
    
	/**
	 * @brief	bool型の情報を保存する
	 * @param	key		設定するキー
	 * @param	val		設定したい値
	 */
	static void setBoolKey(const char* key, bool val);
    
	/**
	 * @brief	bool型の情報を取得する
	 * @param	key		取得するキー
	 * @param	val		キーが存在しない時に返す値
	 * @return	キーに保存されている値
	 */
	static bool getBoolKey(const char* key, bool val);
    
	/**
	 * @brief	int型の情報を保存する
	 * @param	key		設定するキー
	 * @param	val		設定したい値
	 */
	static void setIntKey(const char* key, int val);
    
	/**
	 * @brief	int型の情報を取得する
	 * @param	key		取得するキー
	 * @param	val		キーに対応する値が存在しない時に返す値
	 * @return	キーに対応した値
	 */
	static int getIntKey(const char* key, int val);
    
	/**
	 * @brief	float型の情報を保存する
	 * @param	key		設定するキー
	 * @param	val		設定したい値
	 */
	static void setFloatKey(const char* key, float val);
    
	/**
	 * @brief	float型の情報を取得する
	 * @param	key		取得するキー
	 * @param	val		キーに対応する値が存在しない時に返す値
	 * @return	キーに対応した値
	 */
	static float getFloatKey(const char* key, float val);
    
	/**
	 * @brief	"text""num"の文字列を取得する
	 * @param	text	文字列
	 * @param	num		数字
	 * @return	結合された文字列
	 */
	static std::string getStringAndNumCombine(std::string text, int num);
    
	/**
	 * @brief		"ftext""num""btext"の文字列を取得する
	 * @param		ftext	前置の文字列
	 * @param		btext	後置の文字列
	 * @param		num		数字
	 * @return		結合された文字列
	 */
	static std::string getStringAndNumCombine(std::string ftext, std::string btext, int num);
    
	/**
	 * @brief	シーンを切り替える
	 * @param	type	次に切り替えるシーンの種類
	 */
	static void replaceScene(SceneId type);
    
	/**
	 * @brief	ストップウォッチを設定する
	 * @param	pStopWatch	ストップウォッチのアドレス
	 */
	inline void setStopWatch(StopWatch* pStopWatch) { m_pStopWatch = pStopWatch; }
    
	/**
	 * @brief	ストップウォッチを取得する
	 * @return	ストップウォッチのポインタ
	 */
	inline StopWatch* getStopWatch() { return m_pStopWatch; }
    
	/**
	 * @brief	ステージIDを設定する
	 * @param	chosenStageId	設定するステージID
	 * @note	シングルトンですので、シーン間のデータ保持のためにこの関数を使います
	 */
	inline void setChosenStageId(int chosenStageId) { m_chosenStageId = chosenStageId; }
    
	/**
	 * @brief	ステージIDを取得する
	 * @return	ステージID
	 * @note	シングルトンですので、シーン間のデータ保持のためにこの関数を使います
	 */
	inline int getChosenStageId() { return m_chosenStageId; }
    
	/**
	 * @brief	時間を設定する
	 * @param	time	時間
	 */
	inline void setTime(int time) { m_time = time; }
    
	/**
	 * @brief	時間を取得する
	 * @return	時間
	 */
	inline int getTime() { return m_time; }
    
    /**
     * @brief   ステージクリアフラグを設定
     * @param   isStageClear    ステージクリアフラグ
     */
    inline void setStageClearFlag(bool isStageClear) { m_isStageClear = isStageClear; }
    
    /**
     * @brief   ステージクリアフラグを取得
     * @return  クリア状態
     */
    inline bool getStageClearFlag() { return m_isStageClear; }
    
	/**
	 * @brief	キャラクターのポインタを設定する
	 * @param	pCharacter	キャラクターのアドレス
	 */
	inline void setCharacter(Character* pCharacter) { m_pCharacter = pCharacter; }
    
	/**
	 * @brief	キャラクターのアドレスを取得する
	 * @return	キャラクターのアドレス
	 */
	inline Character* getCharacter() { if(m_pCharacter){ return m_pCharacter; } return NULL; }
    
	/**
	 * @param	pCharacter	キャラクターのポインタ
	 */
	inline void getCharacter(Character* pCharacter) { pCharacter = m_pCharacter; }
};


#endif //__CUPYPA_GAMEMANAGER_H__
