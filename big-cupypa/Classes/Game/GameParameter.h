#ifndef __CUPYPA_GAME_PARAMETER_H__
#define __CUPYPA_GAME_PARAMETER_H__

#include "cocos2d.h"

/**
 * @brief ゲームのパラメータ
 */
class GameParameter
{
public:
    
	// ブロックの数(X座標)
	static const unsigned int NUM_BLOCK_X;
    
	// ブロックの数(Y座標)
	static const unsigned int NUM_BLOCK_Y;
    
	// ブロックの横幅
	static const float BLOCK_WIDTH;
    
	// ブロックの縦幅
	static const float BLOCK_HEIGHT;
    
	// ハーフブロックの横幅
	static const float HALF_BLOCK_WIDTH;
    
	// ハーフブロックの縦幅
	static const float HALF_BLOCK_HEIGHT;
    
	// 画面の横幅
	static const float BASE_SCREEN_WIDTH;
    
	// 画面の縦幅
	static const float BASE_SCREEN_HEIGHT;
    
	// 画面の中心座標
	static const cocos2d::Point SCREEN_CENTER;
    
	// アニメーション間隔
	static const float ANIMATION_INTERVAL;
    
	// 更新間隔
	static const float UPDATE_INTERVAL;
};

#endif // __CUPYPA_GAME_PARAMETER_H__
