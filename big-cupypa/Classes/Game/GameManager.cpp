#include "GameManager.h"
#include "SimpleAudioEngine.h"
#include "SceneFactory.h"
#include "StageNameDefs.h"
#include "SceneId.h"

USING_NS_CC;

/**
 * @brief	インスタンスの取得
 * @return	GameManagerのインスタンスのアドレス
 */
GameManager* GameManager::getInstance()
{
	static GameManager instance;
	return &instance;
}

/**
 * @brief	色付のスプライトを生成する
 * @param	[in]	filename	ファイル名
 * @param	[in]	color	色
 * @return	生成された色付スプライト
 */
Sprite* GameManager::createColorSprite(const char* filename, const cocos2d::Color3B& color)
{
	Sprite* pSprite = Sprite::create(filename);
    
	pSprite->setColor(color);
    
	return pSprite;
}

/**
 * @brief	bool型の情報を保存する
 * @param	key		設定するキー
 * @param	val		設定したい値
 */
void GameManager::setBoolKey(const char* key, bool val)
{
	UserDefault::getInstance()->setBoolForKey(key,val);
	UserDefault::getInstance()->flush();
}

/**
 * @brief	bool型の情報を取得する
 * @param	key		取得するキー
 * @param	val		キーが存在しない時に返す値
 * @return	キーに保存されている値
 */
bool GameManager::getBoolKey(const char* key, bool val)
{
	return UserDefault::getInstance()->getBoolForKey(key,val);
}

/**
 * @brief	int型の情報を保存する
 * @param	key		設定するキー
 * @param	val		設定したい値
 */
void GameManager::setIntKey(const char* key, int val)
{
	CCUserDefault::getInstance()->setIntegerForKey(key, val);
	CCUserDefault::getInstance()->flush();
}
/**
 * @brief	int型の情報を取得する
 * @param	key		取得するキー
 * @param	val		キーに対応する値が存在しない時に返す値
 * @return	キーに対応した値
 */
int GameManager::getIntKey(const char* key, int val)
{
	return UserDefault::getInstance()->getIntegerForKey(key, val);
}

/**
 * @brief	float型の情報を保存する
 * @param	key		設定するキー
 * @param	val		設定したい値
 */
void GameManager::setFloatKey(const char* key, float val)
{
	UserDefault::getInstance()->setFloatForKey(key, val);
	UserDefault::getInstance()->flush();
}

/**
 * @brief	float型の情報を取得する
 * @param	[in]	key		取得するキー
 * @param	[in]	val		キーに対応する値が存在しない時に返す値
 * @return	キーに対応した値
 */
float GameManager::getFloatKey(const char* key, float val)
{
	return CCUserDefault::getInstance()->getFloatForKey(key, val);
}

/**
 * @brief	"text""num"の文字列を取得する
 * @param	text	文字列
 * @param	num		数字
 * @return	結合された文字列
 */
std::string GameManager::getStringAndNumCombine(std::string text, int num)
{
	std::stringstream stream;
	stream << text << num;
	return stream.str();
}

/**
 * @brief		"ftext""num""btext"の文字列を取得する
 * @param		ftext	前置の文字列
 * @param		btext	後置の文字列
 * @param		num		数字
 * @return		結合された文字列
 */
std::string GameManager::getStringAndNumCombine(std::string ftext, std::string btext, int num)
{
	std::stringstream stream;
	stream << GameManager::getStringAndNumCombine(ftext,num) << btext;
	return stream.str();
}

/**
 * @brief	シーンを切り替える
 * @param	type	次に切り替えるシーンの種類
 */
void GameManager::replaceScene(SceneId type)
{
	// BGMを解放する
	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);

	// 次のシーンを生成する
	Scene* pNextScene = SceneFactory::createScene(type);
    
	// トランジションをつける
	pNextScene = TransitionZoomFlipAngular::create(1.0f,pNextScene);
    
	// シーンを切り替える
	Director::getInstance()->replaceScene(pNextScene);
}

/**
 * @brief	サイバーっぽいラベルを生成する
 * @param	[in]	label	ラベルの文章
 * @param	[in]	size	ラベルのサイズ
 * @return	生成されたラベルのアドレス
 */
Label* GameManager::createCyberLabel(const char* label, int size)
{
	Label* pLabel = Label::create(label,"TECHNOID.ttf",size);
    
	return pLabel;
}