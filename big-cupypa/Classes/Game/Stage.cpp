//
//  Stage.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

#include "Stage.h"
#include "GameObject.h"
#include "GameParameter.h"
#include "ObjectFactory.h"
#include "Utilities.h"

#include "CollisionDetection.h"
#include "Character.h"
#include "GameManager.h"

USING_NS_CC;

namespace {
    
    /**
     * @brief   ノード
     */
    enum NodeDepth
    {
        NODEDEPTH_OBJECT,   // オブジェクト
    };
    
    /**
     * @brief   エリア情報
     */
    struct AreaInfo
    {
        const char* filename;   // ファイル名
        float direction;        // 方向
    };
    
    // ステージ情報
    const AreaInfo STAGE_INFO[][4] =
    {
        {
            { "Stage1_1.tmx", 1.0f },
            { "Stage1_2.tmx", -1.0f },
            { "Stage1_3.tmx", 1.0f },
            { "Stage1_4.tmx", -1.0f },
        }
    };
}

/**
 * @brief   ステージの生成
 * @param   stageNumber ステージ番号
 * @return  ステージ
 */
Stage* Stage::create(unsigned int stageNumber)
{
    // ステージを生成
    Stage* stage = new Stage();
    
    // ステージを初期化
    if(stage && stage->init(stageNumber))
    {
        // 自動解放を設定しておく
        stage->autorelease();
        return stage;
    }
    
    delete stage;
    
    return nullptr;
}

/**
 * @brief	デフォルトコンストラクタ
 */
Stage::Stage()
: stageNumber(0)
, areaNumber(0)
, areaChanged(false)
, isStageClear(false)
, collisionDetection(NULL)
, gameObjects()
{
}

/**
 * @brief	デストラクタ
 */
Stage::~Stage()
{
}

/**
 * @brief   ステージの初期化
 * @param   stageNumber ステージ番号
 * @return  true 初期化に成功
 * @return  false 初期化に失敗
 */
bool Stage::init(unsigned int stageNumber)
{
    // 親レイヤーの初期化
    if(!Layer::init())
    {
        return false;
    }
    
    // ステージ番号を設定
    this->stageNumber = stageNumber;
    
    // エリア番号を設定
    areaNumber = 0;
    
    // 衝突判定を生成
    collisionDetection = CollisionDetection::create();
    
    // 最初のエリア情報を読み込む
    loadAreaInfo();
    
    // ステージ構築完了イベントを送信
    sendConstructedStageEvent();
    
    return true;
}

/**
 * @brief   ステージの更新
 * @param   dt  経過時間
 */
void Stage::update(float dt)
{
    // オブジェクト群の更新
	updateObjects(dt);
    
	// 衝突判定を更新
	updateCollisionDetection();
    
	// オブジェクト群を最適化
	optimizeObjects();
    
	// オブジェクト群の座標を更新
	updateObjectPosition();
    
	// エリアを変更
	if (areaChanged) changeArea();
}

/**
 * @brief   解放処理
 */
void Stage::finalize()
{
    for (GameObjectList::iterator itr = gameObjects.begin();
         itr != gameObjects.end(); ++itr)
    {
        // ゲームオブジェクト
        GameObject* object = *itr;
        object->finalize();
    }
}

/**
 * @brief   ゲームオブジェクトの追加
 * @param   object ゲームオブジェクト
 */
void Stage::addGameObject(GameObject* object)
{
    // タグ
    unsigned int tag = object->getObjectType();
    
    // ゲームオブジェクトリストに追加
    gameObjects.push_back(object);
    
    // ステージの子ノードとして追加
    this->addChild(object, NODEDEPTH_OBJECT, tag);
    
    // 衝突判定に追加
    collisionDetection->addGameObject(object);
}

/**
 * @brief	ゲームオブジェクトの取得
 * @param	objects [out] ゲームオブジェクトの格納先
 * @return	ゲームオブジェクトの数
 */
unsigned int Stage::getGameObjects(GameObjectArray *objects)
{
    // ゲームオブジェクト配列をクリア
    objects->clear();
    
    // ゲームオブジェクトの数
    unsigned int numObjects = 0;
    
    for(GameObjectList::iterator itr = gameObjects.begin(); itr != gameObjects.end(); ++itr)
    {
        // ゲームオブジェクト配列に追加
        objects->push_back(*itr);
        // ゲームオブジェクトの数をインクリメント
        ++numObjects;
    }
    
    return numObjects;
}

/**
 * @brief	ゲームオブジェクトの取得
 * @param	objects ゲームオブジェクトの格納先
 * @param	type    オブジェクトタイプ
 * @return	ゲームオブジェクトの数
 */
unsigned int Stage::getGameObjects(GameObjectArray *objects, ObjectType type)
{
    // ゲームオブジェクト配列をクリア
    objects->clear();
    
    // ゲームオブジェクトの数
    unsigned int numObjects = 0;
    
    for(GameObjectList::iterator itr = gameObjects.begin(); itr != gameObjects.end(); ++itr)
    {
        // ゲームオブジェクト
        GameObject* object = *itr;
        
        if(object->getObjectType() == type)
        {
            // ゲームオブジェクト配列に追加
            objects->push_back(*itr);
            // ゲームオブジェクトの数をインクリメント
            ++numObjects;
        }
    }
    
    return numObjects;
}

/**
 * @brief	衝突判定
 * @param	point 座標
 * @param	objects 衝突しているゲームオブジェクトの格納先
 * @return	衝突しているゲームオブジェクトの数
 */
unsigned int Stage::collisionAt(
                                const cocos2d::Point& point,
                                GameObjectArray* objects)
{
	// 衝突しているゲームオブジェクトの数
	unsigned int numObjects = 0;
    
	for (GameObjectList::iterator itr = gameObjects.begin();
         itr != gameObjects.end(); ++itr)
	{
		// ゲームオブジェクト
		GameObject* tmp = *itr;
        
		// 衝突判定
		if (tmp && tmp->checkCollision(point))
		{
			if (objects) objects->push_back(tmp);
			++numObjects;
		}
	}
    
	return numObjects;
}

/**
 * @brief	衝突判定
 * @param	object ゲームオブジェクト
 * @param	objects 衝突しているゲームオブジェクトの格納先
 * @return	衝突しているゲームオブジェクトの数
 */
unsigned int Stage::collisionWith(
                                  GameObject* object,
                                  GameObjectArray* objects)
{
	// 衝突しているゲームオブジェクトの数
	unsigned int numObjects = 0;
    
	for (GameObjectList::iterator itr = gameObjects.begin();
         itr != gameObjects.end(); ++itr)
	{
		// ゲームオブジェクト
		GameObject* tmp = *itr;
        
		// 衝突判定
		if (tmp && tmp != object && tmp->checkCollision(object))
		{
			if (objects) objects->push_back(tmp);
			++numObjects;
		}
	}
    
	return numObjects;
}

/**
 * @brief	衝突判定
 * @param	rect 矩形
 * @param	objects 衝突しているゲームオブジェクトの格納先
 * @return	衝突しているゲームオブジェクトの数
 */
unsigned int Stage::collisionWith(
                                  const cocos2d::Rect& rect,
                                  GameObjectArray* objects)
{
	// 衝突しているゲームオブジェクトの数
	unsigned int numObjects = 0;
    
	for (GameObjectList::iterator itr = gameObjects.begin();
         itr != gameObjects.end(); ++itr)
	{
		// ゲームオブジェクト
		GameObject* tmp = *itr;
        
		// 衝突判定
		if (tmp && tmp->checkCollision(rect))
		{
			if (objects) objects->push_back(tmp);
			++numObjects;
		}
	}
    
	return numObjects;
}

/**
 * @brief	エリアを進む
 */
void Stage::advanceArea()
{
	// エリア変更フラグを設定
	areaChanged = true;
}

/**
 * @brief	エリアのリセット
 */
void Stage::resetArea()
{
	// ゲームオブジェクトをクリア
	clearGameObjects();
    
	// エリア情報を読み込む
	loadAreaInfo();
    
	// ステージ構築完了イベントを送信
	sendConstructedStageEvent();
    
	// エリア変更フラグを設定
	areaChanged = false;
}


/**
 * @brief   エリア情報の読み込み
 * @return  true 読み込みに成功
 * @return  false 読み込みに失敗
 */
bool Stage::loadAreaInfo()
{
    // タイルマップを生成
    TMXTiledMap* tiledMap = TMXTiledMap::create(STAGE_INFO[stageNumber - 1][areaNumber].filename);
    //TMXTiledMap* tiledMap = TMXTiledMap::create("Stage1_2.tmx");
    
    TMXLayer* layer = tiledMap->getLayer("Main");
    
    // オブジェクトを生成
    if(!createObjects(layer))
    {
        return false;
    }
    
    return true;
}

/**
 * @brief	ゲームオブジェクトのクリア
 */
void Stage::clearGameObjects()
{
	// 子オブジェクトを削除
	removeAllChildren();
    
	// ゲームオブジェクリストをクリア
	gameObjects.clear();
    
	// 衝突オブジェクトをクリア
	collisionDetection->clear();
}

/**
 * @brief   オブジェクト群の生成
 * @param   layer タイルマップのレイヤー
 * @return  true 生成に成功
 * @return  false 生成に失敗
 */
bool Stage::createObjects(cocos2d::TMXLayer *layer)
{
    for(unsigned int y = 0; y < GameParameter::NUM_BLOCK_Y; ++y)
    {
        for(unsigned int x = 0; x < GameParameter::NUM_BLOCK_X; ++x)
        {
            // タイルIDを取得
            //unsigned int id = layer->tileGIDAt(Point(x, y));
            
            unsigned int id = layer->getTileGIDAt(Point(x, y));
            
            // 取得に成功
            if(id)
            {
                // オブジェクトを生成
                GameObject* object = createObject(id, x, y);
                
                // オブジェクトを追加
                addGameObject(object);
            }
        }
    }
    
    return true;
}

/**
 * @brief	オブジェクトの生成
 * @param	id タイルID
 * @param	x X座標
 * @param	y Y座標
 * @return	ゲームオブジェクト
 */
GameObject* Stage::createObject(unsigned int id, unsigned int x, unsigned int y)
{
    // オブジェクトを生成
    GameObject* object = ObjectFactory::createObject(static_cast<ObjectType>(id));
    
    // 座標管理変数
    Point p;
    
    // 座標変換
    Utils::convertoTiledMapPosition(&p, x, y);
    
    // Y軸を反転
    p.y = GameParameter::BASE_SCREEN_HEIGHT - p.y;
    
    // 座標を設定
    object->setPosition(p);
    
    if(OBJECTTYPE_CHARACTER == id)
    {
        Character* character = dynamic_cast<Character*>(object);
        if(character)
        {
            // 方向を設定
            character->setDirection(STAGE_INFO[stageNumber - 1][areaNumber].direction);
        }
    }
    
    return object;
}

/**
 * @brief	オブジェクト群の更新
 * @param	dt 経過時間
 */
void Stage::updateObjects(float dt)
{
	for (GameObjectList::iterator itr = gameObjects.begin();
         itr != gameObjects.end(); ++itr)
	{
		// ゲームオブジェクト
		GameObject* object = *itr;
        
		// 更新フラグがtrueの場合
		if (object && object->isEnabled())
		{
			// ゲームオブジェクトを更新
			object->update(dt);
		}
	}
}

/**
 * @brief	衝突判定を更新
 */
void Stage::updateCollisionDetection()
{
	// 衝突判定を更新
	collisionDetection->update();
}

/**
 * @brief	オブジェクト群の最適化
 */
void Stage::optimizeObjects()
{
	GameObjectList::iterator itr = gameObjects.begin();
    
	while (itr != gameObjects.end())
	{
		GameObject* obj = *itr;
        
		// 削除フラグがtrueの場合
		if (obj && obj->getDeleteFlag())
		{
			// 衝突判定から削除
			collisionDetection->removeGameObject(obj);
			// ゲームオブジェクトリストから削除
			itr = gameObjects.erase(itr);
			// 子ノードを削除
			removeChild(obj);
		}
		else
		{
			++itr;
		}
	}
}

/**
 * @brief	オブジェクト群の座標の更新
 */
void Stage::updateObjectPosition()
{
	for (GameObjectList::iterator itr = gameObjects.begin();
         itr != gameObjects.end(); ++itr)
	{
		// ゲームオブジェクト
		GameObject* obj = *itr;
        
		if (obj)
		{
			// 座標を更新
			obj->updatePosition();
		}
	}
}

/**
 * @brief	エリアの変更
 */
void Stage::changeArea()
{
	// エリア番号をインクリメント
	++areaNumber;
    
	// 最終エリアをクリアした場合
	if (4 <= areaNumber)
	{
		// ステージクリア
		succeededStage();
		return;
	}
    
	// ゲームオブジェクトをクリア
	clearGameObjects();
    
	// エリア情報を読み込む
	loadAreaInfo();
    
	// ステージ構築完了イベントを送信
	sendConstructedStageEvent();
    
	// エリア変更フラグを設定
	areaChanged = false;
}

/**
 * @brief	ステージクリア時の処理
 */
void Stage::succeededStage()
{
	GameManager::getInstance()->getStopWatch()->countStop();
    GameManager::getInstance()->setTime(GameManager::getInstance()->getStopWatch()->getTime());
    this->setStageClearFlag(true);
}

/**
 * @brief   ステージ構築完了イベントの送信
 */
void Stage::sendConstructedStageEvent()
{
    for(GameObjectList::iterator itr = gameObjects.begin(); itr != gameObjects.end(); ++itr)
    {
        // ステージ構築完了を通知
        (*itr)->onConstructedStage(this);
    }
}


