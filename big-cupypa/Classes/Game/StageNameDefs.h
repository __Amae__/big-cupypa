//
//  StageNameDefs.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CUPYPA_STAGENAMEDEFS_H__
#define __CUPYPA_STAGENAMEDEFS_H__

/**
 * @brief	ステージ名の取得
 * @param	stageNumber ステージ番号
 * @return	ステージ名
 */
const char* getStageName(unsigned int stageNumber);

#endif //__CUPYPA_STAGENAMEDEFS_H__
