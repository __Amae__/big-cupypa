//
//  StageNameDefs.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "StageNameDefs.h"

/**
 * @brief	ステージ名タイプ
 * @warning	上からステージ1,ステージ2.....
 */
const char* StageNameType[] = {
    "MONO",
    "FACTORY",
    "CITY",
    "UNIVERSE",
    "COLDSLEEP",
    "CLOUD",
    "SILKTOUCH",
    "HELL",
    "STRING",
    "SPARK",
    "HOGE",
    "HOEE"
};

/**
 * @brief	ステージ名の取得
 * @param	stageNumber ステージ番号
 * @return	ステージ名
 */
const char* getStageName(unsigned int stageNumber)
{
	return StageNameType[stageNumber];
}
