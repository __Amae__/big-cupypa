//
//  StopWatch.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CUPYPA_STOPWATCH_H__
#define __CUPYPA_STOPWATCH_H__

#include "cocos2d.h"

/**
 * @brief	ストップウォッチクラス
 */
class StopWatch : public cocos2d::Layer
{
private:
    
	/// タイム
	int m_time;
    
	/// カウントフラグ
	bool m_count;
    
public:
    
	/**
	 * @brief	コンストラクタ
	 */
	StopWatch();
    
public:
    
	/**
	 * @brief	生成
	 */
	CREATE_FUNC(StopWatch);
    
	/**
	 * @brief	初期化
	 * @retval	true	初期化に成功
	 * @retval	false	初期化に失敗
	 */
	bool init();
    
	/**
	 * @brief	更新
	 */
	void update();
    
	/**
	 * @brief	カウントをスタートする
	 */
	void countStart();
    
	/**
	 * @param	[in]	reset	trueならタイムをリセットしてスタートする
	 */
	void countStart(bool reset);
    
	/**
	 * @brief	カウントをストップする
	 */
	void countStop();
    
	/**
	 * @brief	タイムをリセットする
	 */
	void timeReset();
    
	/**
	 * @brief	タイムをセットする
	 * @param	[in]	time	セットするタイム
	 */
	inline void setTime(int time) { m_time = time; }
    
	/**
	 * @brief	タイムをゲットする
	 * @return	タイム
	 */
	inline int getTime() { return m_time; }
};

#endif //__CUPYPA_STOPWATCH_H__
