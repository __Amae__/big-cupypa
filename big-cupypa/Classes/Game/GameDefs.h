//
//  GameDefs.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

#ifndef __GAMEDEFS_H__
#define __GAMEDEFS_H__

#include "cocos2d.h"

class GameDefs
{
public:
    
	// ブロックの数 (X軸)
	static const unsigned int NUM_BLOCK_X;
    
	// ブロックの数 (Y軸)
	static const unsigned int NUM_BLOCK_Y;
    
	// ブロックの幅
	static const float BLOCK_WIDTH;
    
	// ブロックの高さ
	static const float BLOCK_HEIGHT;
    
	// ブロックの幅の半分
	static const float HALF_BLOCK_WIDTH;
    
	// ブロックの高さの半分
	static const float HALF_BLOCK_HEIGHT;
    
	// スクリーンの幅
	static const float BASE_SCREEN_WIDTH;
    
	// スクリーンの高さ
	static const float BASE_SCREEN_HEIGHT;
    
	// スクリーンの中心座標
	static const cocos2d::Point SCREEN_CENTER;
    
	// アニメーションの更新間隔
	static const float ANIMATION_INTERVAL;
    
	// 更新間隔
	static const float UPDATE_INTERVAL;
    
};

#endif //__GAMEDEFS_H__
