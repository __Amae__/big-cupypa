//
//  GameDefs.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

#include "GameDefs.h"

USING_NS_CC;

// ブロックの数 (X軸)
const unsigned int GameDefs::NUM_BLOCK_X = 15;

// ブロックの数 (Y軸)
const unsigned int GameDefs::NUM_BLOCK_Y = 10;

// ブロックの幅
const float GameDefs::BLOCK_WIDTH = 32.0f;

// ブロックの高さ
const float GameDefs::BLOCK_HEIGHT = 32.0f;

// ブロックの幅の半分
const float GameDefs::HALF_BLOCK_WIDTH = BLOCK_WIDTH * 0.5f;

// ブロックの高さの半分
const float GameDefs::HALF_BLOCK_HEIGHT = BLOCK_HEIGHT * 0.5f;

// スクリーンの幅
const float GameDefs::BASE_SCREEN_WIDTH = 480.0f;

// スクリーンの高さ
const float GameDefs::BASE_SCREEN_HEIGHT = 320.0f;

// スクリーンの中心座標
const Point GameDefs::SCREEN_CENTER =
Point(BASE_SCREEN_WIDTH * 0.5f, BASE_SCREEN_HEIGHT * 0.5f);

// アニメーションの更新間隔
const float GameDefs::ANIMATION_INTERVAL = 1.0f / 60.0f;

// 更新間隔
const float GameDefs::UPDATE_INTERVAL = 1.0f / 60.0f;