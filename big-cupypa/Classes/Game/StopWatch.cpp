//
//  StopWatch.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "StopWatch.h"

USING_NS_CC;

/**
 * @brief	コンストラクタ
 */
StopWatch::StopWatch()
: m_time(0)
, m_count(false)
{
}

/**
 * @brief	初期化
 * @retval	true	初期化に成功
 * @retval	false	初期化に失敗
 */
bool StopWatch::init()
{
	if(!Layer::init())
	{
		return false;
	}
    
	return true;
}

/**
 * @brief	更新
 */
void StopWatch::update()
{
	if(m_count) m_time++;
}

/**
 * @brief	カウントをスタートする
 */
void StopWatch::countStart()
{
	m_count = true;
}

/**
 * @param	[in]	reset	trueならタイムをリセットしてスタートする
 */
void StopWatch::countStart(bool reset)
{
	if(reset) this->timeReset();
	this->countStart();
}

/**
 * @brief	カウントをストップする
 */
void StopWatch::countStop()
{
	m_count = false;
}

/**
 * @brief	タイムをリセットする
 */
void StopWatch::timeReset()
{
	m_time = 0;
}
