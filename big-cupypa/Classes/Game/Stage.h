//
//  Stage.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

#ifndef __STAGE_H__
#define __STAGE_H__

#include "cocos2d.h"
#include "ObjectDefs.h"

// ゲームオブジェクト
class GameObject;

// 衝突判定
class CollisionDetection;

// ゲームオブジェクト配列
typedef std::vector<GameObject*> GameObjectArray;

class Stage : public cocos2d::Layer
{
private:
    // ゲームオブジェクトリスト
    typedef std::list<GameObject*> GameObjectList;

public:
    /**
     * @brief   ステージの生成
     * @param   stageNumber ステージ番号
     * @return  ステージ
     */
    static Stage* create(unsigned int stageNumber);
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	Stage();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~Stage();

public:
    
    /**
     * @brief   ステージの初期化
     * @param   stageNumber ステージ番号
     * @return  true 初期化に成功
     * @return  false 初期化に失敗
     */
    bool init(unsigned int stageNumber);
    
    /**
     * @brief   ステージの更新
     * @param   dt  経過時間
     */
    void update(float dt);
    
    /**
     * @brief   解放処理
     */
    void finalize();
    
    /**
     * @brief   ゲームオブジェクトの追加
     * @param   object ゲームオブジェクト
     */
    void addGameObject(GameObject* object);
    
    /**
     * @brief   ゲームオブジェクトの取得
     * @param   objects ゲームオブジェクトの格納先
     * @return  ゲームオブジェクトの数
     */
    unsigned int getGameObjects(GameObjectArray* objects);
    
    /**
     * @brief   ゲームオブジェクトの取得
     * @param   objects ゲームオブジェクトの格納先
     * @param   type オブジェクトタイプ
     * @return  ゲームオブジェクトの数
     */
    unsigned int getGameObjects(GameObjectArray* objects, ObjectType type);
    
    /**
	 * @brief	衝突判定
	 * @param	point 座標
	 * @param	objects 衝突しているゲームオブジェクトの格納先
	 * @return	衝突しているゲームオブジェクトの数
	 */
	unsigned int collisionAt(
                             const cocos2d::Point& point,
                             GameObjectArray* objects);
    
	/**
	 * @brief	衝突判定
	 * @param	object ゲームオブジェクト
	 * @param	objects 衝突しているゲームオブジェクトの格納先
	 * @return	衝突しているゲームオブジェクトの数
	 */
	unsigned int collisionWith(
                               GameObject* object,
                               GameObjectArray* objects);
    
	/**
	 * @brief	衝突判定
	 * @param	rect 矩形
	 * @param	objects 衝突しているゲームオブジェクトの格納先
	 * @return	衝突しているゲームオブジェクトの数
	 */
	unsigned int collisionWith(
                               const cocos2d::Rect& rect,
                               GameObjectArray* objects);
    
	/**
	 * @brief	エリアを進む
	 */
	void advanceArea();
    
	/**
	 * @brief	エリアのリセット
	 */
	void resetArea();
    
    /**
     * @brief   エリア切り替えフラグを取得
     */
    inline bool getAreaChangedFlag() { return areaChanged; }
    
    /**
     * @brief   ステージクリアフラグを設定
     */
    inline void setStageClearFlag(bool isStageClear) { this->isStageClear = isStageClear; }
    
    /**
     * @brief   ステージクリアフラグを取得
     */
    inline bool getStageClearFlag() { return isStageClear; }
    
private:
    
    /**
     * @brief   エリア情報の読み込み
     * @return  true 読み込みに成功
     * @return  false 読み込みに失敗
     */
    bool loadAreaInfo();
    
    /**
     * @brief   ゲームオブジェクトのクリア
     */
    void clearGameObjects();
    
    /**
     * @brief   オブジェクト群の生成
     * @param   layer タイルマップのレイヤー
     * @return  true 生成に成功
     * @return  false 生成に失敗
     */
    bool createObjects(cocos2d::TMXLayer* layer);
    
    /**
     * @brief   オブジェクトの生成
     * @param   id  タイルID
     * @param   x   X座標
     * @param   y   Y座標
     * @return  ゲームオブジェクト
     */
    GameObject* createObject(unsigned int id, unsigned int x, unsigned int y);
    
    /**
     * @brief   オブジェクト群の更新
     * @param   dt 経過時間
     */
    void updateObjects(float dt);
    
    /**
     * @brief   衝突判定を更新
     */
    void updateCollisionDetection();
    
    /**
     * @brief   オブジェクト群の最適化
     */
    void optimizeObjects();
    
    /**
     * @brief   オブジェクト群の座標の更新
     */
    void updateObjectPosition();
    
    /**
     * @brief   エリアの変更
     */
    void changeArea();
    
    /**
	 * @brief	ステージクリア時の処理
	 */
	void succeededStage();
    
    /**
     * @brief   ステージ構築完了イベントの送信
     */
    void sendConstructedStageEvent();
    
private:
    
    unsigned int stageNumber;   // ステージ番号
    unsigned int areaNumber;    // エリア番号
    bool areaChanged;           // エリア変更フラグ
    
    bool isStageClear;  //  ステージクリアしたか？
    
    GameObjectList gameObjects;
    
	CollisionDetection* collisionDetection; // 衝突判定
};

#endif //__STAGE_H__
