#include "GameParameter.h"

USING_NS_CC;

// ブロックの数(X座標)
const unsigned int GameParameter::NUM_BLOCK_X = 8;

// ブロックの数(Y座標)
const unsigned int GameParameter::NUM_BLOCK_Y = 12;

// ブロックの横幅
const float GameParameter::BLOCK_WIDTH = 40.0f;

// フロックの縦幅
const float GameParameter::BLOCK_HEIGHT = 40.0f;

// ハーフブロックの横幅
const float GameParameter::HALF_BLOCK_WIDTH = BLOCK_WIDTH * 0.5f;

// ハーフブロックの縦幅
const float GameParameter::HALF_BLOCK_HEIGHT = BLOCK_HEIGHT * 0.5f;

// 画面の横幅
const float GameParameter::BASE_SCREEN_WIDTH = 320.0f;

// 画面の縦幅
const float GameParameter::BASE_SCREEN_HEIGHT = 480.0f;

// 画面の中心座標
const Point GameParameter::SCREEN_CENTER = Point(BASE_SCREEN_WIDTH * 0.5f, BASE_SCREEN_HEIGHT * 0.5f);

// アニメーション間隔
const float GameParameter::ANIMATION_INTERVAL = 1.0f / 60.0f;

// 更新間隔
const float GameParameter::UPDATE_INTERVAL = 1.0f / 60.0f;