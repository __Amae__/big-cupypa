﻿//
//  ActionBike.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __CUPYPA_ACTIONBIKE_H__
#define __CUPYPA_ACTIONBIKE_H__

#include "cocos2d.h"

/**
 * @brief	バイクアクション
 */
class ActionBike
{
public:

	/**
	 * @brief	アクションの生成
	 * @param	loop [in] ループ回数
	 * @return	ダッシュアクション
	 */
	static cocos2d::ActionInterval* create(int loop);
};

#endif //__CUPYPA_ACTIONBIKE_H__
