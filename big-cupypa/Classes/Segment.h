//
//  Segment.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __SEGMENT_H__
#define __SEGMENT_H__

#include "cocos2d.h"

/**
 * @brief	線分
 */
class Segment
{
public:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	Segment();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~Segment();
    
public:
    
	/**
	 * @brief	線分同士の交差判定
	 * @param	target ターゲット
	 * @retval	true 交差している
	 * @retval	false 交差していない
	 */
	bool intersect(const Segment& target);
    
	/**
	 * @brief	座標の設定
	 * @param	p1 [in] 点1
	 * @param	p2 [in] 点2
	 */
	void set(const cocos2d::Point& p1, const cocos2d::Point& p2);
    
public:
    
	// 始点
	cocos2d::Point start;
    
	// ベクトル
	cocos2d::Point vector;
};

#endif // __SEGMENT_H__
