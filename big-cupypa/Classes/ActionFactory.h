//
//  ActionFactory.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CUPYPA_ACTIONFACTORY_H__
#define __CUPYPA_ACTIONFACTORY_H__

#include "cocos2d.h"
#include "ActionDefs.h"

/**
 * @brief	カピーさんのアクションのファクトリークラス
 */
class ActionFactory
{
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 * @note	インスタンスの生成を禁止する
	 */
	ActionFactory();
    
public:
    
	/**
	 * @brief	カピーさんのアクションを生成する
	 * @param	type 生成したいアクション
	 * @param	target アクションを実行する対象の座標
	 * @return	typeで指定されたアクション
	 */
	static cocos2d::ActionInterval* createAction(ActionType type, const cocos2d::Point& target);
    
	/**
	 * @param		type 生成したいアクション
	 * @return		typeで指定されたアクション
	 */
	static cocos2d::ActionInterval* createAction(ActionType type);
    
	/**
	 * @param		type 生成したいアクション
	 * @param		target アクションを実行する対象のスプライト
	 * @return		typeで指定されたアクション
	 */
	static cocos2d::ActionInterval* createAction(ActionType type, cocos2d::Sprite* target);
};


#endif //__CUPYPA_ACTIONFACTORY_H__
