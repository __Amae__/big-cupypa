//
//  FallDownBlock.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "FallDownBlock.h"
#include "GameParameter.h"
#include "ObjectDefs.h"

USING_NS_CC;

/**
 * @brief   デフォルトコンストラクタ
 */
FalldownBlock::FalldownBlock()
: state(NULL)
{
    // オブジェクトタイプを設定
    setObjectType(OBJECTTYPE_FALLDOWNBLOCK);
}

/**
 * @brief   デストラクタ
 */
FalldownBlock::~FalldownBlock()
{
}

/**
 * @brief	フォールダウンブロックの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool FalldownBlock::init()
{
	// 基底クラスを初期化
	if (!GameObject::init())
	{
		return false;
	}
    
	// ステートを設定
	state = &FalldownBlock::normalUpdate;
    
    // イベントリスナーの定義
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(FalldownBlock::onTouchBegan, this);
    // タッチイベントを追加
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	
    return true;
}

/**
 * @brief	フォールダウンブロックの更新
 * @param	dt 経過時間
 */
void FalldownBlock::update(float dt)
{
	// ステートを実行
	(this->*state)(dt);
}

/**
 * @brief	タッチ開始時の処理
 * @param	touch タッチ情報
 * @param	event イベント情報
 * @retval	true タッチ判定を継続
 * @retval	false タッチ判定を終了
 */
bool FalldownBlock::onTouchBegan(
            cocos2d::Touch* touch,
            cocos2d::Event* event)
{
	// タッチ座標
	Point touchPoint = touch->getLocation();
    
	// 衝突判定
	if (checkCollision(touchPoint))
	{
		// 移動フラグを設定
		setMovable(true);
        
		// ステートを設定
		state = &FalldownBlock::touchedAfterUpdate;
        
		touchedAfterUpdate(0.0f);
	}
    
	return false;
}

/**
 * @brief	衝突イベント
 * @param	target 衝突相手
 */
void FalldownBlock::onCollisionDetected(GameObject* target)
{
	// 線分開始点
	Point start = getPosition();
    
	// 線分終了点
	Point end = getNextPosition();
    
	// 底辺を基準に計算
	end.y -= getCollisionSize().height * 0.5f;
    
	// X軸を統一
	end.x = start.x;
    
	// 線分
	Segment segment;
	segment.set(start, end);
    
	// 線分による交差判定
	if (target->checkCollision(segment, 0, 1))
	{
		float nextY = target->getPositionY();
		nextY += target->getCollisionSize().height * 0.5f;
		nextY += getCollisionSize().height * 0.5f;
        
		// 次のY座標を設定
		setNextPositionY(nextY);
        
		// 重力をリセット
		resetGravity();
	}
}

/**
 * @brief	通常の更新
 * @param	dt 経過時間
 */
void FalldownBlock::normalUpdate(float dt)
{
}

/**
 * @brief	タッチ後の更新
 * @param	dt 経過時間
 */
void FalldownBlock::touchedAfterUpdate(float dt)
{
	// 重力計算
	calcGravity();
    
	// 次の座標を計算
	calcNextPosition();
}

/**
 * @brief	スプライトの生成
 * @return	スプライト
 */
Sprite* FalldownBlock::createSprite()
{
	// スプライトを生成
	Sprite* sprite = Sprite::create(
                    "Blocks.png",
                    Rect(GameParameter::BLOCK_WIDTH,
                         GameParameter::BLOCK_HEIGHT,
                         GameParameter::BLOCK_WIDTH,
                         GameParameter::BLOCK_HEIGHT));
	return sprite;
}

