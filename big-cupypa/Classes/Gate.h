//
//  Gate.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __GATE_H__
#define __GATE_H__

#include "GameObject.h"

/**
 * @brief	ゲート
 */
class Gate : public GameObject
{
public:
    
	/**
	 * @brief	ゲートの生成
	 * @return	ゲート
	 */
	CREATE_FUNC(Gate);
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	Gate();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~Gate();
    
public:
    
	/**
	 * @brief	ゲートの初期化
	 * @retval	true 初期化に成功
	 * @retval	false 初期化に失敗
	 */
	bool init();
    
	/**
	 * @brief	ステージ構築完了イベント
	 * @param	stage [in] ステージ
	 */
	void onConstructedStage(Stage* stage);
    
	/**
	 * @brief	衝突イベント
	 * @param	target [in] 衝突相手
	 */
	void onCollisionDetected(GameObject* target);
    
private:
    
	// キャラクター
	GameObject* character;
};

#endif //__GATE_H__
