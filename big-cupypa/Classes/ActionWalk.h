﻿//
//  ActionWalk.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//
#ifndef __CUPYPA_ACTIONWALK_H__
#define __CUPYPA_ACTIONWALK_H__

#include "cocos2d.h"

/**
 * @brief	カピーさんの歩くアクションのクラス
 */
class ActionWalk : public cocos2d::Layer
{
public:

	/**
	 * @brief	アクションの生成
	 * @param	loop ループ回数
	 * @return	生成されたアクション
	 * @note	loopに-1を入れると、無限ループ
	 */
	static cocos2d::ActionInterval* create(int loop);
};

#endif //__CUPYPA_ACTIONWALK_H__
