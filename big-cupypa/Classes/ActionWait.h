//
//  ActionWait.h
//  cupypa_5_22
//
//  Created by 藤岡佑介 on 2014/05/23.
//
//

#ifndef __CUPYPA_ACTIONWAIT_H__
#define __CUPYPA_ACTIONWAIT_H__

#include "cocos2d.h"

/**
 * @brief	カピーさんの待機アクションのクラス
 */
class ActionWait : public cocos2d::Layer
{
public:
    
	/**
	 * @brief	アクションの生成
	 * @param	loop ループ回数
	 * @return	生成されたアクション
	 * @note	loopに-1を入れると、無限ループ
	 */
	static cocos2d::ActionInterval* create(int loop);
};

#endif //__CUPYPA_ACTIONWAIT_H__