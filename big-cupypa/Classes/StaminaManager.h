//
//  StaminaManager.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CUPYPA_STAMINAMANAGER_H__
#define __CUPYPA_STAMINAMANAGER_H__

#include "cocos2d.h"

// スタミナゲージ
class StaminaGaugeMenu;

/**
 * @brief	スタミナ管理
 */
class StaminaManager : public cocos2d::Layer
{
public:
    
	/**
	 * @brief	インスタンスの取得
	 * @return	スタミナマネージャ
	 */
	static StaminaManager* getInstance();
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	StaminaManager();
    
	/**
	 * @note
	 * インスタンスの複製を禁止する。
	 */
	StaminaManager(const StaminaManager&);
	void operator =(const StaminaManager&);
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~StaminaManager();
    
public:
    
	/**
	 * @brief	スタミナマネージャの初期化
	 * @retval	true 初期化に成功
	 * @retval	false 初期化に失敗
	 */
	bool init();
    
	/**
	 * @brief	スタミナの参照先の設定
	 * @param	stamina スタミナの参照先
	 */
	void setStamina(int* stamina);
    
	/**
	 * @brief	スタミナの値変更時の処理
	 */
	void onStaminaChanged();
    
private:
    
	// スタミナの参照先
	int* stamina;
    
	// スタミナゲージ
	StaminaGaugeMenu* gauge;
};


#endif //__CUPYPA_STAMINAMANAGER_H__
