//
//  Character.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/07.
//
//

#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include "ActionDefs.h"
#include "GameObject.h"

// 線分

/**
 * @brief   キャラクター
 */
class Character : public GameObject
{
public:
    // スタミナの最大値
    static const int MAX_STAMINA;
    
    // キャラクターのスケール
    static const float CUPY_SCALE;
    
private:
    
    // ステート
    typedef void (Character::*State)(void);
    
public:
    
    /**
     * @brief   キャラクターの生成
     * @return  キャラクター
     */
    CREATE_FUNC(Character);
    
protected:
    
    /**
     * @brief   デフォルトコンストラクタ
     */
    Character();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    virtual ~Character();
    
public:
    
    /**
     * @brief   キャラクターの初期化
     * @retval  true 初期化に成功
     * @retval  false 初期化に失敗
     */
    virtual bool init();
    
    /**
     * @brief   キャラクターの更新
     * @param   dt 経過時間
     */
    virtual void update(float dt);
    
    /**
     * @brief   方向の設定
     * @param   dir 方向
     */
    virtual void setDirection(float dir);
    
    /**
	 * @brief	待機要求
	 */
	void requestWait();
    
	/**
	 * @brief	徒歩要求
	 */
	void requestWalk();
    
	/**
	 * @brief	ダッシュ要求
	 */
	void requestDash();
    
	/**
	 * @brief	ジャンプ要求
	 */
	void requestJump();
    
	/**
	 * @brief	反転要求
	 */
	void requestTurn();
    
    /**
	 * @brief	衝突イベント
	 * @param	target 衝突相手
	 */
	virtual void onCollisionDetected(GameObject* target);
    
protected:
    
	/**
	 * @brief	スプライトの生成
	 * @return	スプライト
	 */
	virtual cocos2d::Sprite* createSprite();
    
	/**
	 * @brief	待機
	 */
	virtual void wait();
    
	/**
	 * @brief	移動
	 */
	virtual void move();
    
	/**
	 * @brief	ジャンプ
	 */
	virtual void jump();
    
private:
    
    /**
	 * @brief	下境界線衝突時の処理
	 */
	virtual void onCollidedBottomBorder();
    
	/**
	 * @brief	左境界線衝突時の処理
	 */
	virtual void onCollidedLeftBorder();
    
	/**
	 * @brief	右境界線衝突時の処理
	 */
	virtual void onCollidedRightBorder();
    
	/**
	 * @brief	上から下へ移動した場合の処理
	 */
	virtual void onChangedTopToBottom();
    
	/**
	 * @brief	アクションの変更
	 * @param	type アクションタイプ
	 */
	virtual void changeAction(ActionType type);
    
	/**
	 * @brief	ジャンプ状態の解除
	 */
	virtual void cancelJump();
    
	/**
	 * @brief	横との衝突処理
	 * @param	target 衝突相手
	 */
	virtual void collideSide(GameObject* target);
    
	/**
	 * @brief	底辺との衝突処理
	 * @param	target 衝突相手
	 */
	virtual void collideBottom(GameObject* target);
    
	/**
	 * @brief	横方向と線分判定
	 * @param	dir 左右
	 * @param	target ターゲット
	 * @retval	true 交差している
	 * @retval	false 交差していない
	 */
	virtual bool intersectSide(float dir, GameObject* target);
    
	/**
	 * @brief	下方向と線分判定
	 * @param	target ターゲット
	 * @retval	true 交差している
	 * @retval	false 交差していない
	 */
	virtual bool intersectBottom(GameObject* target);

private:
    
    // ステート
	State state;
    
	// アクション
	cocos2d::ActionInterval* action;
    
	// 視点方向
	float viewDir;
    
	// 移動速度
	float moveSpeed;
    
	// ジャンプ開始フラグ
	bool beginJump;
    
	// ジャンプフラグ
	bool isJumping;
    
	// ジャンプ力
	float jumpPower;
    
	// ダッシュフラグ
	bool isDashing;
    
	// スタミナ
	int stamina;
};

#endif //__CHARACTER_H__
