﻿//
//  ActionSpecial3.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __CUPYPA_ACTIONSPECIAL3_H__
#define __CUPYPA_ACTIONSPECIAL3_H__

#include "cocos2d.h"

/**
 * @brief	カピーさんのスペシャルアクション3クラス
 */
class ActionSpecial3
{
public:

	/**
	 * @brief	アクションの生成
	 * @param	target	アクションを実行する対象の座標
	 * @return	生成されたアクション
	 */
	static cocos2d::ActionInterval* create(const cocos2d::Point& target);
};

#endif //__CUPYPA_ACTIONSPECIAL3_H__
