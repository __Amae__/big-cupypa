//
//  StaminaManager.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "StaminaManager.h"

#include "StaminaManager.h"
#include "StaminaGaugeMenu.h"
#include "Character.h"

USING_NS_CC;

namespace
{
	// スタミナの最大値
	const int MAX_STAMINA = 100;
}

/**
 * @brief	インスタンスの取得
 * @return	スタミナマネージャ
 */
StaminaManager* StaminaManager::getInstance()
{
	// スタミナマネージャ
	static StaminaManager staminaManager;
    
	return &staminaManager;
}

/**
 * @brief	デフォルトコンストラクタ
 */
StaminaManager::StaminaManager()
: stamina(NULL)
, gauge(NULL)
{
	// 初期化
	init();
}

/**
 * @brief	デストラクタ
 */
StaminaManager::~StaminaManager()
{
}

/**
 * @brief	スタミナマネージャの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool StaminaManager::init()
{
	// スタミナゲージ生成
	gauge = StaminaGaugeMenu::create();
    
	// メニューを生成
	Menu* menu = Menu::create(gauge, NULL);
	addChild(menu);
    
	// 座標を設定
	Point p = Point(64.0f, 0.0f);
	menu->setPosition(p);
    
	Sprite* spr = Sprite::create("DashFrame.png");
	spr->setAnchorPoint(Point(0.0f, 0.0f));
	spr->setPosition(p);
	addChild(spr);
    
	return true;
}

/**
 * @brief	スタミナの参照先の設定
 * @param	stamina スタミナの参照先
 */
void StaminaManager::setStamina(int* stamina)
{
	// スタミナの参照先を設定
	this->stamina = stamina;
    
	// 値変更を通知
	onStaminaChanged();
}

/**
 * @brief	スタミナの値変更時の処理
 */
void StaminaManager::onStaminaChanged()
{
	assert(stamina);
	assert(gauge);
    
	// スケール
	float scale = *stamina;
	scale /= MAX_STAMINA;
    
	// スタミナを設定
	gauge->SetStamina(scale);
}
