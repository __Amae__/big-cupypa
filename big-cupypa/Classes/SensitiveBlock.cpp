//
//  SensitiveBlock.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "SensitiveBlock.h"
#include "SimpleAudioEngine.h"
#include "GameParameter.h"
#include "ObjectDefs.h"
#include "Stage.h"
#include "GameTimer.h"

USING_NS_CC;

namespace
{
	// 画像の数
	const unsigned int NUM_IMAGES = 5;
    
	// 1つの耐久度
	const float DURABILITY = 0.5f;
    
	// 初期耐久度
	const float MAX_DURABILITY = NUM_IMAGES * DURABILITY;
}

/**
 * @brief	デフォルトコンストラクタ
 */
SensitiveBlock::SensitiveBlock()
: state(NULL)
, durability(0.0f)
, damageLevel(0)
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_SENSITIVEBLOCK);
}

/**
 * @brief	デストラクタ
 */
SensitiveBlock::~SensitiveBlock()
{
}

/**
 * @brief	センシティブブロックの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool SensitiveBlock::init()
{
	// 基底クラスを初期化
	if (!GameObject::init())
	{
		return false;
	}
    
	// 衝突サイズを設定
	Size collisionSize;
	collisionSize.width = 31.0f;
	collisionSize.height = 31.0f;
	setCollisionSize(collisionSize);
    
	return true;
}

/**
 * @brief	センシティブブロックの更新
 * @param	dt [in] 経過時間
 */
void SensitiveBlock::update(float dt)
{
	Point p = getPosition();
    
	if (p.y >= (GameParameter::BASE_SCREEN_HEIGHT - GameParameter::BLOCK_HEIGHT))
	{
		Rect rect = createCollisionRect();
		rect.origin.y = GameParameter::BLOCK_HEIGHT;
        
		GameObjectArray objects;
		unsigned int numObjects = stage->collisionWith(rect, &objects);
        
		for (unsigned int i = 0; i < numObjects; ++i)
		{
			if (this != objects[i] && objects[i]->isMovable())
			{
				damage();
				break;
			}
		}
	}
}

/**
 * @brief	衝突イベント
 * @param	target [in] 衝突相手
 */
void SensitiveBlock::onCollisionDetected(GameObject* target)
{
	/*
     if (OBJECTTYPE_CHARACTER != target->getObjectType())
     {
     return;
     }
     */
    
	Point targetPoint = target->getNextPosition();
    
	Point point = getPosition();
    
	Size colSize = getCollisionSize();
    
	float halfWidth = colSize.width * 0.5f;
	float minX = point.x - halfWidth;
	float maxX = point.x + halfWidth;
    
	// X軸判定
	if (point.x < minX || maxX < point.x)
	{
		return;
	}
    
	// Y軸判定
	if ((point.y + (colSize.height * 0.5f)) > targetPoint.y)
	{
		return;
	}
    
	// ダメージ
	damage();
}

/**
 * @brief	スプライトの生成
 * @return	スプライト
 */
Sprite* SensitiveBlock::createSprite()
{
    Rect rect(0,
              0,
              GameParameter::BLOCK_WIDTH,
              GameParameter::BLOCK_HEIGHT);
    
	// スプライトを生成
	Sprite* sprite = Sprite::create("SensitiveBlock.png", rect);
    
	return sprite;
}

/**
 * @brief	ダメージ
 */
void SensitiveBlock::damage()
{
	// 耐久度を設定
	durability += GameTimer::getElapsedTime();
    
	float nextLevel = (damageLevel * DURABILITY) + 0.5f;
    
	if (nextLevel < durability)
	{
		// ダメージレベルを設定
		++damageLevel;
        
		// 破壊が進んだ場合の処理
		onDamage();
	}
    
    Rect r(GameParameter::BLOCK_WIDTH * damageLevel, 0, GameParameter::BLOCK_WIDTH, GameParameter::BLOCK_HEIGHT);
    
	// テクスチャの矩形を設定
	getSprite()->setTextureRect(r);
    
	if (MAX_DURABILITY <= durability)
	{
		// 壊れた場合の処理
		onBreaked();
        
		// 削除フラグを設定
		setDeleteFlag(true);
	}
}

/**
 * @brief	破壊が進んだ場合の処理
 */
void SensitiveBlock::onDamage()
{
	rand()%2?CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("GlassDamage1.mp3"):CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("GlassDamage2.mp3");
}

/**
 * @brief	壊れた場合の処理
 */
void SensitiveBlock::onBreaked()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("GlassBreak.mp3");
}