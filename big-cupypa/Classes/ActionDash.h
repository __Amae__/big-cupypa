﻿//
//  ActionDash.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __CUPYPA_ACTIONDASH_H__
#define __CUPYPA_ACTIONDASH_H__

#include "cocos2d.h"

/**
 * @brief	ダッシュアクション
 */
class ActionDash
{
public:

	/**
	 * @brief	アクションの生成
	 * @param	loop ループ回数
	 * @return	ダッシュアクション
	 */
	static cocos2d::ActionInterval* create(int loop);
};

#endif //__CUPYPA_ACTIONDASH_H__
