//
//  CollisionDetection.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "CollisionDetection.h"
#include "GameObject.h"

USING_NS_CC;

/**
 * @brief	è’ìÀîªíËÉIÉuÉWÉFÉNÉgÇÃê∂ê¨
 * @return	è’ìÀîªíËÉIÉuÉWÉFÉNÉg
 */
CollisionDetection* CollisionDetection::create()
{
	// è’ìÀîªíËÇê∂ê¨
	CollisionDetection* collision = new CollisionDetection();
    
	return collision;
}

/**
 * @brief	ÉfÉtÉHÉãÉgÉRÉìÉXÉgÉâÉNÉ^
 */
CollisionDetection::CollisionDetection()
: objects()
{
}

/**
 * @brief	ÉfÉXÉgÉâÉNÉ^
 */
CollisionDetection::~CollisionDetection()
{
}

/**
 * @brief	è’ìÀîªíËÇÃçXêV
 */
void CollisionDetection::update()
{
	for (GameObjectMultiMap::iterator itr = objects.begin();
         itr != objects.end(); ++itr)
	{
		// ÉIÅ[ÉiÅ[
		GameObject* own = itr->second;
        
		// é©ï™ÇÃéüÇÃÉCÉeÉåÅ[É^Ç©ÇÁäJén
		GameObjectMultiMap::iterator tmp = itr;
		++tmp;
        
		// è’ìÀîªíËÉãÅ[Év
		for (; tmp != objects.end(); ++tmp)
		{
			// É^Å[ÉQÉbÉg
			GameObject* target = tmp->second;
            
			// ÉIÅ[ÉiÅ[Ç™è’ìÀîªíËÇçsÇÌÇ»Ç¢èÍçá
			if (!own->isCollidable()) break;
            
			// è’ìÀîªíË
			if (own->checkCollision(target))
			{
				// è’ìÀÇí ím
				own->onCollisionDetected(target);
				target->onCollisionDetected(own);
			}
		}
	}
}

/**
 * @brief	è’ìÀÉIÉuÉWÉFÉNÉgÇÃí«â¡
 * @param	object [in] ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉg
 */
void CollisionDetection::addGameObject(GameObject* object)
{
	objects.insert(GameObjectMultiMap::value_type(object->getObjectType(), object));
}

/**
 * @brief	è’ìÀÉIÉuÉWÉFÉNÉgÇÃçÌèú
 * @param	object [in] ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉg
 */
void CollisionDetection::removeGameObject(GameObject* object)
{
	typedef GameObjectMultiMap::iterator Iterator;
	typedef std::pair<Iterator, Iterator> IteratorPair;
    
	IteratorPair pair = objects.equal_range(object->getObjectType());
    
	while (pair.first != pair.second)
	{
		if (object == pair.first->second)
		{
			// ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉgÉ}ÉãÉ`É}ÉbÉvÇ©ÇÁçÌèú
			objects.erase(pair.first);
			return;
		}
        
		// ÉCÉeÉåÅ[É^ÇêiÇﬂÇÈ
		++pair.first;
	}
}

/**
 * @brief	è’ìÀÉIÉuÉWÉFÉNÉgÇÃÉNÉäÉA
 */
void CollisionDetection::clear()
{
	// ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉgÉ}ÉãÉ`É}ÉbÉvÇÉNÉäÉA
	objects.clear();
}
