//
//  DashMenu.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/09.
//
//

#ifndef __CUPYPA_DASHMENU_H__
#define __CUPYPA_DASHMENU_H__

#include "cocos2d.h"
#include "cocos-ext.h"

/**
 * @brief	ダッシュメニュー
 */
class DashMenu : public cocos2d::Layer
{
private:
    
	/**
	 * @brief	ダッシュをリクエストする
	 */
	void requestDash();
    
	/**
	 * @brief	歩きをリクエストする
	 */
	void requestMove();
    
public:
    
	/**
	 * @brief	ダッシュメニューの生成
	 * @return	ダッシュメニュー
	 */
	CREATE_FUNC(DashMenu);
    
	/**
	 * @brief	初期化
	 * @retval	true	初期化に成功
	 * @retval	false	初期化に失敗
	 */
	bool init();
};

#endif //__CUPYPA_DASHMENU_H__
