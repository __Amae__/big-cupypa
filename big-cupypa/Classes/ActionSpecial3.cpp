﻿//
//  ActionSpecial3.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "ActionSpecial3.h"
#include "GameParameter.h"

USING_NS_CC;

/**
 * @brief	アクションの生成
 * @param	target	アクションを実行する対象の座標
 * @return	生成されたアクション
 */
ActionInterval* ActionSpecial3::create(const Point& target)
{
    // 移動するアクションを生成する
	ActionInterval* pMoveAction1 = MoveTo::create(4.0f, Point(GameParameter::BASE_SCREEN_WIDTH+32.0f, target.y ));
	ActionInterval* pMoveAction2 = MoveTo::create(0.0f, Point(-32.0f, target.y ));
	ActionInterval* pMoveAction3 = MoveTo::create(1.0f, Point(target.x, target.y ));

	// 加速しながら移動するアクションを生成する
	ActionInterval* pAccelMoveAction = EaseIn::create(pMoveAction1,5.0f);

	// 転がるアクションを生成する
	ActionInterval* pRotaAction = RotateBy::create(5.0f, 3600.0f);

	// アニメーションを組み合わせる
	ActionInterval* pSpecialActionMaterial = Sequence::create(pAccelMoveAction, pMoveAction2, pMoveAction3, NULL);

	// スペシャルアクションを生成する
	return Spawn::create(pSpecialActionMaterial, pRotaAction, NULL);
}


