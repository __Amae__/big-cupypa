//
//  GameObject.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

#include "GameObject.h"
#include "GameParameter.h"
#include "Stage.h"
#include "GameTimer.h"

USING_NS_CC;

namespace
{
    /**
     * @brief   ノード深度
     */
    enum NodeDepth
    {
        // スプライト
        NODEDEPTH_SPRITE,
    };
    
    /**
     * @brief   ノードタグ
     */
    enum NodeTag
    {
        // スプライト
        NODETAG_SPRITE,
    };
    
    // 重力加速度
    const float GRAVITSTIONSL_ACCELERATION = -9.8065 * 0.33f;
    
    // 重力の最大値
    const float MAX_GRAVITY = -15.0f;
}

/**
 * @brief   デフォルトコンストラクタ
 */
GameObject::GameObject()
: stage(NULL)
, _type(OBJECTTYPE_EMPTY)
, _sprite(NULL)
, _collisionSize()
, _isEnabled(true)
, _deleteFlag(false)
, _isCollidable(true)
, _isMountable(true)
, _isMovable(false)
, _nextPosition(Point(0.0f, 0.0f))
, _velocity(Point(0.0f, 0.0f))
, _gravity(0.0f)
{
}

/**
 * @brief   デストラクタ
 */
GameObject::~GameObject()
{
    //this->getEventDispatcher()->removeAllEventListeners();
}

/**
 * @brief   ゲームオブジェクトの初期化
 * @retval  true 初期化に成功
 * @retval  false 初期化に失敗
 */
bool GameObject::init()
{
    // 基底クラスを初期化
    if (!Layer::init())
    {
        return false;
    }
    
    // スプライトを生成
    _sprite = createSprite();
    
    if (_sprite)
    {
        // スプライトを追加
        this->addChild(_sprite, NODEDEPTH_SPRITE, NODETAG_SPRITE);
        
        // 衝突サイズ
        Size size;
        size.width = GameParameter::BLOCK_WIDTH;
        size.height = GameParameter::BLOCK_HEIGHT;
        
        //setScale(GameParameter::BLOCK_WIDTH , GameParameter::BLOCK_HEIGHT);
        
        // 衝突サイズを設定
        setCollisionSize(size);
    }
    
    return true;
}

/**
 * @brief   解放処理
 */
void GameObject::finalize()
{
    this->getEventDispatcher()->removeAllEventListeners();
}

/**
 * @brief   座標の更新
 */
void GameObject::updatePosition()
{
    if (!isMovable())
    {
        return;
    }
    
    // 上下のリピート判定
    checkVRepeat();
    
    // 左右のリピート判定
    checkHRepeat();
    
    // 座標を設定
    setPosition(_nextPosition);
}

/**
 * @brief   衝突判定
 * @param   point 座標
 * @retval  true 衝突している
 * @retval  false 衝突していない
 */
bool GameObject::checkCollision(const Point& point)
{
    if (!_sprite)
    {
        return false;
    }
    
    if (!isCollidable())
    {
        return false;
    }
    
    // 衝突判定矩形を生成
    Rect tmp = createCollisionRect();
    
    return tmp.containsPoint(point);
}

/**
 * @brief   衝突判定
 * @param   rect 矩形
 * @retval  true 衝突している
 * @retval  false 衝突していない
 */
bool GameObject::checkCollision(const Rect& rect)
{
    if (!_sprite)
    {
        return false;
    }
    
    if (!isCollidable())
    {
        return false;
    }
    
    // 衝突判定矩形を生成
    Rect tmp = createCollisionRect();
    
    return tmp.intersectsRect(rect);
}

/**
 * @brief   衝突判定
 * @param   segment 線分
 * @param   p1 点1
 * @param   p2 点2
 * @retval  true 衝突している
 * @retval  false 衝突していない
 */
bool GameObject::checkCollision(
        const Segment& segment,
        unsigned int p1,
        unsigned int p2)
{
    if (!_sprite)
    {
        return false;
    }
    
    if (!isCollidable())
    {
        return false;
    }
    
    // 線分の生成
    Segment tmp = createSegment(p1, p2);
    
    // 線分の交差判定
    return tmp.intersect(segment);
}


/**
 * @brief   衝突判定
 * @param   target ターゲット
 * @retval  true 衝突している
 * @retval  false 衝突していない
 */
bool GameObject::checkCollision(GameObject* target)
{
    if (!isCollidable())
    {
        return false;
    }
    
    // 衝突矩形
    Rect collisionRect = target->createCollisionRect();
    
    return checkCollision(collisionRect);
}

/**
 * @brief   オブジェクトタイプの取得
 * @return  オブジェクトタイプ
 */
ObjectType GameObject::getObjectType() const
{
    return _type;
}

/**
 * @brief   オブジェクトタイプの設定
 * @param   type [in] オブジェクトタイプ
 */
void GameObject::setObjectType(ObjectType type)
{
    // オブジェクトタイプを設定
    _type = type;
}

/**
 * @brief   スプライトの取得
 * @return  スプライト
 */
Sprite* GameObject::getSprite()
{
    return _sprite;
}

/**
 * @brief   衝突サイズの取得
 * @return  衝突サイズ
 */
Size GameObject::getCollisionSize() const
{
    return _collisionSize;
}

/**
 * @brief   衝突サイズの設定
 * @return  size 衝突サイズ
 */
void GameObject::setCollisionSize(const cocos2d::Size& size)
{
    // 衝突サイズを設定
    _collisionSize.width = size.width;
    _collisionSize.height = size.height;
}

/**
 * @brief   更新フラグの取得
 * @return  更新フラグ
 */
bool GameObject::isEnabled() const
{
    return _isEnabled;
}

/**
 * @brief   更新フラグの設定
 * @param   isEnabled 更新フラグ
 */
void GameObject::setEnabled(bool isEnabled)
{
    // 更新フラグを設定
    _isEnabled = isEnabled;
}

/**
 * @brief   削除フラグの取得
 * @return  削除フラグ
 */
bool GameObject::getDeleteFlag() const
{
    return _deleteFlag;
}

/**
 * @brief   削除フラグの設定
 * @param   deleteFlag 削除フラグ
 */
void GameObject::setDeleteFlag(bool deleteFlag)
{
    // 削除フラグを設定
    _deleteFlag = deleteFlag;
}

/**
 * @brief   衝突判定フラグの取得
 * @return  衝突判定フラグ
 */
bool GameObject::isCollidable() const
{
    return _isCollidable && !_deleteFlag;
}

/**
 * @brief   衝突判定フラグの設定
 * @return  isCollidable 衝突判定フラグ
 */
void GameObject::setCollidable(bool isCollidable)
{
    // 衝突判定フラグを設定
    _isCollidable = isCollidable;
}

/**
 * @brief   乗れるフラグの取得
 * @return  乗れるフラグ
 */
bool GameObject::isMountable() const
{
    return _isMountable;
}

/**
 * @brief   乗れるフラグの設定
 * @param   isMountable 乗れるフラグ
 */
void GameObject::setMountable(bool isMountable)
{
    // 乗れるフラグを設定
    _isMountable = isMountable;
}

/**
 * @brief   移動フラグの取得
 * @return  移動フラグ
 */
bool GameObject::isMovable() const
{
    return _isMovable;
}

/**
 * @brief   移動フラグの設定
 * @param   isMovable 移動フラグ
 */
void GameObject::setMovable(bool isMovable)
{
    // 移動フラグを設定
    _isMovable = isMovable;
}

/**
 * @brief   次の座標の取得
 * @return  次の座標
 */
Point GameObject::getNextPosition() const
{
    return _nextPosition;
}

/**
 * @brief   次の座標の設定
 * @param   positiomn 次の座標
 */
void GameObject::setNextPosition(const Point& position)
{
    // 次の座標を設定
    _nextPosition = position;
}

/**
 * @brief   次のX座標の設定
 * @param   x 次のX座標
 */
void GameObject::setNextPositionX(float x)
{
    // 次のX座標を設定
    _nextPosition.x = x;
}

/**
 * @brief   次のY座標の設定
 * @param   y 次のY座標
 */
void GameObject::setNextPositionY(float y)
{
    // 次のY座標を設定
    _nextPosition.y = y;
}

/**
 * @brief	速度の取得
 * @return	速度
 */
Point GameObject::getVelocity() const
{
    return _velocity;
}

/**
 * @brief	速度の設定
 * @param	velocity 速度
 */
void GameObject::setVelocity(const Point& velocity)
{
    // 速度を設定
    _velocity = velocity;
}

/**
 * @brief	X軸の速度の設定
 * @param	x X軸の速度
 */
void GameObject::setVelocityX(float x)
{
    // X軸の速度を設定
    _velocity.x = x;
}

/**
 * @brief	Y軸の速度の設定
 * @param	y Y軸の速度
 */
void GameObject::setVelocityY(float y)
{
    // Y軸の速度を設定
    _velocity.y = y;
}

/**
 * @brief   ステージ構築完了イベント
 * @param   stage ステージ
 */
void GameObject::onConstructedStage(Stage* stage)
{
    // ステージを設定
    this->stage = stage;
}

/**
 * @brief   衝突イベント
 * @param   target 衝突相手
 */
void GameObject::onCollisionDetected(GameObject* target)
{
}

/**
 * @brief   衝突判定矩形の生成
 * @return  衝突判定矩形
 */
Rect GameObject::createCollisionRect()
{
    Size size = getCollisionSize();
    Point p = Point(size.width * 0.5f, size.height * 0.5f);
    
    Rect rect;
    
    rect.origin = getPosition();
    rect.origin.x -= p.x;
    rect.origin.y -= p.y;
    
    rect.size = size;
    
    return rect;
}

/**
 * @brief   線分の生成
 * @param   p1 点1
 * @param   p2 点2
 * @return  線分
 */
Segment GameObject::createSegment(unsigned int p1, unsigned int p2)
{
    Size size = getCollisionSize();
    float halfWidth = size.width * 0.5f;
    float halfHeight = size.height * 0.5f;
    
    Point point = getPosition();
    
    Point p[4];
    
    p[0] = Point(point.x - halfWidth, point.y + halfHeight);
    p[1] = Point(point.x + halfWidth, point.y + halfHeight);
    p[2] = Point(point.x - halfWidth, point.y - halfHeight);
    p[3] = Point(point.x + halfWidth, point.y - halfHeight);
    
    Segment segment;
    segment.set(p[p1], p[p2]);
    
    return segment;
}

/**
 * @brief   次の座標の計算
 */
void GameObject::calcNextPosition()
{
    if (!isMovable())
    {
        return;
    }
    
    // 次の座標
    Point next = getPosition();
    
    // 速度を加算
    next.x += _velocity.x;
    next.y += _velocity.y;
    
    // 重力を加算
    next.y += _gravity;
    
    // 次の座標を指定
    _nextPosition = next;
}

/**
 * @brief   重力計算
 */
void GameObject::calcGravity()
{
    if (MAX_GRAVITY < _gravity)
    {
        // 経過時間
        float elapsedTime = GameTimer::getElapsedTime();
        
        // 重力加速度
        float gravitation = GRAVITSTIONSL_ACCELERATION * elapsedTime;
        
        // 重力加速度を加算
        _gravity += gravitation;
        
        if (MAX_GRAVITY > _gravity)
        {
            _gravity = MAX_GRAVITY;
        }
    }
}


/**
 * @brief   重力リセット
 */
void GameObject::resetGravity()
{
    // 重力を設定
    _gravity = 0.0f;
}

/**
 * @brief   スプライトの生成
 * @return  スプライト
 */
Sprite* GameObject::createSprite()
{
    unsigned int offset = _type - 1;
    
   // Rect rect(GameParameter::BLOCK_WIDTH * offset,
//              0.0f,
//              GameParameter::BLOCK_WIDTH,
//              GameParameter::BLOCK_HEIGHT);
    
    Rect rect(32.0f * offset,
              0.0f,
              32.0f,32.0f);
    
    // スプライトを生成
    Sprite* sprite = Sprite::create("Blocks.png", rect);
    
    sprite->setScale(1.25f, 1.25f);
    
    return sprite;
}

/**
 * @brief   上下のリピート判定
 */
void GameObject::checkVRepeat()
{
    // 次の座標
    Point next = getNextPosition();
    
    // 衝突サイズ
    Size colSize = getCollisionSize();
    float halfHeight = colSize.height * 0.5f;
    
    // キャラクターの上下
    float top = next.y + halfHeight;
    float bottom = next.y - halfHeight;
    
    // 上判定
    if (GameParameter::BASE_SCREEN_HEIGHT < top)
    {
        Point p = next;
        p.y = GameParameter::BLOCK_HEIGHT + GameParameter::HALF_BLOCK_HEIGHT;
        
        // 衝突判定
        if (stage->collisionAt(p, NULL))
        {
            onCollidedTopBorder();
            next.y = GameParameter::BASE_SCREEN_HEIGHT - halfHeight;
        }
        else
        {
            if (GameParameter::BASE_SCREEN_HEIGHT < bottom)
            {
                next.y = GameParameter::BLOCK_HEIGHT - halfHeight;
                onChangedTopToBottom();
            }
        }
    }
    // 下判定
    else if (GameParameter::BLOCK_HEIGHT > bottom)
    {
        Point p = next;
        p.y = GameParameter::BASE_SCREEN_HEIGHT - GameParameter::HALF_BLOCK_HEIGHT;
        
        // 衝突判定
        if (stage->collisionAt(p, NULL))
        {
            onCollidedBottomBorder();
            // 重力リセット
            resetGravity();
            next.y = GameParameter::BLOCK_HEIGHT + halfHeight;
        }
        else
        {
            if (GameParameter::BLOCK_HEIGHT > top)
            {
                next.y = GameParameter::BASE_SCREEN_HEIGHT + halfHeight;
                onChangedBottomToTop();
            }
        }
    }
    
    // 次のY座標を設定
    setNextPositionY(next.y);
}

/**
 * @brief   左右のリピート判定
 */
void GameObject::checkHRepeat()
{
    // 次の座標
    Point next = getNextPosition();
    
    // 衝突サイズ
    Size colSize = getCollisionSize();
    float halfWidth = colSize.width * 0.5f;
    
    // キャラクターの左右
    float left = next.x - halfWidth;
    float right = next.x + halfWidth;
    
    // 右判定
    if (GameParameter::BASE_SCREEN_WIDTH < right)
    {
        Point p = next;
        p.x = GameParameter::HALF_BLOCK_WIDTH;
        
        // 衝突判定
        if (stage->collisionAt(p, NULL))
        {
            onCollidedRightBorder();
            next.x = GameParameter::BASE_SCREEN_WIDTH - halfWidth;
        }
        else
        {
            if (GameParameter::BASE_SCREEN_WIDTH < next.x)
            {
                next.x = 0.0f;
                onChangedRightToLeft();
            }
        }
    }
    // 左判定
    else if (0.0f > left)
    {
        Point p = next;
        p.x = GameParameter::BASE_SCREEN_WIDTH - GameParameter::HALF_BLOCK_WIDTH;
        
        // 衝突判定
        if (stage->collisionAt(p, NULL))
        {
            onCollidedLeftBorder();
            next.x = halfWidth;
        }
        else
        {
            if (0.0f > next.x)
            {
                next.x = GameParameter::BASE_SCREEN_WIDTH;
                onChangedLeftToRight();
            }
        }
    }
    
    // 次のX座標を設定
    setNextPositionX(next.x);
}

/**
 * @brief   上境界線衝突時の処理
 */
void GameObject::onCollidedTopBorder()
{
}

/**
 * @brief   下境界線衝突時の処理
 */
void GameObject::onCollidedBottomBorder()
{
}

/**
 * @brief   左境界線衝突時の処理
 */
void GameObject::onCollidedLeftBorder()
{
}

/**
 * @brief   右境界線衝突時の処理
 */
void GameObject::onCollidedRightBorder()
{
}

/**
 * @brief   上から下へ移動した場合の処理
 */
void GameObject::onChangedTopToBottom()
{
}

/**
 * @brief   下から上へ移動した場合の処理
 */
void GameObject::onChangedBottomToTop()
{
}

/**
 * @brief   左から右へ移動した場合の処理
 */
void GameObject::onChangedLeftToRight()
{
}

/**
 * @brief   右から左へ移動した場合の処理
 */
void GameObject::onChangedRightToLeft()
{
}
