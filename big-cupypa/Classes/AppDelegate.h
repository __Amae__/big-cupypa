#ifndef  __CUPYPA_APPDELEGATE_H__
#define  __CUPYPA_APPDELEGATE_H__

#include "cocos2d.h"

/**
 * @brief cocos2d-x ver.3.0を使用したアプリケーション
 */
class  AppDelegate : private cocos2d::Application
{
private:
    
    /**
     * @brief	ディレクターの初期化
     * @return 	true  成功
     * @return 	false 失敗
     */
    virtual bool initDirector();
    
    /**
     * @brief	ビューの初期化
     * @return 	true  成功
     * @return 	false 失敗
     */
    virtual bool initView();
    
    
    // ディレクター
    cocos2d::Director* director;
    
    // ビュー
    cocos2d::GLView* view;

    
public:
    
    /**
     * @brief コンストラクタ
     */
    AppDelegate();
    
    /**
     * @brief デストラクタ
     */
    virtual ~AppDelegate();

    /**
     * @brief    アプリケーションの起動処理
     * @return true    成功
     * @return false   失敗
     */
    virtual bool applicationDidFinishLaunching();

    /**
     * @brief  アプリケーションの停止処理
     */
    virtual void applicationDidEnterBackground();

    /**
     * @brief  アプリケーションの再開処理
     */
    virtual void applicationWillEnterForeground();
};

#endif // __CUPYPA_APPDELEGATE_H__

