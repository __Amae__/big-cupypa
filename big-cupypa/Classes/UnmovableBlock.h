//
//  UnmovableBlock.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __UNMOVABLEBLOCK_H__
#define __UNMOVABLEBLOCK_H__

#include "GameObject.h"

/**
 * @brief   アンムーバブルブロック
 */
class UnmovableBlock : public GameObject
{
public:
    
    /**
     * @brief   アンムーバブルブロックの生成
     * @return  アンムーバブルブロック
     */
    CREATE_FUNC(UnmovableBlock);
    
protected:
    
    /**
     * @brief   デフォルトコンストラクタ
     */
    UnmovableBlock();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    virtual ~UnmovableBlock();
    
};

#endif //__UNMOVABLEBLOCK_H__
