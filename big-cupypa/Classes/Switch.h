//
//  Switch.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//


#ifndef __SWITCH_H__
#define __SWITCH_H__

#include "GameObject.h"

/**
 * @brief	スイッチ
 */
class Switch : public GameObject
{
public:
    
	/**
	 * @brief	スイッチの生成
	 * @return	スイッチ
	 */
	CREATE_FUNC(Switch);
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	Switch();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~Switch();
    
public:
    
	/**
	 * @brief	スイッチの初期化
	 * @retval	true 初期化に成功
	 * @retval	false 初期化に失敗
	 */
	bool init();
    
	/**
	 * @brief	ステージ構築完了イベント
	 * @param	stage [in] ステージ
	 */
	void onConstructedStage(Stage* stage);
    
	/**
	 * @brief	衝突イベント
	 * @param	target [in] 衝突相手
	 */
	void onCollisionDetected(GameObject* target);
    
protected:
    
	/**
	 * @brief	スプライトの生成
	 * @return	スプライト
	 */
	cocos2d::Sprite* createSprite();
    
private:
    
	// キャラクタ
	GameObject* character;
    
	// ゲート
	GameObject* gate;
};

#endif //__SWITCH_H__

