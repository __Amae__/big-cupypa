//
//  UnmovableBlock.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "UnmovableBlock.h"

USING_NS_CC;

/**
 * @brief   デフォルトコンストラクタ
 */
UnmovableBlock::UnmovableBlock()
{
    // オブジェクトタイプを設定
    setObjectType(OBJECTTYPE_UNMOVABLEBLOCK);
}

/**
 * @brief   デストラクタ
 */
UnmovableBlock::~UnmovableBlock()
{
}