﻿//
//  ActionJump.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//
#ifndef __CUPYPA_ACTIONJUMP_H__
#define __CUPYPA_ACTIONJUMP_H__

// アクションインターバル
namespace cocos2d { class ActionInterval; }

/**
 * @brief	ジャンプアクション
 */
class ActionJump
{
public:

	/**
	 * @brief	ジャンプアクションの生成
	 * @return	ジャンプアクション
	 */
	static cocos2d::ActionInterval* create();
};

#endif //__CUPYPA_ACTIONJUMP_H__
