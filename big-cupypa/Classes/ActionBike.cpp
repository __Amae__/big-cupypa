﻿//
//  ActionBike.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "ActionBike.h"

USING_NS_CC;

namespace
{
	/// パラパラアニメーションの更新ディレイ
	const float ANIMATION_DELAYPERUNIT = 0.05f;
}

/**
 * @brief	アクションの生成
 * @param	loop [in] ループ回数
 * @return	ダッシュアクション
 */
ActionInterval* ActionBike::create(int loop)
{
	// アニメーションを生成する
	Animation* pAnimation = Animation::create();

	// ファイルの名前から、アニメーションを作成する
	for( int i=1; i<=3; i++ )
	{
		char filename[100] = {0};
		sprintf(filename, "CupyBike_%02d.png", i);
		pAnimation->addSpriteFrameWithFile(filename);
	}

	// アニメーションの更新速度を設定する
	pAnimation->setDelayPerUnit(ANIMATION_DELAYPERUNIT);

	// アニメーションをループさせる
	pAnimation->setLoops(loop);

	// 作成したアニメーションを、アクションとして保存する
	ActionInterval* pAction = Animate::create(pAnimation);

	return pAction;
}
