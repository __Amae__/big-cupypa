#include "Button.h"

using namespace std;
USING_NS_CC;


/**
 *
 */
Button::Button(std::string filename, cocos2d::Rect* rect)
: mFilename(filename)
, mRect(rect)
{
}


/**
 *
 */
Button::~Button()
{
}


/**
 *
 */
Button* Button::create(std::string filename, cocos2d::Rect* rect)
{
    Button* button = new Button(filename, rect);
    
    if (button->init())
    {
        return button;
        
    } else {
        CC_SAFE_DELETE(button);
        return nullptr;
    }
}

/**
 *
 */
bool Button::init()
{
    if(mRect)
    {
        if (!Sprite::initWithFile(mFilename, *mRect))
        {
            return false;
        }
    }
    else
    {
        if (!Sprite::initWithFile(mFilename))
        {
            return false;
        }
    }
    
    
    return true;
}

/**
 *
 */
bool Button::isTouch(Point touch)
{
    Rect rect = this->getBoundingBox();
    if(rect.containsPoint(touch))
    {
        return true;
    }
    return false;
}