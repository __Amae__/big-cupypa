//
//  Gate.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "Gate.h"
#include "GameDefs.h"
#include "Stage.h"

USING_NS_CC;

/**
 * @brief	デフォルトコンストラクタ
 */
Gate::Gate()
: character(NULL)
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_GATE);
}

/**
 * @brief	デストラクタ
 */
Gate::~Gate()
{
}

/**
 * @brief	ゲートの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool Gate::init()
{
	// 基底クラスを初期化
	if (!GameObject::init())
	{
		return false;
	}
    
	// 衝突サイズ
	Size collisionSize;
	collisionSize.width = 2.0f;
	collisionSize.height = 2.0f;
    
	// 衝突サイズを設定
	setCollisionSize(collisionSize);
    
	// 乗れるフラグを設定
	setMountable(false);
    
    //	EffectManager::getInstance()->addEffect(EFFECTTYPE_GATE, this->getPosition());
    
	return true;
}

/**
 * @brief	ステージ構築完了イベント
 * @param	stage ステージ
 */
void Gate::onConstructedStage(Stage* stage)
{
	GameObject::onConstructedStage(stage);
    
	GameObjectArray tmp;
    
	// キャラクターを取得
	if (stage->getGameObjects(&tmp, OBJECTTYPE_CHARACTER))
	{
		// キャラクターを設定
		character = tmp[0];
	}
}

/**
 * @brief	衝突イベント
 * @param	target 衝突相手
 */
void Gate::onCollisionDetected(GameObject* target)
{
	// キャラクター以外の場合
	if (OBJECTTYPE_CHARACTER != target->getObjectType())
	{
		return;
	}
    
	// エリアを進む
	stage->advanceArea();
}
