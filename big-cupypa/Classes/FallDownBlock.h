//
//  FallDownBlock.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __FALLDOWNBLOCK_H__
#define __FALLDOWNBLOCK_H__

#include "GameObject.h"

class FalldownBlock : public GameObject
{
private:
    
    // ステート
    typedef void (FalldownBlock::*State)(float);
    
public:
    
    /**
     * @brief   フォールダウンブロックの生成
     * @return  フォールダウンブロック
     */
    CREATE_FUNC(FalldownBlock);
    
protected:
    
    /**
     * @brief   デフォルトコンストラクタ
     */
    FalldownBlock();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    virtual ~FalldownBlock();
    
public:
    
    /**
     * @brief   フォールダウンブロック
     * @retval  true 初期化に成功
     * @retval  false 初期化に失敗
     */
    virtual bool init();
    
    /**
	 * @brief	フォールダウンブロックの更新
	 * @param	dt 経過時間
	 */
	virtual void update(float dt);
    
    /**
	 * @brief	タッチ開始時の処理
	 * @param	touch タッチ情報
	 * @param	event イベント情報
	 * @retval	true タッチ判定を継続
	 * @retval	false タッチ判定を終了
	 */
	virtual bool onTouchBegan(
            cocos2d::Touch* touch,
            cocos2d::Event* event);
	
    /**
	 * @brief	衝突イベント
	 * @param	target [in] 衝突相手
	 */
	virtual void onCollisionDetected(GameObject* target);
    
private:
    
    /**
	 * @brief	通常の更新
	 * @param	dt 経過時間
	 */
	virtual void normalUpdate(float dt);
    
	/**
	 * @brief	タッチ後の更新
	 * @param	dt 経過時間
	 */
	virtual void touchedAfterUpdate(float dt);
    
	/**
	 * @brief	スプライトの生成
	 * @return	スプライト
	 */
	virtual cocos2d::Sprite* createSprite();
    
private:
    
    // ステート
    State state;
};

#endif //__FALLDOWNBLOCK_H__
