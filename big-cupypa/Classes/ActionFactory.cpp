//
//  ActionFactory.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "ActionFactory.h"
#include "ActionBike.h"
#include "ActionDash.h"
#include "ActionJump.h"
#include "ActionWalk.h"
#include "ActionWait.h"
#include "ActionSpecial1.h"
#include "ActionSpecial2.h"
#include "ActionSpecial3.h"

USING_NS_CC;

/**
 * @brief	カピーさんのアクションを生成する
 * @param	type 生成したいアクション
 * @param	target アクションを実行する対象の座標
 * @return	typeで指定されたアクション
 */
ActionInterval* ActionFactory::createAction(ActionType type, const Point& target)
{
	ActionInterval* pAction = NULL;
    
	switch(type)
	{
            // 不明
        case ACTIONTYPE_UNKNOWN:
            assert(false);
            break;
            // 待機
        case ACTIONTYPE_WAIT:
            pAction = ActionWait::create(-1);
            break;
            // 歩き
        case ACTIONTYPE_WALK:
            pAction = ActionWalk::create(-1);
            break;
            // バイク
        case ACTIONTYPE_BIKE:
            pAction = ActionBike::create(-1);
            break;
            // ダッシュ
        case ACTIONTYPE_DASH:
            pAction = ActionDash::create(-1);
            break;
            // ジャンプ
        case ACTIONTYPE_JUMP:
            pAction = ActionJump::create();
            break;
            // 15回歩き
        case ACTIONTYPE_WALK15:
            pAction = ActionWalk::create(15);
            break;
            // スペシャル1
        case ACTIONTYPE_SPECIAL1:
            pAction = ActionSpecial1::create(target);
            break;
            // スペシャル2
        case ACTIONTYPE_SPECIAL2:
            pAction = ActionSpecial2::create(target);
            break;
            // スペシャル3
        case ACTIONTYPE_SPECIAL3:
            pAction = ActionSpecial3::create(target);
            break;
        default:
            break;
	}
    
	return pAction;
}

/**
 * @param	type 生成したいアクション
 * @return	typeで指定されたアクション
 */
ActionInterval* ActionFactory::createAction(ActionType type)
{
	return ActionFactory::createAction(type, Point::ZERO);
}

/**
 * @param	type	生成したいアクション
 * @param	target  アクションを実行する対象のスプライト
 * @return  typeで指定されたアクション
 */
ActionInterval* ActionFactory::createAction(ActionType type, cocos2d::Sprite* target)
{
	return ActionFactory::createAction(type, target->getPosition());
}
