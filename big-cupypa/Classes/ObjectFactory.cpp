//
//  ObjectFactory.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "ObjectFactory.h"

#include "MovableBlock.h"
#include "UnmovableBlock.h"
#include "BugBlock.h"
#include "CleanerBlock.h"
#include "Switch.h"
#include "Warp.h"
#include "Gate.h"
#include "FallDownBlock.h"
#include "Character.h"
#include "SensitiveBlock.h"

#include "GameManager.h"

/**
 * @brief   オブジェクトの生成
 * @param   type オブジェクトタイプ
 * @return  ゲームオブジェクト
 */
GameObject* ObjectFactory::createObject(ObjectType type)
{
    GameObject* object = nullptr;
    
    switch (type) {
        case OBJECTTYPE_EMPTY:
            assert(false);
            break;
        case OBJECTTYPE_MOVABLEBLOCK:
            // ムーバブルブロックを生成
            object = MovableBlock::create();
            break;
        case OBJECTTYPE_UNMOVABLEBLOCK:
            // アンムーバブルブロックを生成
            object = UnmovableBlock::create();
            break;
        case OBJECTTYPE_BUGBLOCK:
            // バグブロックを生成
            object = BugBlock::create();
            break;
        case OBJECTTYPE_CLEANERBLOCK:
            // クリーナーブロックを生成
            object = CleanerBlock::create();
            break;
        case OBJECTTYPE_SWITCH:
            // スイッチを生成
            object = Switch::create();
            break;
        case OBJECTTYPE_WARP:
            // ワープを生成
            object = Warp::create();
            break;
        case OBJECTTYPE_GATE:
            // ゲートを生成
            object = Gate::create();
            break;
        case OBJECTTYPE_CHARACTER:
            // カピーさんを生成
            object = Character::create();
            GameManager::getInstance()->setCharacter(static_cast<Character*>(object));
            break;
        case OBJECTTYPE_FALLDOWNBLOCK:
            // フォールダウンブロックを生成
            object = FalldownBlock::create();
            break;
        case OBJECTTYPE_SENSITIVEBLOCK:
            // センシティブブロックを生成
            object = SensitiveBlock::create();
            break;
        default:
            assert(false);
            //object = UnmovableBlock::create();
            break;
    }
    
    return object;
}