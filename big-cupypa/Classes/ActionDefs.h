//
//  ActionDefs.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/07.
//
//

#ifndef __ACTIONDEFS_H__
#define __ACTIONDEFS_H__

/**
 * @brief	カピーさんのアクションタイプ
 */
enum ActionType
{
	/// 不明
	ACTIONTYPE_UNKNOWN = -1,
    
	/// 待機
	ACTIONTYPE_WAIT,
    
	/// 永遠に歩く
	ACTIONTYPE_WALK,
    
	/// バイク
	ACTIONTYPE_BIKE,
    
	/// ダッシュ
	ACTIONTYPE_DASH,
    
	/// ジャンプ
	ACTIONTYPE_JUMP,
    
	/// 15回歩く
	ACTIONTYPE_WALK15,
    
	/// スペシャル1
	ACTIONTYPE_SPECIAL1,
    
	/// スペシャル2
	ACTIONTYPE_SPECIAL2,
    
	/// スペシャル3
	ACTIONTYPE_SPECIAL3,
    
	/// アクションの数
	NUM_ACTIONS
};


#endif //__ACTIONDEFS_H__
