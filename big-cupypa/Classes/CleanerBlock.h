//
//  CleanerBlock.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CLEANERBLOCK_H__
#define __CLEANERBLOCK_H__

#include "MovableBlock.h"

/**
 * @brief	クリーナーブロック
 */
class CleanerBlock : public MovableBlock
{
private:
    
    // 基底クラス
    typedef MovableBlock Base;
    
public:
    
	/**
	 * @brief	クリーナーブロックの生成
	 * @return	クリーナーブロック
	 */
	CREATE_FUNC(CleanerBlock);
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	CleanerBlock();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~CleanerBlock();
    
public:
    
	/**
	 * @brief	タッチ終了時の処理
	 * @param	touch タッチ情報
	 * @param	event イベント情報
	 */
	void onTouchEnded(
            cocos2d::Touch* touch,
            cocos2d::Event* event);
};

#endif //__CLEANERBLOCK_H__
