#include "GameTimer.h"

USING_NS_CC;

// 経過時間
float GameTimer::elapsedTime = 0.0f;

// 時間
timeval GameTimer::timeVal;

/**
 * @brief	タイマーの初期化
 */
void GameTimer::init()
{
    timeval now;
    gettimeofday(&now, NULL);
    
    elapsedTime = (now.tv_sec - timeVal.tv_sec) + (now.tv_usec - timeVal.tv_usec) / 1000000.0f;
    elapsedTime = MAX(0, elapsedTime);
    
	// 時間を保存
	timeVal = now;
}

/**
 * @brief	タイマーの更新
 */
void GameTimer::update()
{
    timeval now;
    gettimeofday(&now, NULL);
    
    elapsedTime = (now.tv_sec - timeVal.tv_sec) + (now.tv_usec - timeVal.tv_usec) / 1000000.0f;
    elapsedTime = MAX(0, elapsedTime);
    
	// 時間を保存
	timeVal = now;
}
