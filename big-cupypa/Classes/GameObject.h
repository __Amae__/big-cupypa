//
//  GameObject.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

//
//  GameObject.h
//  CupypaObject
//
//  Created by Yusuke Fujioka on 2014/04/21.
//
//

#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include "cocos2d.h"
#include "ObjectDefs.h"
#include "Segment.h"

// ステージ
class Stage;

/**
 * @brief   ゲームオブジェクト
 */
class GameObject : public cocos2d::Layer
{
protected:
    
    /**
     * @brief   デフォルトコンストラクタ
     */
    GameObject();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    virtual ~GameObject();
    
public:
    
    /**
     * @brief   ゲームオブジェクトの初期化
     * @retval  true 初期化に成功
     * @retval  false 初期化に失敗
     */
    virtual bool init();
    
    /**
     * @brief   解放処理
     */
    virtual void finalize();
    
    /**
     * @brief   座標の更新
     */
    virtual void updatePosition();
    
    /**
     * @brief   衝突判定
     * @param   point 座標
     * @retval  true 衝突している
     * @retval  false 衝突していない
     */
    virtual bool checkCollision(const cocos2d::Point& point);
    
    /**
     * @brief   衝突判定
     * @param   rect 矩形
     * @retval  true 衝突している
     * @retval  false 衝突していない
     */
    virtual bool checkCollision(const cocos2d::Rect& rect);
    
    /**
     * @brief   衝突判定
     * @param   segment 線分
     * @param   p1 点1
     * @param   p2 点2
     * @retval  true 衝突している
     * @retval  false 衝突していない
     */
    virtual bool checkCollision(
                                const Segment& segment,
                                unsigned int p1,
                                unsigned int p2);
    
    /**
     * @brief   衝突判定
     * @param   target ターゲット
     * @retval  true 衝突している
     * @retval  false 衝突していない
     */
    virtual bool checkCollision(GameObject* target);
    
    /**
     * @brief   オブジェクトタイプの取得
     * @return  オブジェクトタイプ
     */
    ObjectType getObjectType() const;
    
    /**
     * @brief   オブジェクトタイプの設定
     * @param   type オブジェクトタイプ
     */
    void setObjectType(ObjectType type);
    
    /**
     * @brief   スプライトの取得
     * @return  スプライト
     */
    cocos2d::Sprite* getSprite();
    
    /**
     * @brief   衝突サイズの取得
     * @return  衝突サイズ
     */
    cocos2d::Size getCollisionSize() const;
    
    /**
     * @brief   衝突サイズの設定
     * @return  size 衝突サイズ
     */
    void setCollisionSize(const cocos2d::Size& size);
    
    /****************************************************************************/
    /** フラグ関連 **/
    
    /**
     * @brief   更新フラグの取得
     * @return  更新フラグ
     */
    bool isEnabled() const;
    
    /**
     * @brief   更新フラグの設定
     * @param   isEnabled 更新フラグ
     */
    void setEnabled(bool isEnabled);
    
    /**
     * @brief   削除フラグの取得
     * @return  削除フラグ
     */
    bool getDeleteFlag() const;
    
    /**
     * @brief   削除フラグの設定
     * @param   deleteFlag 削除フラグ
     */
    void setDeleteFlag(bool deleteFlag);
    
    /**
     * @brief   衝突判定フラグの取得
     * @return  衝突判定フラグ
     */
    bool isCollidable() const;
    
    /**
     * @brief   衝突判定フラグの設定
     * @return  isCollidable 衝突判定フラグ
     */
    void setCollidable(bool isCollidable);
    
    /**
     * @brief   乗れるフラグの取得
     * @return  乗れるフラグ
     */
    bool isMountable() const;
    
    /**
     * @brief   乗れるフラグの設定
     * @param   isMountable 乗れるフラグ
     */
    void setMountable(bool isMountable);
    
    /**
     * @brief   移動フラグの取得
     * @return  移動フラグ
     */
    bool isMovable() const;
    
    /**
     * @brief   移動フラグの設定
     * @param   isMovable 移動フラグ
     */
    void setMovable(bool isMovable);
    
    /****************************************************************************/
    /** 座標計算関連 **/
    
    /**
     * @brief   次の座標の取得
     * @return  次の座標
     */
    virtual cocos2d::Point getNextPosition() const;
    
    /**
     * @brief   次の座標の設定
     * @param   position 次の座標
     */
    virtual void setNextPosition(const cocos2d::Point& position);
    
    /**
     * @brief   次のX座標の設定
     * @param   x 次のX座標
     */
    virtual void setNextPositionX(float x);
    
    /**
     * @brief   次のY座標の設定
     * @param   y 次のY座標
     */
    virtual void setNextPositionY(float y);
    
    /**
     * @brief	速度の取得
     * @return	速度
     */
    virtual cocos2d::Point getVelocity() const;
    
    /**
     * @brief	速度の設定
     * @param	velocity 速度
     */
    virtual void setVelocity(const cocos2d::Point& velocity);
    
    /**
     * @brief	X軸の速度の設定
     * @param	x X軸の速度
     */
    virtual void setVelocityX(float x);
    
    /**
     * @brief	Y軸の速度の設定
     * @param	y Y軸の速度
     */
    virtual void setVelocityY(float y);
    
    /**
     * @brief   ステージ構築完了イベント
     * @param   stage ステージ
     */
    virtual void onConstructedStage(Stage* stage);
    
    /**
     * @brief   衝突イベント
     * @param   target 衝突相手
     */
    virtual void onCollisionDetected(GameObject* target);
    
protected:
    
    /**
     * @brief   衝突判定矩形の生成
     * @return  衝突判定矩形
     */
    cocos2d::Rect createCollisionRect();
    
    /**
     * @brief   線分の生成
     * @param   p1 点1
     * @param   p2 点2
     * @return  線分
     */
    Segment createSegment(unsigned int p1, unsigned int p2);
    
    /**
     * @brief   次の座標の計算
     */
    void calcNextPosition();
    
    /**
     * @brief   重力計算
     */
    void calcGravity();
    
    /**
     * @brief   重力リセット
     */
    void resetGravity();
    
    /**
     * @brief   スプライトの生成
     * @return  スプライト
     */
    virtual cocos2d::Sprite* createSprite();
    
private:
    
    /**
     * @brief   上下のリピート判定
     */
    void checkVRepeat();
    
    /**
     * @brief   左右のリピート判定
     */
    void checkHRepeat();
    
    /**
     * @brief   上境界線衝突時の処理
     */
    virtual void onCollidedTopBorder();
    
    /**
     * @brief   下境界線衝突時の処理
     */
    virtual void onCollidedBottomBorder();
    
    /**
     * @brief   左境界線衝突時の処理
     */
    virtual void onCollidedLeftBorder();
    
    /**
     * @brief   右境界線衝突時の処理
     */
    virtual void onCollidedRightBorder();
    
    /**
     * @brief   上から下へ移動した場合の処理
     */
    virtual void onChangedTopToBottom();
    
    /**
     * @brief   下から上へ移動した場合の処理
     */
    virtual void onChangedBottomToTop();
    
    /**
     * @brief   左から右へ移動した場合の処理
     */
    virtual void onChangedLeftToRight();
    
    /**
     * @brief   右から左へ移動した場合の処理
     */
    virtual void onChangedRightToLeft();
    
protected:
    
    // ステージ
    Stage* stage;
    
private:
    /****************************************************************************/
    
    // オブジェクトタイプ
    ObjectType _type;
    
    // スプライト
    cocos2d::Sprite* _sprite;
    
    // 衝突矩形のサイズ
    cocos2d::Size _collisionSize;
    
    /****************************************************************************/
    /** フラグ関連 **/
    
    // 更新フラグ
    bool _isEnabled;
    
    // 削除フラグ
    bool _deleteFlag;
    
    // 衝突判定フラグ
    bool _isCollidable;
    
    // 乗れるフラグ
    bool _isMountable;
    
    // 移動フラグ
    bool _isMovable;
    
    /****************************************************************************/
    /** 座標計算関連 **/
    
    // 次の座標
    cocos2d::Point _nextPosition;
    
    // 速度
    cocos2d::Point _velocity;
    
    // 重力
    float _gravity;
    
    /****************************************************************************/
    
};

#endif //__GAMEOBJECT_H__
