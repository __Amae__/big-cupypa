//
//  StaminaGaugeMenu.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CUPYPA_STAMINAGAUGEMENU_H__
#define __CUPYPA_STAMINAGAUGEMENU_H__

#include "cocos2d.h"

/**
 * @brief	スタミナゲージメニュー
 */
class StaminaGaugeMenu : public cocos2d::MenuItemImage
{
    
private:
	cocos2d::Sprite* m_pSprite; // スプライト
    
private:
    
	/**
	 * @brief   初期化
	 */
	bool init();

public:
	/**
	 * @brief	コンストラクタ
	 */
	StaminaGaugeMenu();
    
	/**
	 * @brief	デストラクタ
	 */
	~StaminaGaugeMenu();
    
	/**
	 * @brief	ゲージの生成
	 */
	CREATE_FUNC(StaminaGaugeMenu);
    
    /**
     * @brief 
     */
	static cocos2d::Menu* menu();
    
	/**
	 * @brief	スタミナゲージ表示
	 */
	void SetStamina(float scaleX);
};


#endif //__CUPYPA_STAMINAGAUGEMENU_H__
