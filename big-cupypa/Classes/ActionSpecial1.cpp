﻿//
//  ActionSpecial1.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "ActionSpecial1.h"

USING_NS_CC;

/**
 * @brief	アクションの生成
 * @param	target	アクションを実行する対象の座標
 * @return	生成されたアクション
 */
ActionInterval* ActionSpecial1::create(const Point& target)
{
	// ジャンプするアクションを生成する
	ActionInterval* pJumpAction = JumpBy::create(2.4f, Point(0.0f,0.0f), 64, 3 );

	// 右へと移動するアクションを生成する
	ActionInterval* pMoveToRightAction = MoveTo::create( 2.4f, Point(target.x + 192.0f, target.y));

	// ジャンプをしながら右へと移動するアクションを生成する
	Spawn* pJumpAndMoveToRightAction = Spawn::create(pJumpAction,pMoveToRightAction,NULL);

	// こけるアクションを生成する
	ActionInterval* pTumbleActionMaterial1 = RotateBy::create(0.1f, 90.0f);
	ActionInterval* pTumbleActionMaterial2 = MoveBy::create(0.1f,Point(0.0f,-8.0f));
	Spawn* pTumbleAction = Spawn::create(pTumbleActionMaterial1,pTumbleActionMaterial2,NULL);

	// 左へと移動するアクションを生成する
	ActionInterval* pMoveToLeftAction = MoveTo::create( 6.0f, Point(target.x, target.y ));

	// こけて左へと移動するアクションを生成する
	Spawn* pTumbleAndMoveToLeftAction = Spawn::create(pTumbleAction,pMoveToLeftAction,NULL);

	// 起き上がるアクションを生成する
	ActionInterval* pGetUpActionMaterial1 = RotateBy::create(0.1f, -90.0f);
	ActionInterval* pGetUpActionMaterial2 = MoveBy::create(0.1f,Point(0.0f,8.0f));
	Spawn* pGetUpAction = Spawn::create(pGetUpActionMaterial1,pGetUpActionMaterial2,NULL);

	// スペシャルアクションを生成する
	return (Sequence::create(pJumpAndMoveToRightAction, pTumbleAndMoveToLeftAction, pGetUpAction, NULL));
}
