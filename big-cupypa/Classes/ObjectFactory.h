//
//  ObjectFactory.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __OBJECTFACTORY_H__
#define __OBJECTFACTORY_H__

#include "ObjectDefs.h"

// ゲームオブジェクト
class GameObject;

/**
 * @brief   オブジェクトの生成
 */
class ObjectFactory
{
private:
    
    /**
     * @brief   デフォルトコンストラクタ
     * @note    インスタンスの生成を禁止する
     */
    ObjectFactory();
    
public:
    
    /**
     * @brief   オブジェクトの生成
     * @param   type オブジェクトタイプ
     * @return  ゲームオブジェクト
     */
    static GameObject* createObject(ObjectType type);
};

#endif //__OBJECTFACTORY_H__
