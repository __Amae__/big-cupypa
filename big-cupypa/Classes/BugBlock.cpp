//
//  BugBlock.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "BugBlock.h"

/**
 * @brief	デフォルトコンストラクタ
 */
BugBlock::BugBlock()
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_BUGBLOCK);
}

/**
 * @brief	デストラクタ
 */
BugBlock::~BugBlock()
{
}
