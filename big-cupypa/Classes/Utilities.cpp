//
//  Utilities.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "GameParameter.h"
#include "Utilities.h"

USING_NS_CC;

namespace
{
    // オフセット
    const cocos2d::Point OFFSET = Point(16.0f, 16.0f);
}

namespace Utils
{
    /**
     * @brief   タイルマップ座標への変換
     * @param   point 座標の格納先
     * @param   x X軸のインデックス
     * @param   y Y軸のインデックス
     */
    void convertoTiledMapPosition(cocos2d::Point* point, unsigned int indexX, unsigned int indexY)
    {
        // 座標
        Point p = Point(OFFSET.x + indexX * GameParameter::BLOCK_WIDTH,
                        OFFSET.y + indexY * GameParameter::BLOCK_HEIGHT);
        
        // 座標を設定
        *point = p;
    }
    
    /**
     * @brief   座標変換
     * @param   point 座標
     */
    void convertPosition(cocos2d::Point* point)
    {
        Point tmp = *point;
        
        // クランプ
        tmp.x = clampf(tmp.x, 0.0f, GameParameter::BASE_SCREEN_WIDTH);
        tmp.y = clampf(tmp.y, 0.0f, GameParameter::BASE_SCREEN_HEIGHT);
        
        // インデックス
        unsigned int indexX = static_cast<unsigned int>(tmp.x / GameParameter::BLOCK_WIDTH);
        unsigned int indexY = static_cast<unsigned int>(tmp.y / GameParameter::BLOCK_HEIGHT);
        
        // 座標
        Point p;
        
        // 座標変換
        Utils::convertoTiledMapPosition(&p, indexX, indexY);
        
        // 座標を設定
        *point = p;
    }
    
    /**
     * @brief   時間を秒に変換
     * @param   time 時間(f)
     */
    int convertTimeToSecond(int time)
    {
        return time / 60 % 60;
    }
    
    /**
     * @brief   時間を分に変換
     * @param   time 時間(f)
     */
    int convertTimeToMinute(int time)
    {
        return time / 60 / 60;
    }
}

