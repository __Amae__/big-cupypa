//
//  Segment.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "Segment.h"

USING_NS_CC;

namespace Utils
{
	float cross(const cocos2d::Point& v1, const cocos2d::Point& v2)
	{
		float value = v1.x * v2.y - v1.y * v2.x;
		return value;
	}
}

/**
 * @brief	デフォルトコンストラクタ
 */
Segment::Segment()
: start(Point(0.0f, 0.0f))
, vector(Point(0.0f, 0.0f))
{
}

/**
 * @brief	デストラクタ
 */
Segment::~Segment()
{
}

/**
 * @brief	線分同士の交差判定
 * @param	target ターゲット
 * @retval	true 交差している
 * @retval	false 交差していない
 */
bool Segment::intersect(const Segment& target)
{
	Point v = target.start - start;
    
	float crossV1_V2 = Utils::cross(vector, target.vector);
    
	if (crossV1_V2 == 0.0f)
	{
		return false;
	}
    
	float crossV_V1 = Utils::cross(v, vector);
	float crossV_V2 = Utils::cross(v, target.vector);
    
	float t1 = crossV_V2 / crossV1_V2;
	float t2 = crossV_V1 / crossV1_V2;
    
	static const float eps = 0.00001f;
	if (t1 + eps < 0.0f || t1 - eps > 1.0f || t2 + eps < 0.0f || t2 - eps > 1.0f)
	{
		return false;
	}
    
	return true;
}

/**
 * @brief	座標の設定
 * @param	p1 点1
 * @param	p2 点2
 */
void Segment::set(const cocos2d::Point& p1, const cocos2d::Point& p2)
{
	start = p1;
	vector = p2 - p1;
}
