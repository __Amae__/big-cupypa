//
//  MovableBlock.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#include "MovableBlock.h"
#include "GameParameter.h"
#include "Utilities.h"
#include "Stage.h"

USING_NS_CC;

bool MovableBlock::isSmooth = true;

/**
 * @brief   デフォルトコンストラクタ
 */
MovableBlock::MovableBlock()
: moveSprite(NULL)
, alpha(1.0f)
{
    // オブジェクトタイプを設定
    setObjectType(OBJECTTYPE_MOVABLEBLOCK);
}

/**
 * @brief   デストラクタ
 */
MovableBlock::~MovableBlock()
{
}

/**
 * @brief   ムーバブルブロックの初期化
 * @retval  true 初期化に成功
 * @retval  false 初期化に失敗
 */
bool MovableBlock::init()
{
    // 基底クラスを初期化
    if(!GameObject::init())
    {
        return false;
    }
    
    // イベントリスナーの定義
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(MovableBlock::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(MovableBlock::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(MovableBlock::onTouchEnded, this);
    
    // タッチイベントを追加
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

/**
 * @brief   移動可能ブロックの更新
 * @param   dt 経過時間
 */
void MovableBlock::update(float dt)
{
    if(moveSprite)
    {
        static float v = 0.1f;
        alpha += v;
        
        if(0.0f > alpha)
        {
            v = 0.1f;
            alpha = 0.0f;
        }
        else if(1.0f < alpha)
        {
            v = -0.1f;
            alpha = 1.0f;
        }
        
        unsigned char opacity = alpha * 255;
        
        getSprite()->setOpacity(opacity);
        moveSprite->setOpacity(opacity);
    }
}

/**
 * @brief	タッチ開始時の処理
 * @param	touch タッチ情報
 * @param	event イベント情報
 * @retval	true タッチ判定を継続
 * @retval	false タッチ判定を終了
 */
bool MovableBlock::onTouchBegan(
                                cocos2d::Touch* touch,
                                cocos2d::Event* event)
{
	// タッチ座標
	Point touchPoint = touch->getLocation();
    
	// 衝突判定
	if (checkCollision(touchPoint))
	{
		// スプライトを生成
		moveSprite = createSprite();
		moveSprite->setScale(2.0f);
		alpha = 255;
		moveSprite->setOpacity(alpha);
		addChild(moveSprite);
        
		return true;
	}
    
	return false;
}

/**
 * @brief	タッチ移動時の処理
 * @param	touch タッチ情報
 * @param	event イベント情報
 */
void MovableBlock::onTouchMoved(
                                cocos2d::Touch* touch,
                                cocos2d::Event* event)
{
	// タッチ座標
	Point touchPoint = touch->getLocation();
    
	Point offset = getPosition();
    
	if (!MovableBlock::isSmooth)
	{
		// 座標変換
		Utils::convertPosition(&touchPoint);
		Utils::convertPosition(&offset);
	}
    
	touchPoint.x -= offset.x;
	touchPoint.y -= offset.y;
    
	// 座標を設定
	moveSprite->setPosition(touchPoint);
}

/**
 * @brief	タッチ終了時の処理
 * @param	touch タッチ情報
 * @param	event イベント情報
 */
void MovableBlock::onTouchEnded(
                                cocos2d::Touch* touch,
                                cocos2d::Event* event)
{
	// 座標
	Point touchPoint = touch->getLocation();
    
	// 座標変換
	Utils::convertPosition(&touchPoint);
    
	if (GameParameter::BLOCK_HEIGHT <= touchPoint.y &&			// 上下画面外判定
        GameParameter::BASE_SCREEN_WIDTH >= touchPoint.x)	// 左右画面外判定
	{
		// 衝突判定
		GameObjectArray objects;
		unsigned int numCollidedObjects = stage->collisionAt(touchPoint, &objects);
        
		bool isCollided = false;
		for (unsigned int i = 0; i < numCollidedObjects; ++i)
		{
			// 自分以外と衝突している場合
			if (objects[i] != this)
			{
				isCollided = true;
				break;
			}
		}
        
		if (!isCollided)
		{
			Rect rect;
			rect.origin = touchPoint;
			rect.size = getCollisionSize();
			rect.size.width *= 0.5f;
			rect.size.height *= 0.5f;
			rect.origin.x -= rect.size.width * 0.5f;
			rect.origin.y -= rect.size.height * 0.5f;
            
			if (!stage->collisionWith(rect, NULL))
			{
				// 座標を設定
				setPosition(touchPoint);
			}
		}
	}
    
	removeChild(moveSprite);
	moveSprite = NULL;
    
	getSprite()->setOpacity(255);
}

/**
 * @brief	スムーズフラグの設定
 * @param	isSmooth スムーズフラグ
 * @memo
 * true スムーズに移動
 * false グリッド基準で移動
 */
void MovableBlock::setSmooth(bool isSmooth)
{
	// スムーズフラグを設定
	MovableBlock::isSmooth = isSmooth;
}