//
//  CollisionDetection.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __CUPYPA_COLLISIONDETECTION_H__
#define __CUPYPA_COLLISIONDETECTION_H__

#include <map>
#include "ObjectDefs.h"

// ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉg
class GameObject;

/**
 * @brief	è’ìÀîªíË
 */
class CollisionDetection
{
private:
    
	// ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉgÉ}ÉãÉ`É}ÉbÉv
	typedef std::multimap<ObjectType, GameObject*> GameObjectMultiMap;
    
public:
    
	/**
	 * @brief	è’ìÀîªíËÉIÉuÉWÉFÉNÉgÇÃê∂ê¨
	 * @return	è’ìÀîªíËÉIÉuÉWÉFÉNÉg
	 */
	static CollisionDetection* create();
    
private:
    
	/**
	 * @brief	ÉfÉtÉHÉãÉgÉRÉìÉXÉgÉâÉNÉ^
	 */
	CollisionDetection();
    
public:
    
	/**
	 * @brief	ÉfÉXÉgÉâÉNÉ^
	 */
	~CollisionDetection();
    
public:
    
	/**
	 * @brief	è’ìÀîªíËÇÃçXêV
	 */
	void update();
    
	/**
	 * @brief	è’ìÀÉIÉuÉWÉFÉNÉgÇÃí«â¡
	 * @param	object [in] ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉg
	 */
	void addGameObject(GameObject* object);
    
	/**
	 * @brief	è’ìÀÉIÉuÉWÉFÉNÉgÇÃçÌèú
	 * @param	object [in] ÉQÅ[ÉÄÉIÉuÉWÉFÉNÉg
	 */
	void removeGameObject(GameObject* object);
    
	/**
	 * @brief	è’ìÀÉIÉuÉWÉFÉNÉgÇÃÉNÉäÉA
	 */
	void clear();
    
private:
    
	// è’ìÀÉIÉuÉWÉFÉNÉg
	GameObjectMultiMap objects;
};

#endif // CUPYPA_COLLISIONDETECTION_H
