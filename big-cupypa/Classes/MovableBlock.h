//
//  MovableBlock.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/01.
//
//

#ifndef __MOVABLEBLOCK_H__
#define __MOVABLEBLOCK_H__

#include "GameObject.h"

/**
 * @brief   移動可能ブロック
 */
class MovableBlock : public GameObject
{
public:
    
    /**
     * @brief   移動可能ブロックの生成
     * @return  移動可能ブロック
     */
    CREATE_FUNC(MovableBlock);
    
protected:
    
    /**
     * @brief   デフォルトコンストラクタ
     */
    MovableBlock();
    
public:
    
    /**
     * @brief   デストラクタ
     */
    virtual ~MovableBlock();
    
public:
    
    /**
     * @brief   移動可能ブロックの初期化
     * @return  true 初期化に成功
     * @return  false 初期化に失敗
     */
    virtual bool init();
    
    /**
     * @brief   移動可能ブロックの更新
     * @param   dt 経過時間
     */
    virtual void update(float dt);
    
    /**
     * @brief   タッチ開始時の処理
     * @param   touch タッチ情報
     * @param   event イベント情報
     * @return  true タッチ判定を継続
     * @return  false タッチ判定を終了
     */
    virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    
    /**
     * @brief   タッチ移動時の処理
     * @param   touch タッチ情報
     * @param   event イベント情報
     */
    virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    
    /**
     * @brief   タッチ終了時の処理
     * @param   touch タッチ情報
     * @param   event イベント情報
     */
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    /**
	 * @brief	スムーズフラグの設定
	 * @param	isSmooth スムーズフラグ
	 * @memo
	 * true スムーズに移動
	 * false グリッド基準で移動
	 */
    static void setSmooth(bool isSmooth);

private:

    // スムーズフラグ
    static bool isSmooth;

    // 移動スプライト
    cocos2d::Sprite* moveSprite;

    // 透過度
    float alpha;
};

#endif //__MOVABLEBLOCK_H__
