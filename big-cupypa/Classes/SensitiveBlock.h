//
//  SensitiveBlock.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#ifndef __SENSITIVEBLOCK_H__
#define __SENSITIVEBLOCK_H__

#include "GameObject.h"

/**
 * @brief	センシティブブロック
 */
class SensitiveBlock : public GameObject
{
private:
    
	// ステート
	typedef void (SensitiveBlock::*State)(void);
    
public:
    
	/**
	 * @brief	センシティブブロックの生成
	 * @return	センシティブブロック
	 */
	CREATE_FUNC(SensitiveBlock);
    
private:
    
	/**
	 * @brief	デフォルトコンストラクタ
	 */
	SensitiveBlock();
    
public:
    
	/**
	 * @brief	デストラクタ
	 */
	~SensitiveBlock();
    
public:
    
	/**
	 * @brief	センシティブブロックの初期化
	 * @retval	true 初期化に成功
	 * @retval	false 初期化に失敗
	 */
	bool init();
    
	/**
	 * @brief	センシティブブロックの更新
	 * @param	dt [in] 経過時間
	 */
	void update(float dt);
    
	/**
	 * @brief	衝突イベント
	 * @param	target [in] 衝突相手
	 */
	void onCollisionDetected(GameObject* target);
    
private:
    
	/**
	 * @brief	スプライトの生成
	 * @return	スプライト
	 */
	cocos2d::Sprite* createSprite();
    
	/**
	 * @brief	ダメージ
	 */
	void damage();
    
	/**
	 * @brief	破壊が進んだ場合の処理
	 */
	void onDamage();
    
	/**
	 * @brief	壊れた場合の処理
	 */
	void onBreaked();
    
private:
    
	// ステート
	State state;
    
	// 耐久度
	float durability;
    
	// ダメージの進行度
	int damageLevel;
};

#endif // CUPYPA_SENSITIVEBLOCK_H
