//
//  StaminaGaugeMenu.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "StaminaGaugeMenu.h"
#include <cmath>
#include <algorithm>

USING_NS_CC;

namespace
{
	// ノードのZオーダー
	enum NodeZOrder
    {
		GAUGE_ZORDER,   // ゲージ
	};
	// ノードのタグ
	enum NodeTag
    {
		GAUGE_TAG,      // ゲージのタグ
	};
    
	/**
	 * ここからしばらくHSVによる色調整
	 */
	float Fraction( float v )
    {
		return v - floor(v);
	}
    
	struct RGB
    {
		RGB(){}
        
		RGB( float r_, float g_, float b_ ) : r(r_),g(g_),b(b_){}
        
		float r,g,b;	// [0.0f, 1.0f]
	};
	
    struct HSV
    {
		HSV(){}
        
		HSV( float h_, float s_, float v_ ) : h(h_),s(s_),v(v_){}
        
		float h;	// ... 0°==0.0f, 360°==1.0f ...
        
		float s,v;	// [0.0f, 1.0f]
	};
	
    RGB HSVtoRGB( float a, float b, float c )
    {
		const float h = Fraction(a);
		const float s = b;
		const float v = c;
		const float hueF = h * 6.0f;
		const int hueI = static_cast<int>(hueF);
		const float fr = hueF - hueI;
		const float m = v * (1.0f-s);
		const float n = v * (1.0f-s*fr);
		const float p = v * (1.0f-s*(1.0f-fr));
		RGB rgb;
		
        switch(hueI)
        {
			case 0: rgb.r = v; rgb.g = p; rgb.b = m; break;
			case 1: rgb.r = n; rgb.g = v; rgb.b = m; break;
			case 2: rgb.r = m; rgb.g = v; rgb.b = p; break;
			case 3: rgb.r = m; rgb.g = n; rgb.b = v; break;
			case 4: rgb.r = p; rgb.g = m; rgb.b = v; break;
			default: rgb.r = v; rgb.g = m; rgb.b = n; break;
		}
		return rgb;
	
    }
	HSV RGBtoHSV( const RGB& rgb )
    {
		const float min = std::min(std::min(rgb.r,rgb.g),rgb.b);
		const float max = std::max(std::max(rgb.r,rgb.g),rgb.b);
		HSV hsv(0.0f,0.0f,max);
		const float delta = max - min;
		
        if(delta != 0.0f)
        {
			hsv.s = delta / max;
			
            if(rgb.r == max)
            {
				hsv.h = (rgb.g - rgb.b) / delta;
			}
            else if(rgb.g==max)
            {
				hsv.h = 2.0f + (rgb.b - rgb.r) / delta;
			}
            else
            {
				hsv.h = 4.0f + (rgb.r - rgb.g) / delta;
			}
			
            hsv.h /= 6.0f;
			
            if(hsv.h < 0.0f)
            {
				hsv.h += 1.0f;
			}
		}
		return hsv;
	}
	/**
	 * HSV色調整ここまで
	 */
}

/**
 * @brief	デフォルトコンストラクタ
 */
StaminaGaugeMenu::StaminaGaugeMenu()
{
}

/**
 * @brief  デストラクタ
 */
StaminaGaugeMenu::~StaminaGaugeMenu()
{
}

/**
 * @brief   初期化処理
 */
bool StaminaGaugeMenu::init()
{
    if (!MenuItemImage::init())
    {
		return false;
	}
	
    // 生成
	m_pSprite = CCSprite::create("gauge.png");
	
    // 基点を設定
	m_pSprite->setAnchorPoint(Point(0, 0));
	
    // 画面に追加
	this->addChild(m_pSprite, GAUGE_ZORDER, GAUGE_TAG);
    
	return true;
}

/**
 * @brief	スタミナゲージを表示
 */
void StaminaGaugeMenu::SetStamina(float scaleX)
{
    // 色調整
	float h = scaleX * 0.65f;	//  一周すると赤に戻る為適当な値で周回を止める
	float s = 1.0f;
	float v = 1.0f;
	
    //色指定
	Color3B color = { static_cast<GLubyte>(HSVtoRGB(h, s, v).r * 255),
                      static_cast<GLubyte>(HSVtoRGB(h, s, v).g * 255),
                      static_cast<GLubyte>(HSVtoRGB(h, s, v).b * 255) };
	
    // スケールを反映
	m_pSprite->setScaleX(scaleX*7);
	
    // 色を反映
	m_pSprite->setColor(color);
}
