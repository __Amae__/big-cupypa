//
//  ActionWait.cpp
//  cupypa_5_22
//
//  Created by 藤岡佑介 on 2014/05/23.
//
//

#include "ActionWait.h"
#include "Character.h"

USING_NS_CC;

namespace
{
	// パラパラアニメーションの更新速度
	const float ANIMATION_DELAYPERUNIT = 0.15f;
}

/**
 * @brief	アクションの生成
 * @param	loop ループ回数
 * @return	生成されたアクション
 * @note	loopに-1を入れると、無限ループ
 */
ActionInterval* ActionWait::create(int loop)
{
    // アニメーションを生成する
	Animation* pAnimation = Animation::create();
    
	// ファイルの名前から、アニメーションを作成する
//	for( int i=1; i<=3; i++ )
//	{
//		char filename[100] = {0};
//		sprintf(filename, "CupyBike_%02d.png", i);
//		pAnimation->addSpriteFrameWithFile(filename);
//	}
    
	// アニメーションの更新速度を設定する
	pAnimation->setDelayPerUnit(ANIMATION_DELAYPERUNIT);
    
	// アニメーションをループさせる
	pAnimation->setLoops(loop);
    
	// 作成したアニメーションを、アクションとして保存する
	ActionInterval* pAction = Animate::create(pAnimation);
    
	return pAction;
}