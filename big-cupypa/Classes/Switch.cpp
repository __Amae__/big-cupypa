//
//  Switch.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "Switch.h"
#include "GameDefs.h"
#include "Stage.h"

USING_NS_CC;

/**
 * @brief	デフォルトコンストラクタ
 */
Switch::Switch()
: character(NULL)
, gate(NULL)
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_SWITCH);
}

/**
 * @brief	デストラクタ
 */
Switch::~Switch()
{
}

/**
 * @brief	スイッチの初期化
 * @retval	true 初期化に成功
 * @retval	false 初期化に失敗
 */
bool Switch::init()
{
	// 基底クラスを初期化
	if (!GameObject::init())
	{
		return false;
	}
    
	// 衝突サイズ
	Size collisionSize;
	collisionSize.width = 2.0f;
	collisionSize.height = 2.0f;
    
	// 衝突サイズを設定
	setCollisionSize(collisionSize);
    
	// 乗れるフラグを設定
	setMountable(false);
    
	return true;
}

/**
 * @brief	ステージ構築完了イベント
 * @param	stage [in] ステージ
 */
void Switch::onConstructedStage(Stage* stage)
{
	GameObject::onConstructedStage(stage);
    
	GameObjectArray tmp;
    
	// キャラクターを取得
	if (stage->getGameObjects(&tmp, OBJECTTYPE_CHARACTER))
	{
		// キャラクターを設定
		character = tmp[0];
		tmp.clear();
	}
    
	// ゲートを取得
	if (stage->getGameObjects(&tmp, OBJECTTYPE_GATE))
	{
		// ゲートを設定
		gate = tmp[0];
        
		// 描画フラグを設定
		gate->setVisible(false);
        
		// 衝突フラグを設定
		gate->setCollidable(false);
	}
}

/**
 * @brief	衝突イベント
 * @param	target [in] 衝突相手
 */
void Switch::onCollisionDetected(GameObject* target)
{
	// キャラクター以外の場合
	if (OBJECTTYPE_CHARACTER != target->getObjectType())
	{
		return;
	}
    
	// 描画フラグを設定
	gate->setVisible(true);
    
	// 衝突フラグを設定
	gate->setCollidable(true);
    
	// 削除フラグを設定
	//setDeleteFlag(true);
    
	// スプライトを取得
	Sprite* oldSpr = getSprite();
    
	// スプライトを削除
	removeChild(oldSpr);
    
	// 新しいスプライトの矩形
	Rect rect(32.0f, 0.0f, 32.0f, 32.0f);
    
	// スプライトを生成
	Sprite* spr = Sprite::create("Switch.png", rect);
	addChild(spr);
    
	// 衝突判定フラグを設定
	setCollidable(false);
}

/**
 * @brief	スプライトの生成
 * @return	スプライト
 */
cocos2d::Sprite* Switch::createSprite()
{
	Rect rect(0.0f, 0.0f, 32.0f, 32.0f);
    
	Sprite* ret = Sprite::create("Switch.png", rect);
    
	return ret;
}
