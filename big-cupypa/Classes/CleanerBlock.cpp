//
//  CleanerBlock.cpp
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/05/08.
//
//

#include "CleanerBlock.h"
#include "Stage.h"

USING_NS_CC;

/**
 * @brief	デフォルトコンストラクタ
 */
CleanerBlock::CleanerBlock()
{
	// オブジェクトタイプを設定
	setObjectType(OBJECTTYPE_CLEANERBLOCK);
}

/**
 * @brief	デストラクタ
 */
CleanerBlock::~CleanerBlock()
{
}

/**
 * @brief	タッチ終了時の処理
 * @param	touch [in] タッチ情報
 * @param	event [in] イベント情報
 */
void CleanerBlock::onTouchEnded(
        cocos2d::Touch* touch,
        cocos2d::Event* event)
{
	// 座標
	Point touchPoint = touch->getLocation();
    
	// バグブロックを取得
	GameObjectArray bugs;
	unsigned int numBugs = stage->getGameObjects(&bugs, OBJECTTYPE_BUGBLOCK);
    
	for (unsigned int i = 0; i < numBugs; ++i)
	{
		// 衝突判定
		if (bugs[i]->checkCollision(touchPoint))
		{
			// 削除フラグを設定
			bugs[i]->setDeleteFlag(true);
            
			// 全削除ver
			/*for (unsigned int k = 0; k < numBugs; ++k)
             {
             bugs[k]->setDeleteFlag(true);
             }*/
			setDeleteFlag(true);
			return;
		}
	}
    
	// 基底クラスのタッチ終了時の処理
	MovableBlock::onTouchEnded(touch, event);
}
