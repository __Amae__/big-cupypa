//
//  ObjectDefs.h
//  cupypa
//
//  Created by Yusuke Fujioka on 2014/04/28.
//
//

#ifndef __OBJECTDEFS_H__
#define __OBJECTDEFS_H__

/**
 * @brief   オブジェクトタイプ
 */
enum ObjectType
{
    // 不明
    OBJECTTYPE_UNKNOWN = -1,
    
    // 空
	OBJECTTYPE_EMPTY,
	
    // ムーバブルブロック
	OBJECTTYPE_MOVABLEBLOCK,
	
    // アンムーバブルブロック
	OBJECTTYPE_UNMOVABLEBLOCK,
	
    // バグブロック
	OBJECTTYPE_BUGBLOCK,
	
    // クリーナーブロック
	OBJECTTYPE_CLEANERBLOCK,
	
    // スイッチ
	OBJECTTYPE_SWITCH,
    
    // ワープ
	OBJECTTYPE_WARP,
    
    // ゲート
    OBJECTTYPE_GATE,

    // キャラクター
	OBJECTTYPE_CHARACTER,
	
    // フォールダウンブロック
	OBJECTTYPE_FALLDOWNBLOCK,
	
    // センシティブブロック
	OBJECTTYPE_SENSITIVEBLOCK,
	
    // 自動移動ブロック
	OBJECTTYPE_AUTOMOVEBLOCK,
    
	// ステージ
	OBJECTTYPE_STAGE,
    
	// オブジェクトタイプの数
	NUM_OBJECTTYPES
};

#endif //__OBJECTDEFS_H__
