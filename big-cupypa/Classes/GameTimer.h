#ifndef __CUPYPA_TIMER_H__
#define __CUPYPA_TIMER_H__

#include "cocos2d.h"

/**
 * @brief	タイマー
 */
class GameTimer
{
public:
    
	/**
	 * @brief	タイマーの初期化
	 */
	static void init();
    
	/**
	 * @brief	タイマーの更新
	 */
	static void update();
    
	/**
	 * @brief	経過時間の取得
	 * @return	経過時間
	 */
	static float getElapsedTime() { return elapsedTime; }
    
private:
    
	// 経過時間
	static float elapsedTime;
    
	// 時間
	static timeval timeVal;
};

#endif // __CUPYPA_TIMER_H__
