#include "AppDelegate.h"
#include "SceneId.h"
#include "SceneFactory.h"
#include "GameParameter.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

namespace
{
	// 最初のシーンID
    const SceneId FIRST_SCENETYPE = PLAY_SCENE;
    
	// 表示状態
	const bool SHOW_STATUS = true;
}

/**
 * @brief コンストラクタ
 */
AppDelegate::AppDelegate()
: director(nullptr)
, view(nullptr)
{
    // 乱数の初期化
    srand(static_cast<unsigned int>(time(NULL)));
}

/**
 * @brief デストラクタ
 */
AppDelegate::~AppDelegate() 
{
}

/**
 * @brief  アプリケーションの起動処理
 * @return true  成功
 * @return false 失敗
 */
bool AppDelegate::applicationDidFinishLaunching()
{
    // ディレクターのインスタンス取得
	director = Director::getInstance();
    
	// OpenGLViewの生成
    view = director->getOpenGLView();
    if(!view)
    {
        view = GLView::create("CUPYPA");
    }
    // ディレクターの初期化
	if (!initDirector())
	{
		return false;
	}
    
	// ビューの初期化
	if (!initView())
	{
		return false;
	}
    
	// É^ÉCÉÄÇê›íËÇ∑ÇÈ
	//GameManager::getInstance()->setTime(19000);
    
	// 最初のシーンを生成
	Scene* firstScene = SceneFactory::createScene(FIRST_SCENETYPE);
    
	// シーンの実行
	director->runWithScene(firstScene);

    return true;
}

/**
 * @brief アプリケーションの停止処理
 */
void AppDelegate::applicationDidEnterBackground()
{
    // 更新処理の停止
    director->stopAnimation();
    
    // BGM・SE一時停止
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    SimpleAudioEngine::getInstance()->pauseAllEffects();
}

/**
 * @brief アプリケーションの再開処理
 */
void AppDelegate::applicationWillEnterForeground()
{
    // 更新処理の再開
    director->startAnimation();
    
    // BGM・SE一時停止
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    SimpleAudioEngine::getInstance()->resumeAllEffects();
}

/**
 * @brief	ディレクターの初期化
 * @return 	true  成功
 * @return 	false 失敗
 */
bool AppDelegate::initDirector()
{
    // OpenGLViewの設定
    director->setOpenGLView(view);
                  
    // デバッグ表示の設定
    director->setDisplayStats(SHOW_STATUS);
                  
    // 更新間隔の設定
    director->setAnimationInterval(GameParameter::ANIMATION_INTERVAL);
    
    return true;
}

/**
 * @brief	ビューの初期化
 * @return 	true  成功
 * @return 	false 失敗
 */
bool AppDelegate::initView()
{
    // ビューの設定
	view->setDesignResolutionSize(
                                  GameParameter::BASE_SCREEN_WIDTH,
                                  GameParameter::BASE_SCREEN_HEIGHT,
                                  ResolutionPolicy::SHOW_ALL);
    
    return true;
}