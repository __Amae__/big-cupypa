#ifndef __CUPYPA_UI_BUTTON_H__
#define __CUPYPA_UI_BUTTON_H__

#include <iostream>
#include "cocos2d.h"

class Button : public cocos2d::Sprite
{
private:
    
    std::string mFilename;
    
    cocos2d::Rect* mRect;
    
    /**
     *
     */
    Button(std::string filename, cocos2d::Rect* rect);
    
public:
    
    /**
     *
     */
    ~Button();

    /**
     *
     */
    static Button* create(std::string filename, cocos2d::Rect* rect = nullptr);
    
    /**
     *
     */
    bool init();
    
    /**
     *
     */
    bool isTouch(cocos2d::Point touch);
};

#endif // __CUPYPA_UI_BUTTON_H__
